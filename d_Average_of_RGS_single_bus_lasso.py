import b_Control_function
import e_Randomized_grid_searchRGS_single_bus_lasso
import time
import pandas as pd
from scipy.stats import randint as sp_randint, uniform

def Average_of_RGS(data, select_bus, param_dist, cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons'], vals={'Best MSE': 0, 'activation': 0, 'batch_size': 0, 'epochs': 0, 'learning_rate': 0, 'num_layers': 0, 'num_neurons': 0 }, choice_of_hyper='best'):

        collection = []
        num_of_itr = 2 # number of iterations reduced because the performance went down with increase in iterations

        for i in range(num_of_itr):
            collection.append(e_Randomized_grid_searchRGS_single_bus_lasso.Randomized_grid_search_RGS(data, select_bus, param_dist, choice_of_hyper))

        df = pd.DataFrame(columns=cols)

        rows= 1 # number of buses
        colm = len(cols) # number of parameters

        for i in range(rows):
                df = df._append(vals, ignore_index=True)
                for j in range(colm):
                        if choice_of_hyper == 'best':
                                if j == 1:
                                        check1 = 0
                                        check2 = 0
                                        for k in range(num_of_itr):
                                                if collection[k].iloc[i][j] == 'relu':
                                                        check1 = check1 + 1
                                                elif collection[k].iloc[i][j] == 'linear':
                                                        check2 = check2 + 1

                                        if check1 > check2:
                                                df.iloc[i][j] = 'relu'
                                        else:
                                                df.iloc[i][j] = 'linear'
                                else:
                                        sum = 0
                                        for k in range(num_of_itr):
                                                sum = sum + collection[k].iloc[i][j]
                                        df.iloc[i][j] = sum / num_of_itr

        print('Average of RGS')
        print(df)
        #---------------------------------------------------------------------------------
        return df


if __name__ == "__main__":


        start = time.time()  # recording the time
        data, num_cols, bus_name = b_Control_function.prepare_data()  # fetching data from control_function
        select_bus = bus_name[0]
        choice_of_hyper = 'best'

        if choice_of_hyper == 'best':
                param_dist = {
                        'epochs': sp_randint(10, 500),
                        'batch_size': sp_randint(10, 30),
                        'learning_rate': uniform(1e-5, 1e-1),
                        'num_layers': sp_randint(1, 10),
                        'num_neurons': sp_randint(10, 50),
                        'activation': ['relu', 'linear']
                }
                cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons']
                vals = {'Best MSE': 0,
                        'activation': 0,
                        'batch_size': 0,
                        'epochs': 0,
                        'learning_rate': 0,
                        'num_layers': 0,
                        'num_neurons': 0}

        df= Average_of_RGS(data, select_bus, param_dist, cols, vals, choice_of_hyper)
        stop = time.time()
        print(f"Training took {stop - start:.3f} seconds.")
