import b_Control_function
import c_Linear_regression_single_bus_Lasso
import numpy as np
import pandas as pd
from sklearn.model_selection import RandomizedSearchCV
from sklearn.base import BaseEstimator, RegressorMixin  # Import BaseEstimator and RegressorMixin
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.losses import MeanSquaredError
from scipy.stats import randint as sp_randint, uniform
import time
from itertools import chain


# Define the function to create the model
def create_model(learning_rate=1e-3, num_layers=2, activation='relu', num_neurons=15):
    L1 = 0.1 #0.0458
    L2 =  0.1 #0.0458
    model = Sequential()
    if activation == 'linear':
        model.add(Dense(units=1,
                        input_dim=2,
                        activation=activation,
                        name='layer_1',
                        kernel_regularizer=tf.keras.regularizers.L1(L1),
                        activity_regularizer=tf.keras.regularizers.L2(L2),
                        bias_regularizer=tf.keras.regularizers.L1(0.2) #0.055
                        )
                  )
    else:
        model.add(tf.keras.layers.Dense(units=int(num_neurons),
                                        input_dim=2,
                                        activation=activation,
                                        name='layer_1',
                                        kernel_regularizer=tf.keras.regularizers.L1(L1),
                                        activity_regularizer=tf.keras.regularizers.L2(L2),
                                        bias_regularizer=tf.keras.regularizers.L1(0.2) #0.055
                                        )
                  )
    for _ in range(int(num_layers)):
        model.add(tf.keras.layers.Dense(units=int(num_neurons),
                                        activation=activation,
                                        kernel_regularizer=tf.keras.regularizers.L1(L1),
                                        activity_regularizer=tf.keras.regularizers.L2(L2),
                                        bias_regularizer=tf.keras.regularizers.L1(0.2) #0.055
                                        )
                  )
    model.add(Dense(units=1, activation='linear', name='layer_output'))
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate), loss=MeanSquaredError())
    return model


# Define the KerasRegressorWithHistory wrapper
class KerasRegressorWithHistory(BaseEstimator, RegressorMixin):
    def __init__(self, build_fn=create_model, epochs=100, batch_size=3, learning_rate=1e-3, num_layers=2, activation='relu', num_neurons=15):
        self.build_fn = build_fn
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.num_layers = int(num_layers)
        self.activation = activation
        self.num_neurons = int(num_neurons)
        self.model = None
        self.loss_history = []

    def fit(self, X, y):
        self.model = self.build_fn(self.learning_rate, self.num_layers, self.activation, self.num_neurons)
        history = self.model.fit(X, y, epochs=self.epochs, batch_size=self.batch_size, verbose=0, callbacks=[tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda epoch, logs: self.loss_history.append(logs['loss']))])
        return self

    def predict(self, X):
        return self.model.predict(X)


def Randomized_grid_search_RGS(data, select_bus, param_dist, choice_of_hyper):


    # Fetching data for the first bus
    x1 = np.array(pd.DataFrame(data)[select_bus]["V"])
    x2 = np.array(pd.DataFrame(data)[select_bus]["q generated"])
    y = np.array(pd.DataFrame(data)[select_bus]["q generated t+1"])

    # Splitting data into training, test and cross validation
    reshaped_xtrain, reshaped_xtest, y_train, y_test= c_Linear_regression_single_bus_Lasso.split_data(x1,x2,y)

    # Perform randomized grid search
    n_iter_search = 2 #change?---------------------------------------------------------------------------------
    random_search = RandomizedSearchCV(KerasRegressorWithHistory(), param_distributions=param_dist, n_iter=n_iter_search, n_jobs=-1, verbose=0, cv=3, pre_dispatch='2*n_jobs')

    # Fit the randomized search to the data
    random_search.fit(reshaped_xtrain, y_train)
    new_model = random_search.best_estimator_  # best model
    v = list(random_search.best_params_.values()) # best parameters
    y_est = list(chain.from_iterable(new_model.predict(reshaped_xtrain))) # predict values
    print(v)

    if choice_of_hyper == 'best':
        result_data= {'Best MSE': random_search.best_score_, # best parameters found out
                   'activation': v[0],
                   'batch_size': v[1],
                   'epochs': v[2],
                   'learning_rate': v[3],
                   'num_layers': v[4],
                   'num_neurons': v[5],
                   'model': y_est}
        result_cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons', 'model']

    df = pd.DataFrame(columns=result_cols)

    df = df._append(result_data, ignore_index=True)

    return df


if __name__ == "__main__":
    start = time.time()
    data, num_cols, bus_name = b_Control_function.prepare_data()  # fetching data from control_function
    select_bus = bus_name[0]
    choice_of_hyper= 'best'

    if choice_of_hyper == 'best':
        param_dist = {
            'epochs': sp_randint(10, 500),
            'batch_size': sp_randint(10, 30),
            'learning_rate': uniform(1e-5, 1e-1),
            'num_layers': sp_randint(1, 10),
            'num_neurons': sp_randint(10, 50),
            'activation': ['relu', 'linear']
        }
        cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons']
        vals = {'Best MSE': 0,
                'activation': 0,
                'batch_size': 0,
                'epochs': 0,
                'learning_rate': 0,
                'num_layers': 0,
                'num_neurons': 0}
    df = Randomized_grid_search_RGS(data, select_bus, param_dist, choice_of_hyper)
    print(df)
    stop = time.time()
    print(f"Training took {stop - start:.3f} seconds.")

