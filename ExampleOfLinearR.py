import matplotlib.pyplot as plt
import numpy as np
import os
import plotly.graph_objects as go


output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
def plot(x1, y1, x1label, y1label, graph_name, x2=None, y2=None, x2label=None, y2label=None, graph_name2=None):
    coefficients = np.polyfit(x, y, 1)
    m, b = coefficients

    plt.plot(x1, m * np.array(x1) + b, label=f'Regression Line: y = {m:.2f}x + {b:.2f}', color='red')
    plt.scatter(x1, y1, label='Data Points', color='blue', marker='o')
    plt.xlabel(x1label)
    plt.ylabel(y1label)
    plt.title(graph_name)
    plt.legend()

    y_hat = m * np.array(x1) + b

    for xi, yi, yi_hat in zip(x, y, y_hat):
        arrow_color = 'black'
        arrow_style = '<->'  # Double-headed arrow
        arrow_line_width = 1
        plt.annotate('', xytext=(xi, yi), xy=(xi, yi_hat),
                     arrowprops=dict(arrowstyle=arrow_style, color=arrow_color, lw=arrow_line_width,
                                     shrinkA=0, shrinkB=0))

    plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
def calculate_cost(y_hat, y):
    n = len(y_hat)  # Number of data points
    squared_errors = [(y_hat[i] - y[i]) ** 2 for i in range(n)]
    J = (1 / n) * sum(squared_errors)
    return J
def plot_cost_function(w_range, c_range, y, fig_filename):
    w_values = np.linspace(w_range[0], w_range[1], 1000)
    c_values = np.linspace(c_range[0], c_range[1], 1000)

    W, C = np.meshgrid(w_values, c_values)
    J_values = np.zeros_like(W)

    for i in range(len(w_values)):
        for j in range(len(c_values)):
            J_values[i][j] = calculate_cost([W[i][j] * x + C[i][j] for x in y], y)

    min_J = np.min(J_values)
    min_indices = np.argwhere(J_values == min_J)
    if len(min_indices) > 0:
        min_w, min_c = w_values[min_indices[0][0]], c_values[min_indices[0][1]]
    else:
        min_w, min_c = None, None

    fig = go.Figure(data=[go.Surface(z=J_values, x=W, y=C)])
    print(f"Min J: {min_J}")
    print(f"Optimal w: {min_w}")
    print(f"Optimal c: {min_c}")
    print(min_J)
    fig.update_layout(scene=dict(xaxis_title='w', yaxis_title='c', zaxis_title='J(w,b)'))
    fig.write_html(os.path.join(output_path, fig_filename), auto_open=True)

if __name__ == '__main__':
    x = [15, 17.5, 20, 22.5, 25, 27.5, 30, 32.5, 35, 37.5]  # Temp
    y = [205, 220, 280, 312, 348, 370, 355, 420, 405, 445]  # sales
    plot(x, y, 'Temperature (°C)', 'Ice cream sales (Number)', 'Regression example')


    w_range = (-1000,1000)
    c_range = (-50, 50)
    fig_filename = 'j_ice.html'

    plot_cost_function(w_range, c_range, y, fig_filename)