from mpl_toolkits.mplot3d.axes3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure(figsize=(8, 5))
ax1 = fig.add_subplot(111, projection='3d')

alpha = 0.8
w = np.linspace(-alpha, alpha, 1000)
c = np.linspace(-alpha, alpha, 1000)
W, C = np.meshgrid(w, c)
J = 1 / (1 + np.exp(-(W**2 + C**2)))

surface = ax1.plot_surface(W, C, J, cmap='viridis')

ax1.set_xticks([])  # Remove x-axis numbering
ax1.set_yticks([])  # Remove y-axis numbering
ax1.set_zticks([])  # Remove z-axis numbering

ax1.set_xlabel('w')
ax1.set_ylabel('c')
ax1.set_zlabel('J')
ax1.grid()
ax1.set_title('3D Plot with w, c, and J')

ax1.grid(True)  # Add a grid

fig.colorbar(surface, ax=ax1, shrink=0.5, aspect=10)

plt.show()
