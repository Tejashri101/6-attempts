import matplotlib.pyplot as plt

def plot_mygraph(graph_name, data, labelx1, labely1, labely2=None, labely3=None):
    """
    :param graph_name: Name of the graph to be saved
    :param data: dataframe with multicolumns data[bus_name]['q generated'or'q generated t+1'or'V']
    :param labelx1: Label for x-axis
    :param labely1: Name of this parameter
    :param labely2: Name of this parameter
    :param labely3: Name of this parameter
    :return: a plot of all the buses
    """

    num_cols = int(len(data.columns)/3)
    nrows = 4
    ncols = 4
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(20, 10))

    for idx in range(num_cols):

        bus_name = data.columns[3*idx][0].split("___")[0] #3*idx done to select the correct value of bus_name due to data#s structure
        # Extract values for plotting
        time_series = data.index
        values1 = data[bus_name][labely1]
        values2 = data[bus_name][labely2]
        values3 = data[bus_name][labely3]

        ax = axes[idx // ncols][idx % ncols]

        # Create a line plot
        ax.plot(time_series, values1, label=labely1)
        if (labely2 != 'Extra1'):
            ax.plot(time_series, values2, label=labely2, color='green')

        if (labely3 != 'Extra2'):
            ax.plot(time_series, values3, label=labely3)

        # Add labels and title
        if graph_name == 'Loss function':
            ax.set_ylim(-.001,.001)

        ax.set_xlabel(labelx1)
        ax.set_ylabel('Y_axis')
        ax.set_title(bus_name)
        ax.legend(loc='upper right')

        # Save the plot as an image file
        plt.tight_layout()

    plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
    plt.close()