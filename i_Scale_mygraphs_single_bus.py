import matplotlib.pyplot as plt

def feature_scaling_mymodel(graph_name, norm_new_crtl, norm_old_crtl, count, time_taken=0):
    #FEATURE SCALING!!
    num_cols = int(len(norm_new_crtl.columns) / 3)
    fig, axes = plt.subplots(figsize=(20, 10))

    for idx in range(num_cols):
        bus_name = norm_new_crtl.columns[3 * idx][0].split("___")[0]  # 3*idx done to select the correct value of bus_name due to data#s structure
        # Extract values for plotting

        time_series = norm_new_crtl.index

        values1 = (norm_new_crtl[bus_name]['Model Prediction']) / (norm_old_crtl[bus_name]['Original'])
        values2 = (norm_old_crtl[bus_name]['Model Prediction']) / (norm_old_crtl[bus_name]['Original'])
        values3 = (norm_old_crtl[bus_name]['Original']) / (norm_old_crtl[bus_name]['Original'])

        #values1 = (norm_new_crtl[bus_name]['Model Prediction'])
        #values2 = (norm_old_crtl[bus_name]['Model Prediction'])
        #values3 = (norm_old_crtl[bus_name]['Original'])

        # Create a line plot

        axes.plot(time_series, values2, label='Old Model', color='grey')
        axes.plot(time_series, values3, label='Original', color='black')
        if count == 1:
            axes.plot(time_series, values1, label='New Model', color='green')
            axes.fill_between(time_series, values1, values3, where=(values1 >= values3), interpolate=True, color='green', alpha=0.3, label='New')
            axes.fill_between(time_series, values1, values3, where=(values1 < values3), interpolate=True, color='green', alpha=0.3)
        else:
            axes.plot(time_series, values1, label='New Model', color='red')
            axes.fill_between(time_series, values1, values3, where=(values1 >= values3), interpolate=True, color='red', alpha=0.3, label='New')
            axes.fill_between(time_series, values1, values3, where=(values1 < values3), interpolate=True, color='red', alpha=0.3)
        axes.fill_between(time_series, values2, values3, where=(values2 >= values3), interpolate=True, color='yellow', alpha=0.3, label='Old')
        axes.fill_between(time_series, values2, values3, where=(values2 < values3), interpolate=True, color='yellow', alpha=0.3)

        # Add labels and title
        axes.set_xlabel('Time')
        axes.set_ylabel('Y_axis')
        axes.set_title(bus_name)

        # Append 'Time Taken' to the existing legends
        handles, labels = axes.get_legend_handles_labels()
        time_taken_min = time_taken/60
        handles.append(plt.Line2D([0], [0], color='white', label=f'Time Taken: {time_taken_min:.3f} minutes'))
        labels.append(f'Time Taken: {time_taken_min:.3f} minutes')

        # Display the legend with all labels
        axes.legend(handles, labels, loc='upper right')
        # Save the plot as an image file
        plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
        plt.tight_layout()




