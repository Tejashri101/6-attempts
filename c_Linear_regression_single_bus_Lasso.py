from sklearn.metrics import mean_squared_error, r2_score
import b_Control_function
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.losses import MeanSquaredError
from sklearn.model_selection import train_test_split  # For creating train/test splits
from sklearn.base import BaseEstimator, RegressorMixin
from itertools import chain
import os
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from keras.callbacks import History
history = History()
epochs_num =200

# Set the random seed for NumPy and TensorFlow
random_seed = 42
np.random.seed(random_seed)
tf.random.set_seed(random_seed)


def reshape_for_regression(x1_train, x1_test, x2_train, x2_test):

    reshaped_xtrain = np.stack([x1_train, x2_train], 1)  # Reshape to (None, 1)
    reshaped_xtest = np.stack([x1_test, x2_test], 1)  # Reshape to (None, 1)

    return reshaped_xtrain, reshaped_xtest

def split_data(x1, x2, y, test_ratio = 0.3):

    x1_train, x1_test = train_test_split(x1, test_size=test_ratio, random_state=42)
    x2_train, x2_test = train_test_split(x2, test_size=test_ratio, random_state=42)
    y_train, y_test = train_test_split(y, test_size=test_ratio, random_state=42)
    #data is split between test and train only as the cv spli will be donr automatically within the randomizedgridsearch function.

    #Z score normalization x = (x-x_mean)/x_std
    scaler_x1train = StandardScaler()
    x1_train = x1_train.reshape(-1,1)
    scaler_x1train.fit(x1_train)
    normalized_x1train = scaler_x1train.transform(x1_train)

    scaler_x2train = StandardScaler()
    x2_train = x2_train.reshape(-1, 1)
    scaler_x2train.fit(x2_train)
    normalized_x2train = scaler_x2train.transform(x2_train)

    scaler_x1test = StandardScaler()
    x1_test = x1_test.reshape(-1, 1)
    scaler_x1test.fit(x1_test)
    normalized_x1test = scaler_x1test.transform(x1_test)

    scaler_x2test = StandardScaler()
    x2_test = x2_test.reshape(-1, 1)
    scaler_x2test.fit(x2_test)
    normalized_x2test = scaler_x2test.transform(x2_test)

    scaler_ytrain = StandardScaler()
    y_train = y_train.reshape(-1, 1)
    scaler_ytrain.fit(y_train)
    normalized_ytrain = scaler_ytrain.transform(y_train)

    scaler_ytest = StandardScaler()
    y_test = y_test.reshape(-1, 1)
    scaler_ytest.fit(y_test)
    normalized_ytest = scaler_ytest.transform(y_test)

    # Reshaping
    reshaped_xtrain, reshaped_xtest = reshape_for_regression(
        normalized_x1train, normalized_x1test,
        normalized_x2train, normalized_x2test
    )
    xtrain_reshape = reshaped_xtrain.reshape(reshaped_xtrain.shape[0], -1)
    xtest_reshape = reshaped_xtest.reshape(reshaped_xtest.shape[0], -1)
    ytrain_reshape = normalized_ytrain.flatten()
    ytest_reshape = normalized_ytest.flatten()

    return xtrain_reshape, xtest_reshape, ytrain_reshape, ytest_reshape


class KerasRegressorWithHistory(BaseEstimator, RegressorMixin):
    def __init__(self, build_fn, epochs = epochs_num):
        self.build_fn = build_fn
        self.epochs = epochs
        self.model = None
        self.alpha = 0.01
        self.loss_history = []
        self.val_loss_history = []

    def fit(self, X, y, batch_size):

        self.model = self.build_fn()
        history = self.model.fit(X, y,
                                 epochs=self.epochs,
                                 batch_size=batch_size,
                                 verbose=0,
                                 callbacks=[tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda epoch, logs: self.loss_history.append(logs['loss'])),
                                            tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda epoch, logs: self.val_loss_history.append(logs['val_loss']))
                                            ],
                                 validation_split = 0.7,
                                 workers = 2,
                                 use_multiprocessing = True
                                 )
        return self

    def predict(self, X):
        return self.model.predict(X)

# Wrap the Keras model within a custom class
def create_model(select_bus, activation='relu', learning_rate=1e-3, num_layers=2, num_neurons=15, equally_split_neurons=True, batch_size=10):
    output_path = os.path.abspath(os.path.join(__file__, "..", "Saved_models"))
    L1 = 0.1 #0.0458
    L2 =  0.1  #0.0458
    model = Sequential()
    model.add(Dense(units=num_neurons,
                    input_dim=2,
                    activation=activation,
                    name='layer_1',
                    kernel_regularizer=tf.keras.regularizers.L1(L1),
                    activity_regularizer=tf.keras.regularizers.L2(L2),
                    bias_regularizer=tf.keras.regularizers.L1(0.2) #0.055
                    )
              )

    if equally_split_neurons:
        neurons_per_layer = [num_neurons] * num_layers
    else:
        # Generate a random number of layers
        min_num_layers = 2
        max_num_layers = 6
        random_num_layers = np.random.randint(min_num_layers, max_num_layers + 1)

        # Generate random neurons for each layer
        min_random_neurons = 10
        max_random_neurons = 100
        neurons_per_layer = np.random.randint(min_random_neurons, max_random_neurons + 1, size=random_num_layers)

    for neurons in neurons_per_layer:
        model.add(Dense(units=neurons,
                        activation=activation,
                        kernel_regularizer=tf.keras.regularizers.L1(L1),
                        activity_regularizer=tf.keras.regularizers.L2(L2),
                        bias_regularizer=tf.keras.regularizers.L1(0.2) #0.055
                        )
                  )

    model.add(Dense(units=1,
                    activation='linear',
                    name='layer_output',
                    kernel_regularizer=tf.keras.regularizers.L1(L1),
                    activity_regularizer=tf.keras.regularizers.L2(L2),
                    bias_regularizer=tf.keras.regularizers.L1(0.2) #0.055
                    )
              )

    STEPS_PER_EPOCH = int(1e4) // batch_size

    learning_rate_mod=tf.keras.optimizers.schedules.InverseTimeDecay(learning_rate, decay_steps=STEPS_PER_EPOCH*1000, decay_rate=1, staircase=False)

    model.compile(
        optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate_mod),
        loss=[MeanSquaredError()]#, r2_score]
    )

    model_name = 'real_model_' + select_bus
    model.save(f'{output_path}/{model_name}.hd5')
    # model.save(model_name,save_format='tf')
    # to load back the model
    # model = keras.models.load_model('path/to/location.keras')

    return model


def Linear_regression_single_bus(data, select_bus, activation='relu', learning_rate=1e-3, num_layers=3, num_neurons=10, batch_size=10, equally_split_neurons=False):
    #selecting 0.01 instead of 0.001 because the learning curve of 0.01 for 20 datapoints on the first bus was better
    interim_data_loss= {}
    interim_data_controller= {}

    # Fetching data for the first bus
    x1 = np.array(pd.DataFrame(data)[select_bus]["V"])
    x2 = np.array(pd.DataFrame(data)[select_bus]["q generated"])
    y = np.array(pd.DataFrame(data)[select_bus]["q generated t+1"])

    # Splitting
    reshaped_xtrain, reshaped_xtest, y_train, y_test = split_data(x1, x2, y)
#---------------------------------------------------------------------------------------
    # Training set
    model_train = KerasRegressorWithHistory(build_fn=lambda: create_model(select_bus, activation, learning_rate, int(num_layers), int(num_neurons), equally_split_neurons, batch_size))
    model_train.fit(reshaped_xtrain, y_train, int(batch_size))

    # Access the loss history for training and cross-validation
    xtrain_loss_history = model_train.loss_history
    xval_loss_history = model_train.val_loss_history

    # Make all the three columns of the same length and add to dataframe
    req_len=len(xtrain_loss_history)
# ---------------------------------------------------------------------------------------

    interim_data_loss[(select_bus, 'Training set')]=xtrain_loss_history
    interim_data_loss[(select_bus, 'Extra1')] = [0]*req_len
    interim_data_loss[(select_bus, 'Extra2')] = [0]*req_len


    send_data_loss = pd.DataFrame(interim_data_loss)

    # ------------------------------------------------------------------

    original_controller = y_test
    model_predicted_outputs = list(chain.from_iterable((model_train.predict(reshaped_xtest)).tolist())) # Use the trained model to predict
    model_predicted_train_outputs = list(chain.from_iterable((model_train.predict(reshaped_xtrain)).tolist()))

    # Adjust the sizes to make a dataframe at the end

    len_ori = len(original_controller)
    len_mod = len(model_predicted_outputs)

    max_len = max(len_ori, len_mod)
    min_len = min(len_ori, len_mod)

    req_len = max_len - min_len
    add_to_predict = [None]*req_len

    model_predicted_outputs.extend(add_to_predict)

    interim_data_controller[(select_bus, 'Original')] = original_controller
    interim_data_controller[(select_bus, 'Model Prediction')] = model_predicted_outputs
    interim_data_controller[(select_bus, 'Extra2')] = [0]*max_len

    send_data_controller = pd.DataFrame(interim_data_controller)

    # ------------------------------------------------------------------
    val_mse_values = []
    for epoch in range(epochs_num):
        val_mse = mean_squared_error(y_test, model_predicted_outputs)
        val_mse_values.append(val_mse)
    # ------------------------------------------------------------------

    mse_test = mean_squared_error(y_test, model_predicted_outputs) # compared to predicted
    mse_train = mean_squared_error(y_train, model_predicted_train_outputs)

    std_dev_ytest = np.std(y_test)
    std_dev_ytrain = np.std(y_train)
    std_dev_pred = np.std(model_predicted_outputs)

    r2_test = r2_score(y_test, model_predicted_outputs) #using predicted values
    r2_train = r2_score(y_train, model_predicted_train_outputs)


    mean_ytest = y_test.mean()
    mean_model_pred = ((np.array(model_predicted_outputs)).mean())
    mean_ytrain = y_train.mean()
    # ------------------------------------------------------------------

    return send_data_loss, send_data_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred

if __name__ == "__main__":


    data, num_cols, bus_name = b_Control_function.prepare_data()  # fetching data from control_function
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))


    select_bus = bus_name[0]

    loss, controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_train, std_dev_ytest, std_dev_ytrain, std_dev_pred = Linear_regression_single_bus(data, select_bus)
    send_data_controller = pd.DataFrame(controller)
    send_data_loss = pd.DataFrame(loss)


    time_series = send_data_controller.index
    values1 = send_data_controller[select_bus]['Original']
    values2 = send_data_controller[select_bus]['Model Prediction']

    fig, axes = plt.subplots(figsize=(20, 10))
    # Create a line plot
    axes.plot(time_series, values1, label='Original')
    axes.plot(time_series, values2, label='Model Prediction', color='green')

    axes.set_xlabel('Time')
    axes.set_ylabel('Y_axis')
    axes.set_title(select_bus)
    axes.legend(loc='upper right')

    # Save the plot as an image file
    plt.tight_layout()
    plt.savefig(f'{os.path.join(output_path,select_bus)}.svg', format='svg', dpi=300, bbox_inches='tight')
    #plt.close()


