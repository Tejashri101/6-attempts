# Use an official Python runtime as a parent image
#FROM python:3.10-slim-buster
#FROM nvcr.io/nvidia/tensorflow:21.01
FROM tensorflow/tensorflow:latest-gpu
#FROM python:3.10

# Allow to specify user and group for this image
#ARG USER_ID=1223613778
#ARG GROUP_ID=1223600513

# Add group and user specified with the build command
#RUN addgroup --gid $GROUP_ID user
#RUN adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID user


# Set the working directory in the container
WORKDIR /app

# Copy your Python files to the container

COPY a_data_loader.py /app/
COPY b_Control_function.py /app/
COPY c_Linear_regression_single_bus_Lasso.py /app/
COPY d_Average_of_RGS_single_bus_lasso.py /app/
COPY e_Randomized_grid_searchRGS_single_bus_lasso.py /app/
COPY f_New_parameter_model_single_bus_lasso.py /app/


COPY g_Plot_mygraphs.py /app/
COPY g_Plot_mygraphs_single_bus.py /app/
COPY h_Compare_mygraphs_single_bus.py /app/
COPY i_Scale_mygraphs_single_bus.py /app/

COPY j_analysis.py /app/

COPY requirements.txt /app/

RUN mkdir -p /app/Storage
RUN mkdir -p /app/Dumps
RUN mkdir -p /app/Dumps1
RUN ls -la /app

# Install any Python dependencies if needed (create a requirements.txt)
# Update package lists and install pip
RUN apt-get update && apt-get install -y python3-pip
RUN pip install --no-cache-dir -r requirements.txt

# Now switch to user privileges
#RUN chown -R $USER_ID:$GROUP_ID /app
#USER user

# Run your Python script
CMD ["python", "f_New_parameter_model_single_bus_lasso.py"]

