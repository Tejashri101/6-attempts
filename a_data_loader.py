import pandas as pd
import numpy as np
import os

def load_data(nrows=None):
    path = os.path.abspath(os.path.join(__file__, "..", "Storage", "midasmv_der.hdf5"))
    print(path)

    tmp = pd.HDFStore(path)
    data = tmp["Powergrid__0"]
    tmp.close()

    vcols = []
    qcols = []
    for col in data.columns:
        if "bus" in col and "vm_pu" in col:
            vcols.append(col)
        if "bus" in col and "q_mvar" in col:
            qcols.append(col)
    vdata = data[vcols]
    qdata = data[qcols]
    if nrows is not None:
        vdata = vdata.iloc[:nrows]
        qdata = qdata.iloc[:nrows]

    #print(qdata)
    return vdata, qdata


if __name__ == '__main__':
    load_data()