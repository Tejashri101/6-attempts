import matplotlib.pyplot as plt
import numpy as np
import os

output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))

# Linear Regression Example
x_linear = np.linspace(0, 10, 100)
y_linear = 2 * x_linear + 1 + np.random.normal(0, 2, size=len(x_linear))

# ReLU Activation Function Example
x_relu = np.linspace(-5, 5, 100)
y_relu = np.maximum(0, x_relu)

# Linear Activation Function Example
x_linear_func = np.linspace(-5, 5, 100)
y_linear_func = x_linear_func  # f(x) = x

plt.figure(figsize=(8, 6))
plt.plot(x_relu, y_relu, label=r'ReLU: $f(x) = \max(0, x)$')
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.legend()
plt.grid(True)
graph_name = 'relu.svg'
plt.savefig(os.path.join(output_path, graph_name), format='svg', dpi=300)
plt.show()


plt.figure(figsize=(8, 6))
plt.plot(x_linear_func, y_linear_func, label=r'Linear: $f(x) = x$')
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.legend()
plt.grid(True)
graph_name = 'linear.svg'
plt.savefig(os.path.join(output_path, graph_name), format='svg', dpi=300)
plt.show()
