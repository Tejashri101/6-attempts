import b_Control_function_real
import b_Control_function
import c_Linear_regression_single_bus_Lasso
import h_Compare_mygraphs_single_bus
import i_Scale_mygraphs_single_bus
import g_Plot_mygraphs_single_bus
import d_Average_of_RGS_single_bus_lasso
import pandas as pd
import time
import os
from sklearn.preprocessing import StandardScaler
from scipy.stats import randint as sp_randint, uniform
import matplotlib.pyplot as plt



def new_model_creation(data, num_cols, select_bus, param_dist, choice_of_hyper, cols, vals):

    if choice_of_hyper=='best':

        df = d_Average_of_RGS_single_bus_lasso.Average_of_RGS(data, select_bus, param_dist, cols, vals, choice_of_hyper)

        activation = df.iloc[0][1]
        batch_size = int(df.iloc[0][2]) #3 was epochs
        learning_rate = df.iloc[0][4]
        num_layers = int(df.iloc[0][5])
        num_neurons = int(df.iloc[0][6])



    new_loss, new_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_train, std_dev_ytest, std_dev_ytrain, std_dev_pred= c_Linear_regression_single_bus_Lasso.Linear_regression_single_bus(
        data,
        select_bus,
        activation=activation,
        learning_rate=learning_rate,
        num_layers=num_layers,
        num_neurons=num_neurons,
        batch_size=batch_size,
        equally_split_neurons=False
    )

    return new_loss, new_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_train, std_dev_ytest, std_dev_ytrain, std_dev_pred


def old_model_extract(data, select_bus):

    old_loss, old_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_train, std_dev_ytest, std_dev_ytrain, std_dev_pred = c_Linear_regression_single_bus_Lasso.Linear_regression_single_bus(data, select_bus)

    return old_loss, old_controller

def normalize_mydata(data_as_dataframe):

    scaler = StandardScaler()
    normalized_data = scaler.fit_transform(data_as_dataframe)
    normalized_df = pd.DataFrame(normalized_data, columns=data_as_dataframe.columns)
    return normalized_df

def New_parameter_model(data, num_cols, select_bus, choice_of_hyper='best'):

    if choice_of_hyper == 'best':
        param_dist = {
            'epochs': sp_randint(10, 500),
            'batch_size': sp_randint(10, 30),
            'learning_rate': uniform(1e-5, 1e-1),
            'num_layers': sp_randint(1, 50),
            'num_neurons': sp_randint(5, 50),
            'activation': ['relu', 'linear']
        }
        cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons']
        vals = {'Best MSE': 0,
                'activation': 0,
                'batch_size': 0,
                'epochs': 0,
                'learning_rate': 0,
                'num_layers': 0,
                'num_neurons': 0}

    new_loss, new_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_train, std_dev_ytest, std_dev_ytrain, std_dev_pred = new_model_creation(data, num_cols, select_bus, param_dist, choice_of_hyper, cols, vals)
    old_loss, old_controller= old_model_extract(data, select_bus)


    #graph_name = 'new_loss_' + select_bus
    #g_Plot_mygraphs_single_bus.plot_mygraph(os.path.join(output_path,graph_name), new_loss, 'Epoch', 'Training set', 'Extra1', 'Extra2')
    #graph_name = 'SampleEfficiency_' + select_bus
    #g_Plot_mygraphs_single_bus.plot_mygraph(os.path.join(output_path,graph_name), new_controller, labelx1='Time', labely1='Model Prediction', mse = None)
    #graph_name = 'Comapre_loss_' + select_bus
    #h_Compare_mygraphs_single_bus.compare_mygraph(os.path.join(output_path,graph_name), new_loss, old_loss, 'Epoch', 'Training set', 'New Loss','Training set', 'Old Loss', 'Extra2', 'Extra2')
    #graph_name = 'compare_' + select_bus
    #h_Compare_mygraphs_single_bus.compare_mygraph(os.path.join(output_path,graph_name), new_controller, old_controller, 'Time', 'Model Prediction','New model', 'Model Prediction', 'Old model', 'Original', 'Original')
    #graph_name = 'normalize_' + select_bus
    #i_Scale_mygraphs_single_bus.feature_scaling_mymodel(os.path.join(output_path,graph_name), new_controller, old_controller)


    return new_loss, new_controller, old_loss, old_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_train, std_dev_ytest, std_dev_ytrain, std_dev_pred

def plot_r2_mse(graph_name, x, y_r2_test, y_r2_train, y_mse_test, y_mse_train, y_ytest_mean, y_ypred_mean, y_ytrain_mean, std_dev_ytest, std_dev_ytrain, std_dev_model):
    plt.figure(figsize=(8, 6))
    plt.plot(x, y_r2_test, label='R-squared', color='blue', marker='o')
    plt.plot(x, y_r2_train, label='R-squared', color='magenta', marker='o')
    plt.plot(x, y_mse_test, label='MSE test', color='green', marker='o')
    plt.plot(x, y_mse_train, label='MSE train', color='cyan', marker='o')
    plt.plot(x, y_ytest_mean, label='Mean y_test', color='orange', marker='o')
    plt.plot(x, y_ypred_mean, label='Mean y_pred', color='red', marker='o')
    plt.plot(x, y_ytrain_mean, label='Mean y_train', color='lightsalmon', marker='o')
    plt.plot(x, std_dev_ytest, label='Std y_test', color='grey', marker='o')
    plt.plot(x, std_dev_ytrain, label='Std y_test', color='yellow', marker='o')
    plt.plot(x, std_dev_model, label='STd y_pred', color='black', marker='o')

    plt.xlabel('Data Points')
    plt.ylabel('Values')
    plt.title('R-squared and MSE Values')
    plt.legend()
    plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)

if __name__ == "__main__":

    calc=0
    total_time=0
    mse_test = []
    mse_train = []
    r2_test = []
    r2_train = []
    mean_ytest = []
    mean_ypred = []
    mean_ytrain = []
    std_dev_ytest = []
    std_dev_ytrain = []
    std_dev_model = []

    start1 = time.time()
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))

    #Simulated data
    data, num_cols, bus_name = b_Control_function.prepare_old_data(200)

    #Real data
    #data, num_cols, bus_name = b_Control_function_real.prepare_data()  # fetching data from control_function

    num_cols=1
    result_area = pd.DataFrame()
    for i in range(num_cols):
        start = time.time() # recording the time
        select_bus = bus_name[i]
        try: #to check if there is some syntax error or not
            (new_loss, new_controller, old_loss, old_controller,
             mse_testn, mse_trainn, val_mse_values, r2_testn, r2_trainn, mean_ytest_n,
             mean_model_pred_n, mean_ytrain_n, std_dev_ytest_n, std_dev_ytrain_n, std_dev_pred_n)= New_parameter_model(data, num_cols, select_bus,'best')
        except Exception:
            print("Error in main")
            raise

        stop = time.time()
        time_taken = (stop - start)


        output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
        graph_name = 'new_learning_curve_' + select_bus
        g_Plot_mygraphs_single_bus.plot_mygraph(os.path.join(output_path,graph_name), new_loss, 'Epoch', 'Training set', mse= val_mse_values)
        graph_name = 'compare_' + select_bus
        count, area_comparison_df=h_Compare_mygraphs_single_bus.compare_mygraph(os.path.join(output_path,graph_name), new_controller, old_controller, 'Time', 'Model Prediction','New model', 'Model Prediction', 'Old model', 'Original', 'Original')
        graph_name = 'rescaling_' + select_bus
        i_Scale_mygraphs_single_bus.feature_scaling_mymodel(os.path.join(output_path, graph_name), new_controller, old_controller, count, time_taken)

        mse_test.append(mse_testn)
        mse_train.append(mse_trainn)
        r2_test.append(r2_testn)
        r2_train.append(r2_trainn)
        mean_ytest.append(mean_ytest_n)
        mean_ypred.append(mean_model_pred_n)
        mean_ytrain.append(mean_ytrain_n)
        std_dev_ytest.append(std_dev_ytest_n)
        std_dev_ytrain.append(std_dev_ytrain_n)
        std_dev_model.append(std_dev_pred_n)

        result_area = result_area._append(area_comparison_df, ignore_index=True)
        if count==1:
            calc =calc+1

    print(result_area)

    # r2, mse, mean plot-----------------------------------------------------------
    x = range(1, len(mse_test) + 1)
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    graph_name = os.path.join(output_path,'r2_mse_mean.svg')
    plot_r2_mse(graph_name,x, r2_test, r2_train, mse_test, mse_train, mean_ytest, mean_ypred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_model)

    data = {
        'r2': [float(r2_test[0]), float(r2_train[0]), '-'],
        'mse': [float(mse_test[0]), float(mse_train[0]), '-'],
        'mean': [float(mean_ytest[0]), float(mean_ytrain[0]), float(mean_ypred[0])],
        'std': [float(std_dev_ytest[0]), (std_dev_ytrain[0]), float(std_dev_model[0])]
    }

    # Creating a DataFrame
    r2_mean_mse_df = pd.DataFrame(data, index=['Test', 'Train', 'Pred'])
    print(r2_mean_mse_df)
    r2_mean_mse_df.to_csv(os.path.join(output_path,'r2_mse_mean.csv'), index=False)
    # -----------------------------------------------------------------------------
    stop2 = time.time()
    total_time = (stop2-start1)/60
    print(f"{calc} buses have performed better than before in {total_time} minutes.")


"""
    try:
        new_loss = joblib.load(new_loss_path)
        new_controller = joblib.load(new_controller_path)
    except FileNotFoundError:
        new_loss, new_controller = new_model_creation(data, num_cols, bus_name, param_dist, choice_of_hyper, cols, vals)
        if choice_of_hyper == 'best':
            joblib.dump(new_loss, new_loss_path)
            joblib.dump(new_controller, new_controller_path)

    old_loss_path = os.path.join(path, "old_loss.joblib.z")
    print(f"Path for old loss is {old_loss_path}")
    old_controller_path = os.path.join(path, "old_controller.joblib.z")
    print(f"Path for old controller is {old_controller_path}")

    try:
        old_loss = joblib.load(old_loss_path)
        old_controller = joblib.load(old_controller_path)
    except FileNotFoundError:
        old_loss, old_controller = old_model_extract(data, num_cols, bus_name)
        joblib.dump(old_loss, old_loss_path)
        joblib.dump(old_controller, old_controller_path)
"""