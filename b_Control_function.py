import numpy as np
import pandas as pd
import a_data_loader
import g_Plot_mygraphs
import os
import matplotlib.pyplot as plt

# Define a class for q_controller
class QController():
    def __init__(self, q_gen_t, V_t, step_size, num_cols):
        # Initialize attributes
        self.q_gen_t = q_gen_t
        self.V_t = V_t
        self.num_cols = num_cols
        self.step_size = step_size
        self.q_gen_upp = 2000/self.num_cols      # Upper limit for q generation (2000 kW is the grid divided by the number of buses)
        self.q_gen_low = -2000/self.num_cols       # Lower limit for q generation (2000 kW is the grid divided by the number of buses)
        self.matrix = np.abs(np.diag([self.step_size] * len(self.V_t)))
        self.q_gen_t1 = None

    def control(self):
        # Calculate new q generated values
        self.q_gen_t1 = self.q_gen_t - np.dot(self.matrix, (self.V_t - 1))
        self.q_gen_t1 = np.clip(
            self.q_gen_t1,
            self.q_gen_low,
            self.q_gen_upp
        )
        return self.q_gen_t1

def prepare_data(n_samples=0):

    vdata, qdata = a_data_loader.load_data()
    num_cols = len(vdata.columns)
    step_size = 2  # assumption of the step size
    data = {}
    bus_name =[]

    for idx in range(num_cols):
        bus_name_temp = vdata.columns[idx].split("___")[0]
        vt_pass = vdata[vdata.columns[idx]].values
        qgent_pass = qdata[qdata.columns[idx]].values
        ctrl = QController(qgent_pass, vt_pass, step_size, num_cols)
        result = ctrl.control()

        bus_name.append(bus_name_temp) #collecting the names

        data[bus_name_temp] = {
            "q generated": ctrl.q_gen_t,
            "V": ctrl.V_t,
            "q generated t+1": result,
        }

    df = {}
    for col1 in data:                   #unpacking the data fetched
        for col2 in data[col1]:
            df[(col1, col2)] = data[col1][col2]
    df = pd.DataFrame(df)

    if n_samples > 0:
        df = df[:n_samples]

    #output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    df.to_csv(os.path.join('/home/tbhatt/Documents/1. MasterThesis/6. Attempts/Data/control_function_data.csv'), index=False)



    return num_cols, bus_name

def prepare_old_data(n_samples=0):
    data = pd.read_csv('/home/tbhatt/Documents/1. MasterThesis/6. Attempts/Data/simulated_data.csv')
    data.columns = pd.MultiIndex.from_tuples(
        [('0__bus__1', 'q generated'), ('0__bus__1', 'V'), ('0__bus__1', 'q generated t+1'),
         ('0__bus__10', 'q generated'), ('0__bus__10', 'V'), ('0__bus__10', 'q generated t+1'),
         ('0__bus__11', 'q generated'), ('0__bus__11', 'V'), ('0__bus__11', 'q generated t+1'),
         ('0__bus__12', 'q generated'), ('0__bus__12', 'V'), ('0__bus__12', 'q generated t+1'),
         ('0__bus__13', 'q generated'), ('0__bus__13', 'V'), ('0__bus__13', 'q generated t+1'),
         ('0__bus__14', 'q generated'), ('0__bus__14', 'V'), ('0__bus__14', 'q generated t+1'),
         ('0__bus__2', 'q generated'), ('0__bus__2', 'V'), ('0__bus__2', 'q generated t+1'),
         ('0__bus__3', 'q generated'), ('0__bus__3', 'V'), ('0__bus__3', 'q generated t+1'),
         ('0__bus__4', 'q generated'), ('0__bus__4', 'V'), ('0__bus__4', 'q generated t+1'),
         ('0__bus__5', 'q generated'), ('0__bus__5', 'V'), ('0__bus__5', 'q generated t+1'),
         ('0__bus__6', 'q generated'), ('0__bus__6', 'V'), ('0__bus__6', 'q generated t+1'),
         ('0__bus__7', 'q generated'), ('0__bus__7', 'V'), ('0__bus__7', 'q generated t+1'),
         ('0__bus__8', 'q generated'), ('0__bus__8', 'V'), ('0__bus__8', 'q generated t+1'),
         ('0__bus__9', 'q generated'), ('0__bus__9', 'V'), ('0__bus__9', 'q generated t+1')], names=['', ''])
    data = data.drop(0)
    data = data.apply(pd.to_numeric, errors='coerce')

    num_cols = int(data.shape[1]/3)
    bus_name = ['0__bus__1', '0__bus__10', '0__bus__11', '0__bus__12', '0__bus__13', '0__bus__14', '0__bus__2', '0__bus__3', '0__bus__4', '0__bus__5', '0__bus__6', '0__bus__7', '0__bus__8', '0__bus__9' ]

    if n_samples > 0:
        data = data[:n_samples]

    return data, num_cols, bus_name


def main():
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    #num_cols, bus_name = prepare_data()

    data, num_cols, bus_name = prepare_old_data()

    g_Plot_mygraphs.plot_mygraph(os.path.join(output_path, 'Qt_Qt1_V_simulated'), data, 'Time', 'q generated','q generated t+1', 'V')

    time_series = range(0, len(data))
    fig, axs = plt.subplots(4, 3, figsize=(15, 15))

    mean_data_v = []
    std_data_v = []
    mean_data_q = []
    std_data_q = []
    mean_data_qt1 = []
    std_data_qt1 = []

    for bus_num in range(1, 15):

        bus_name = f'0__bus__{bus_num}'
        axs[0,0].plot(time_series, data[bus_name]['V'], label=f'{bus_name} voltage')
        axs[0,1].plot(time_series, data[bus_name]['q generated'], label=f'{bus_name} q generated')
        axs[0,2].plot(time_series, data[bus_name]['q generated t+1'], label=f'{bus_name} q generated t+1')

        bus_name = f'0__bus__{bus_num}'

        mean_v = data[bus_name]['V'].mean()
        std_v = data[bus_name]['V'].std()

        mean_q = data[bus_name]['q generated'].mean()
        std_q = data[bus_name]['q generated'].std()

        mean_qt1 = data[bus_name]['q generated t+1'].mean()
        std_qt1 = data[bus_name]['q generated t+1'].std()

        mean_data_v.append(mean_v)
        std_data_v.append(std_v)

        mean_data_q.append(mean_q)
        std_data_q.append(std_q)

        mean_data_qt1.append(mean_qt1)
        std_data_qt1.append(std_qt1)

    # Add legends
    #axs[0].legend()
    #axs[1].legend()
    #axs[2].legend()

    # Add titles
    axs[0,0].set_title('Voltage (V)')
    axs[0,1].set_title('q generated')
    axs[0,2].set_title('q generated t+1')

    # Plot mean values
    axs[1, 0].plot(range(1, 15), mean_data_v, color='blue')
    axs[1, 0].set_title('Mean V')
    axs[1, 0].set_xlabel('Bus Number')
    axs[1, 0].set_ylabel('Mean Value')

    # Plot std deviation values
    axs[1, 1].plot(range(1, 15), std_data_v, color='orange')
    axs[1, 1].set_title('Std Dev V')
    axs[1, 1].set_xlabel('Bus Number')
    axs[1, 1].set_ylabel('Std Dev Value')

    # Plot mean values
    axs[2, 0].plot(range(1, 15), mean_data_q, color='blue')
    axs[2, 0].set_title('Mean Q')
    axs[2, 0].set_xlabel('Bus Number')
    axs[2, 0].set_ylabel('Mean Value')

    # Plot std deviation values
    axs[2, 1].plot(range(1, 15), std_data_q, color='orange')
    axs[2, 1].set_title('Std Dev Q')
    axs[2, 1].set_xlabel('Bus Number')
    axs[2, 1].set_ylabel('Std Dev Value')

    # Plot mean values
    axs[3, 0].plot(range(1, 15), mean_data_qt1, color='blue')
    axs[3, 0].set_title('Mean Q@t+1')
    axs[3, 0].set_xlabel('Bus Number')
    axs[3, 0].set_ylabel('Mean Value')

    # Plot std deviation values
    axs[3, 1].plot(range(1, 15), std_data_qt1, color='orange')
    axs[3, 1].set_title('Std Dev Q@t+1')
    axs[3, 1].set_xlabel('Bus Number')
    axs[3, 1].set_ylabel('Std Dev Value')

    # Remove unnecessary subplots
    axs[1, 2].axis('off')
    axs[2, 2].axis('off')
    axs[3, 2].axis('off')


    # Add common y-axis label
    fig.text(0, 0.5, 'Values', va='center', rotation='vertical', fontsize=12)

    # Adjust layout to prevent clipping of titles
    plt.tight_layout()

    # Save the combined plot
    plt.savefig(os.path.join(output_path, 'v_q_qt1_simulated.svg'), format='svg', dpi=300)
    plt.show()



# Main program execution
if __name__ == "__main__":

    main()
