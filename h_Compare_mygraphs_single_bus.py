import matplotlib.pyplot as plt
import pandas as pd
from scipy.integrate import trapz
import numpy as np



def compare_mygraph(graph_name, data1, data2, labelx1, labely1, labely11, labely2, labely22, labely3='Extra2', labely33='Extra2'):


    num_cols = int(len(data1.columns)/3)
    fig, axes = plt.subplots()
    area_comparison_df = pd.DataFrame(columns=['Bus Name', 'Org-Old(Yellow)', 'Org-New(Red)'])


    for idx in range(num_cols):
        calc = 0
        bus_name = data1.columns[3*idx][0].split("___")[0] #3*idx done to select the correct value of bus_name due to data#s structure
        # Extract values for plotting
        time_series = data1.index
        values1 = data1[bus_name][labely1]
        values2 = data2[bus_name][labely2]
        if labely3 != 'Extra2':
            values3 = data2[bus_name][labely3]

        # Create a line plot
        axes.plot(time_series, values1, label=labely11, color='Red')
        axes.plot(time_series, values2, label=labely22, color='grey')
        if labely3 != 'Extra2':
            axes.plot(time_series, values3, label=labely33, color='black')
            axes.fill_between(time_series, values1, values3, where=(values1 >= values3), interpolate=True, color='red', alpha=0.3, label='New')
            axes.fill_between(time_series, values1, values3, where=(values1 < values3), interpolate=True, color='red', alpha=0.3)
            axes.fill_between(time_series, values2, values3, where=(values2 >= values3), interpolate=True, color='yellow', alpha=0.3, label='Old')
            axes.fill_between(time_series, values2, values3, where=(values2 < values3), interpolate=True, color='yellow', alpha=0.3)

            area_red = trapz(np.abs(values3 - values1), x=time_series)
            area_yellow = trapz(np.abs(values3 - values2), x=time_series)

            data_add = pd.DataFrame({'Bus Name': [bus_name], 'Org-Old(Yellow)': [area_yellow], 'Org-New(Red)': [area_red]})
            area_comparison_df=area_comparison_df._append(data_add, ignore_index=True)

        # Add labels and title
        axes.set_xlabel(labelx1)
        axes.set_ylabel('Y_axis')
        axes.set_title(bus_name)
        axes.legend(loc='upper right')

        # Save the plot as an image file
        plt.tight_layout()

    if labely3 != 'Extra2':
        count = 0
        area_comparison_df['Difference'] = area_comparison_df['Org-Old(Yellow)'] - area_comparison_df['Org-New(Red)']
        area_comparison_df['Difference %'] = (100*area_comparison_df['Difference'])/area_comparison_df['Org-Old(Yellow)']
        area_comparison_df['Check'] = np.nan
        for idx in range(num_cols):
            if area_comparison_df['Org-Old(Yellow)'][idx]> area_comparison_df['Org-New(Red)'][idx]:
                area_comparison_df['Check'][idx]= 1
            else:
                area_comparison_df['Check'][idx] = 0
                count+=1
        print("Comparison of Areas:")
        print(area_comparison_df)
        print(f"{num_cols - count} / {num_cols} buses have performed better than before.")
        #count buses which perform better
        if (num_cols - count)==1:
            calc=1





    #plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
    plt.close()
    return calc,area_comparison_df