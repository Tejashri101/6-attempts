import matplotlib.pyplot as plt
import numpy as np

def plot_mygraph(graph_name, data, labelx1, labely1, labely2=None, labely3=None, mse=None):

    num_cols = int(len(data.columns)/3)
    fig, axes = plt.subplots()

    for idx in range(num_cols):

        bus_name = data.columns[3*idx][0].split("___")[0] #3*idx done to select the correct value of bus_name due to data#s structure
        # Extract values for plotting
        time_series = data.index
        values1 = data[bus_name][labely1]
        if (labely2 != None):
            values2 = data[bus_name][labely2]
        if (labely2 != None):
            values3 = data[bus_name][labely3]

        # Create a line plot


        axes.plot(time_series, values1, label=labely1)
        if mse != None:
            axes.plot(time_series, mse, label='Validation set')
        if (labely2 != None):
            axes.plot(time_series, values2, label=labely2, color='green')

        if (labely3 != None):
            axes.plot(time_series, values3, label=labely3)

        # Add labels and title
        if graph_name == 'Loss function':
            axes.set_ylim(-.001,.001)

        axes.set_xlabel(labelx1)
        axes.set_ylabel('Y_axis')
        axes.set_title(bus_name)
        axes.legend(loc='upper right')

        # Save the plot as an image file
        plt.tight_layout()

    plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
    plt.close()