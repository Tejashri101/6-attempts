Ϗ
��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
�
BiasAdd

value"T	
bias"T
output"T""
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
8
Const
output"dtype"
valuetensor"
dtypetype
$
DisableCopyOnRead
resource�
.
Identity

input"T
output"T"	
Ttype
2
L2Loss
t"T
output"T"
Ttype:
2
u
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
2	
�
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool("
allow_missing_filesbool( �
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
@
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
d
Shape

input"T&
output"out_type��out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
L

StringJoin
inputs*N

output"

Nint("
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.13.02v2.13.0-rc2-7-g1cb1a030a628��
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
~
current_learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *&
shared_namecurrent_learning_rate
w
)current_learning_rate/Read/ReadVariableOpReadVariableOpcurrent_learning_rate*
_output_shapes
: *
dtype0
f
	iterationVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	iteration
_
iteration/Read/ReadVariableOpReadVariableOp	iteration*
_output_shapes
: *
dtype0	
z
layer_output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_namelayer_output/bias
s
%layer_output/bias/Read/ReadVariableOpReadVariableOplayer_output/bias*
_output_shapes
:*
dtype0
�
layer_output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_namelayer_output/kernel
{
'layer_output/kernel/Read/ReadVariableOpReadVariableOplayer_output/kernel*
_output_shapes

:*
dtype0
r
dense_67/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_67/bias
k
!dense_67/bias/Read/ReadVariableOpReadVariableOpdense_67/bias*
_output_shapes
:*
dtype0
z
dense_67/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:X* 
shared_namedense_67/kernel
s
#dense_67/kernel/Read/ReadVariableOpReadVariableOpdense_67/kernel*
_output_shapes

:X*
dtype0
r
dense_66/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:X*
shared_namedense_66/bias
k
!dense_66/bias/Read/ReadVariableOpReadVariableOpdense_66/bias*
_output_shapes
:X*
dtype0
z
dense_66/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:X* 
shared_namedense_66/kernel
s
#dense_66/kernel/Read/ReadVariableOpReadVariableOpdense_66/kernel*
_output_shapes

:X*
dtype0
r
dense_65/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_65/bias
k
!dense_65/bias/Read/ReadVariableOpReadVariableOpdense_65/bias*
_output_shapes
:*
dtype0
z
dense_65/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:$* 
shared_namedense_65/kernel
s
#dense_65/kernel/Read/ReadVariableOpReadVariableOpdense_65/kernel*
_output_shapes

:$*
dtype0
r
dense_64/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:$*
shared_namedense_64/bias
k
!dense_64/bias/Read/ReadVariableOpReadVariableOpdense_64/bias*
_output_shapes
:$*
dtype0
z
dense_64/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:$* 
shared_namedense_64/kernel
s
#dense_64/kernel/Read/ReadVariableOpReadVariableOpdense_64/kernel*
_output_shapes

:$*
dtype0
r
dense_63/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_namedense_63/bias
k
!dense_63/bias/Read/ReadVariableOpReadVariableOpdense_63/bias*
_output_shapes
:*
dtype0
z
dense_63/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:c* 
shared_namedense_63/kernel
s
#dense_63/kernel/Read/ReadVariableOpReadVariableOpdense_63/kernel*
_output_shapes

:c*
dtype0
r
dense_62/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:c*
shared_namedense_62/bias
k
!dense_62/bias/Read/ReadVariableOpReadVariableOpdense_62/bias*
_output_shapes
:c*
dtype0
z
dense_62/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
c* 
shared_namedense_62/kernel
s
#dense_62/kernel/Read/ReadVariableOpReadVariableOpdense_62/kernel*
_output_shapes

:
c*
dtype0
p
layer_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namelayer_1/bias
i
 layer_1/bias/Read/ReadVariableOpReadVariableOplayer_1/bias*
_output_shapes
:
*
dtype0
x
layer_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*
shared_namelayer_1/kernel
q
"layer_1/kernel/Read/ReadVariableOpReadVariableOplayer_1/kernel*
_output_shapes

:
*
dtype0
�
serving_default_layer_1_inputPlaceholder*'
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_layer_1_inputlayer_1/kernellayer_1/biasdense_62/kerneldense_62/biasdense_63/kerneldense_63/biasdense_64/kerneldense_64/biasdense_65/kerneldense_65/biasdense_66/kerneldense_66/biasdense_67/kerneldense_67/biaslayer_output/kernellayer_output/bias*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*2
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8� */
f*R(
&__inference_signature_wrapper_12129104

NoOpNoOp
�H
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�G
value�GB�G B�G
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer_with_weights-4
layer-4
layer_with_weights-5
layer-5
layer_with_weights-6
layer-6
layer_with_weights-7
layer-7
		variables

trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature
	optimizer
loss

signatures*
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias*
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
* &call_and_return_all_conditional_losses

!kernel
"bias*
�
#	variables
$trainable_variables
%regularization_losses
&	keras_api
'__call__
*(&call_and_return_all_conditional_losses

)kernel
*bias*
�
+	variables
,trainable_variables
-regularization_losses
.	keras_api
/__call__
*0&call_and_return_all_conditional_losses

1kernel
2bias*
�
3	variables
4trainable_variables
5regularization_losses
6	keras_api
7__call__
*8&call_and_return_all_conditional_losses

9kernel
:bias*
�
;	variables
<trainable_variables
=regularization_losses
>	keras_api
?__call__
*@&call_and_return_all_conditional_losses

Akernel
Bbias*
�
C	variables
Dtrainable_variables
Eregularization_losses
F	keras_api
G__call__
*H&call_and_return_all_conditional_losses

Ikernel
Jbias*
�
K	variables
Ltrainable_variables
Mregularization_losses
N	keras_api
O__call__
*P&call_and_return_all_conditional_losses

Qkernel
Rbias*
z
0
1
!2
"3
)4
*5
16
27
98
:9
A10
B11
I12
J13
Q14
R15*
z
0
1
!2
"3
)4
*5
16
27
98
:9
A10
B11
I12
J13
Q14
R15*
x
S0
T1
U2
V3
W4
X5
Y6
Z7
[8
\9
]10
^11
_12
`13
a14
b15* 
�
cnon_trainable_variables

dlayers
emetrics
flayer_regularization_losses
glayer_metrics
		variables

trainable_variables
regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses*

htrace_0
itrace_1* 

jtrace_0
ktrace_1* 
* 
W
l
_variables
m_iterations
n_current_learning_rate
o_update_step_xla*
* 

pserving_default* 

0
1*

0
1*

S0
T1* 
�
qnon_trainable_variables

rlayers
smetrics
tlayer_regularization_losses
ulayer_metrics
	variables
trainable_variables
regularization_losses
__call__
vactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&w"call_and_return_conditional_losses*

xtrace_0* 

ytrace_0* 
^X
VARIABLE_VALUElayer_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE*
ZT
VARIABLE_VALUElayer_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE*

!0
"1*

!0
"1*

U0
V1* 
�
znon_trainable_variables

{layers
|metrics
}layer_regularization_losses
~layer_metrics
	variables
trainable_variables
regularization_losses
__call__
activity_regularizer_fn
* &call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_62/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_62/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE*

)0
*1*

)0
*1*

W0
X1* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
#	variables
$trainable_variables
%regularization_losses
'__call__
�activity_regularizer_fn
*(&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_63/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_63/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*

10
21*

10
21*

Y0
Z1* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
+	variables
,trainable_variables
-regularization_losses
/__call__
�activity_regularizer_fn
*0&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_64/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_64/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE*

90
:1*

90
:1*

[0
\1* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
3	variables
4trainable_variables
5regularization_losses
7__call__
�activity_regularizer_fn
*8&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_65/kernel6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_65/bias4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUE*

A0
B1*

A0
B1*

]0
^1* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
;	variables
<trainable_variables
=regularization_losses
?__call__
�activity_regularizer_fn
*@&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_66/kernel6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_66/bias4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUE*

I0
J1*

I0
J1*

_0
`1* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
C	variables
Dtrainable_variables
Eregularization_losses
G__call__
�activity_regularizer_fn
*H&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
_Y
VARIABLE_VALUEdense_67/kernel6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUE*
[U
VARIABLE_VALUEdense_67/bias4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUE*

Q0
R1*

Q0
R1*

a0
b1* 
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
K	variables
Ltrainable_variables
Mregularization_losses
O__call__
�activity_regularizer_fn
*P&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses*

�trace_0* 

�trace_0* 
c]
VARIABLE_VALUElayer_output/kernel6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUElayer_output/bias4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUE*

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 

�trace_0* 
* 
<
0
1
2
3
4
5
6
7*

�0*
* 
* 
* 
* 
* 
* 

m0*
SM
VARIABLE_VALUE	iteration0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUE*
jd
VARIABLE_VALUEcurrent_learning_rate;optimizer/_current_learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
* 

S0
T1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

U0
V1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

W0
X1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

Y0
Z1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

[0
\1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

]0
^1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

_0
`1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 

a0
b1* 
* 

�trace_0* 

�trace_0* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
<
�	variables
�	keras_api

�total

�count*
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

�0
�1*

�	variables*
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenamelayer_1/kernellayer_1/biasdense_62/kerneldense_62/biasdense_63/kerneldense_63/biasdense_64/kerneldense_64/biasdense_65/kerneldense_65/biasdense_66/kerneldense_66/biasdense_67/kerneldense_67/biaslayer_output/kernellayer_output/bias	iterationcurrent_learning_ratetotalcountConst*!
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__traced_save_12129845
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamelayer_1/kernellayer_1/biasdense_62/kerneldense_62/biasdense_63/kerneldense_63/biasdense_64/kerneldense_64/biasdense_65/kerneldense_65/biasdense_66/kerneldense_66/biasdense_67/kerneldense_67/biaslayer_output/kernellayer_output/bias	iterationcurrent_learning_ratetotalcount* 
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *-
f(R&
$__inference__traced_restore_12129914��
�
�
F__inference_dense_63_layer_call_and_return_conditional_losses_12129329

inputs0
matmul_readvariableop_resource:c-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_63/bias/Regularizer/Abs/ReadVariableOp�.dense_63/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:c*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:����������
.dense_63/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:c*
dtype0�
dense_63/kernel/Regularizer/AbsAbs6dense_63/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:cr
!dense_63/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_63/kernel/Regularizer/SumSum#dense_63/kernel/Regularizer/Abs:y:0*dense_63/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_63/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_63/kernel/Regularizer/mulMul*dense_63/kernel/Regularizer/mul/x:output:0(dense_63/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_63/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
dense_63/bias/Regularizer/AbsAbs4dense_63/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_63/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_63/bias/Regularizer/SumSum!dense_63/bias/Regularizer/Abs:y:0(dense_63/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_63/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_63/bias/Regularizer/mulMul(dense_63/bias/Regularizer/mul/x:output:0&dense_63/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_63/bias/Regularizer/Abs/ReadVariableOp/^dense_63/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������c: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_63/bias/Regularizer/Abs/ReadVariableOp,dense_63/bias/Regularizer/Abs/ReadVariableOp2`
.dense_63/kernel/Regularizer/Abs/ReadVariableOp.dense_63/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������c
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
E__inference_layer_1_layer_call_and_return_conditional_losses_12129243

inputs0
matmul_readvariableop_resource:
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������
�
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
/__inference_layer_output_layer_call_fn_12129510

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_12128389o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129504:($
"
_user_specified_name
12129506
�

�
__inference_loss_fn_3_12129583C
5dense_62_bias_regularizer_abs_readvariableop_resource:c
identity��,dense_62/bias/Regularizer/Abs/ReadVariableOp�
,dense_62/bias/Regularizer/Abs/ReadVariableOpReadVariableOp5dense_62_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:c*
dtype0
dense_62/bias/Regularizer/AbsAbs4dense_62/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:ci
dense_62/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_62/bias/Regularizer/SumSum!dense_62/bias/Regularizer/Abs:y:0(dense_62/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_62/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_62/bias/Regularizer/mulMul(dense_62/bias/Regularizer/mul/x:output:0&dense_62/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentity!dense_62/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: Q
NoOpNoOp-^dense_62/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2\
,dense_62/bias/Regularizer/Abs/ReadVariableOp,dense_62/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�

�
__inference_loss_fn_5_12129603C
5dense_63_bias_regularizer_abs_readvariableop_resource:
identity��,dense_63/bias/Regularizer/Abs/ReadVariableOp�
,dense_63/bias/Regularizer/Abs/ReadVariableOpReadVariableOp5dense_63_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:*
dtype0
dense_63/bias/Regularizer/AbsAbs4dense_63/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_63/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_63/bias/Regularizer/SumSum!dense_63/bias/Regularizer/Abs:y:0(dense_63/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_63/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_63/bias/Regularizer/mulMul(dense_63/bias/Regularizer/mul/x:output:0&dense_63/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentity!dense_63/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: Q
NoOpNoOp-^dense_63/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2\
,dense_63/bias/Regularizer/Abs/ReadVariableOp,dense_63/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
__inference_loss_fn_15_12129703G
9layer_output_bias_regularizer_abs_readvariableop_resource:
identity��0layer_output/bias/Regularizer/Abs/ReadVariableOp�
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOp9layer_output_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: c
IdentityIdentity%layer_output/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: U
NoOpNoOp1^layer_output/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
&__inference_signature_wrapper_12129104
layer_1_input
unknown:

	unknown_0:

	unknown_1:
c
	unknown_2:c
	unknown_3:c
	unknown_4:
	unknown_5:$
	unknown_6:$
	unknown_7:$
	unknown_8:
	unknown_9:X

unknown_10:X

unknown_11:X

unknown_12:

unknown_13:

unknown_14:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*2
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference__wrapped_model_12128057o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*F
_input_shapes5
3:���������: : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
12129070:($
"
_user_specified_name
12129072:($
"
_user_specified_name
12129074:($
"
_user_specified_name
12129076:($
"
_user_specified_name
12129078:($
"
_user_specified_name
12129080:($
"
_user_specified_name
12129082:($
"
_user_specified_name
12129084:(	$
"
_user_specified_name
12129086:(
$
"
_user_specified_name
12129088:($
"
_user_specified_name
12129090:($
"
_user_specified_name
12129092:($
"
_user_specified_name
12129094:($
"
_user_specified_name
12129096:($
"
_user_specified_name
12129098:($
"
_user_specified_name
12129100
�
�
F__inference_dense_65_layer_call_and_return_conditional_losses_12128282

inputs0
matmul_readvariableop_resource:$-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_65/bias/Regularizer/Abs/ReadVariableOp�.dense_65/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:����������
.dense_65/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0�
dense_65/kernel/Regularizer/AbsAbs6dense_65/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_65/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_65/kernel/Regularizer/SumSum#dense_65/kernel/Regularizer/Abs:y:0*dense_65/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_65/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_65/kernel/Regularizer/mulMul*dense_65/kernel/Regularizer/mul/x:output:0(dense_65/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_65/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
dense_65/bias/Regularizer/AbsAbs4dense_65/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_65/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_65/bias/Regularizer/SumSum!dense_65/bias/Regularizer/Abs:y:0(dense_65/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_65/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_65/bias/Regularizer/mulMul(dense_65/bias/Regularizer/mul/x:output:0&dense_65/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_65/bias/Regularizer/Abs/ReadVariableOp/^dense_65/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������$: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_65/bias/Regularizer/Abs/ReadVariableOp,dense_65/bias/Regularizer/Abs/ReadVariableOp2`
.dense_65/kernel/Regularizer/Abs/ReadVariableOp.dense_65/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������$
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
F__inference_dense_62_layer_call_and_return_conditional_losses_12128174

inputs0
matmul_readvariableop_resource:
c-
biasadd_readvariableop_resource:c
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_62/bias/Regularizer/Abs/ReadVariableOp�.dense_62/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
c*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������cr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:c*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������cP
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������c�
.dense_62/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
c*
dtype0�
dense_62/kernel/Regularizer/AbsAbs6dense_62/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
cr
!dense_62/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_62/kernel/Regularizer/SumSum#dense_62/kernel/Regularizer/Abs:y:0*dense_62/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_62/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_62/kernel/Regularizer/mulMul*dense_62/kernel/Regularizer/mul/x:output:0(dense_62/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_62/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:c*
dtype0
dense_62/bias/Regularizer/AbsAbs4dense_62/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:ci
dense_62/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_62/bias/Regularizer/SumSum!dense_62/bias/Regularizer/Abs:y:0(dense_62/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_62/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_62/bias/Regularizer/mulMul(dense_62/bias/Regularizer/mul/x:output:0&dense_62/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������c�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_62/bias/Regularizer/Abs/ReadVariableOp/^dense_62/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_62/bias/Regularizer/Abs/ReadVariableOp,dense_62/bias/Regularizer/Abs/ReadVariableOp2`
.dense_62/kernel/Regularizer/Abs/ReadVariableOp.dense_62/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
F__inference_dense_67_layer_call_and_return_conditional_losses_12128354

inputs0
matmul_readvariableop_resource:X-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_67/bias/Regularizer/Abs/ReadVariableOp�.dense_67/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:����������
.dense_67/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0�
dense_67/kernel/Regularizer/AbsAbs6dense_67/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_67/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_67/kernel/Regularizer/SumSum#dense_67/kernel/Regularizer/Abs:y:0*dense_67/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_67/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_67/kernel/Regularizer/mulMul*dense_67/kernel/Regularizer/mul/x:output:0(dense_67/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_67/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
dense_67/bias/Regularizer/AbsAbs4dense_67/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_67/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_67/bias/Regularizer/SumSum!dense_67/bias/Regularizer/Abs:y:0(dense_67/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_67/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_67/bias/Regularizer/mulMul(dense_67/bias/Regularizer/mul/x:output:0&dense_67/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_67/bias/Regularizer/Abs/ReadVariableOp/^dense_67/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������X: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_67/bias/Regularizer/Abs/ReadVariableOp,dense_67/bias/Regularizer/Abs/ReadVariableOp2`
.dense_67/kernel/Regularizer/Abs/ReadVariableOp.dense_67/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������X
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
N__inference_layer_output_layer_call_and_return_all_conditional_losses_12129521

inputs
unknown:
	unknown_0:
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_12128389�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *?
f:R8
6__inference_layer_output_activity_regularizer_12128113o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129513:($
"
_user_specified_name
12129515
�

�
__inference_loss_fn_1_12129563B
4layer_1_bias_regularizer_abs_readvariableop_resource:

identity��+layer_1/bias/Regularizer/Abs/ReadVariableOp�
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOp4layer_1_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: ^
IdentityIdentity layer_1/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: P
NoOpNoOp,^layer_1/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
ݝ
�
!__inference__traced_save_12129845
file_prefix7
%read_disablecopyonread_layer_1_kernel:
3
%read_1_disablecopyonread_layer_1_bias:
:
(read_2_disablecopyonread_dense_62_kernel:
c4
&read_3_disablecopyonread_dense_62_bias:c:
(read_4_disablecopyonread_dense_63_kernel:c4
&read_5_disablecopyonread_dense_63_bias::
(read_6_disablecopyonread_dense_64_kernel:$4
&read_7_disablecopyonread_dense_64_bias:$:
(read_8_disablecopyonread_dense_65_kernel:$4
&read_9_disablecopyonread_dense_65_bias:;
)read_10_disablecopyonread_dense_66_kernel:X5
'read_11_disablecopyonread_dense_66_bias:X;
)read_12_disablecopyonread_dense_67_kernel:X5
'read_13_disablecopyonread_dense_67_bias:?
-read_14_disablecopyonread_layer_output_kernel:9
+read_15_disablecopyonread_layer_output_bias:-
#read_16_disablecopyonread_iteration:	 9
/read_17_disablecopyonread_current_learning_rate: )
read_18_disablecopyonread_total: )
read_19_disablecopyonread_count: 
savev2_const
identity_41��MergeV2Checkpoints�Read/DisableCopyOnRead�Read/ReadVariableOp�Read_1/DisableCopyOnRead�Read_1/ReadVariableOp�Read_10/DisableCopyOnRead�Read_10/ReadVariableOp�Read_11/DisableCopyOnRead�Read_11/ReadVariableOp�Read_12/DisableCopyOnRead�Read_12/ReadVariableOp�Read_13/DisableCopyOnRead�Read_13/ReadVariableOp�Read_14/DisableCopyOnRead�Read_14/ReadVariableOp�Read_15/DisableCopyOnRead�Read_15/ReadVariableOp�Read_16/DisableCopyOnRead�Read_16/ReadVariableOp�Read_17/DisableCopyOnRead�Read_17/ReadVariableOp�Read_18/DisableCopyOnRead�Read_18/ReadVariableOp�Read_19/DisableCopyOnRead�Read_19/ReadVariableOp�Read_2/DisableCopyOnRead�Read_2/ReadVariableOp�Read_3/DisableCopyOnRead�Read_3/ReadVariableOp�Read_4/DisableCopyOnRead�Read_4/ReadVariableOp�Read_5/DisableCopyOnRead�Read_5/ReadVariableOp�Read_6/DisableCopyOnRead�Read_6/ReadVariableOp�Read_7/DisableCopyOnRead�Read_7/ReadVariableOp�Read_8/DisableCopyOnRead�Read_8/ReadVariableOp�Read_9/DisableCopyOnRead�Read_9/ReadVariableOpw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: w
Read/DisableCopyOnReadDisableCopyOnRead%read_disablecopyonread_layer_1_kernel"/device:CPU:0*
_output_shapes
 �
Read/ReadVariableOpReadVariableOp%read_disablecopyonread_layer_1_kernel^Read/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:
*
dtype0i
IdentityIdentityRead/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:
a

Identity_1IdentityIdentity:output:0"/device:CPU:0*
T0*
_output_shapes

:
y
Read_1/DisableCopyOnReadDisableCopyOnRead%read_1_disablecopyonread_layer_1_bias"/device:CPU:0*
_output_shapes
 �
Read_1/ReadVariableOpReadVariableOp%read_1_disablecopyonread_layer_1_bias^Read_1/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:
*
dtype0i

Identity_2IdentityRead_1/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:
_

Identity_3IdentityIdentity_2:output:0"/device:CPU:0*
T0*
_output_shapes
:
|
Read_2/DisableCopyOnReadDisableCopyOnRead(read_2_disablecopyonread_dense_62_kernel"/device:CPU:0*
_output_shapes
 �
Read_2/ReadVariableOpReadVariableOp(read_2_disablecopyonread_dense_62_kernel^Read_2/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:
c*
dtype0m

Identity_4IdentityRead_2/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:
cc

Identity_5IdentityIdentity_4:output:0"/device:CPU:0*
T0*
_output_shapes

:
cz
Read_3/DisableCopyOnReadDisableCopyOnRead&read_3_disablecopyonread_dense_62_bias"/device:CPU:0*
_output_shapes
 �
Read_3/ReadVariableOpReadVariableOp&read_3_disablecopyonread_dense_62_bias^Read_3/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:c*
dtype0i

Identity_6IdentityRead_3/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:c_

Identity_7IdentityIdentity_6:output:0"/device:CPU:0*
T0*
_output_shapes
:c|
Read_4/DisableCopyOnReadDisableCopyOnRead(read_4_disablecopyonread_dense_63_kernel"/device:CPU:0*
_output_shapes
 �
Read_4/ReadVariableOpReadVariableOp(read_4_disablecopyonread_dense_63_kernel^Read_4/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:c*
dtype0m

Identity_8IdentityRead_4/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:cc

Identity_9IdentityIdentity_8:output:0"/device:CPU:0*
T0*
_output_shapes

:cz
Read_5/DisableCopyOnReadDisableCopyOnRead&read_5_disablecopyonread_dense_63_bias"/device:CPU:0*
_output_shapes
 �
Read_5/ReadVariableOpReadVariableOp&read_5_disablecopyonread_dense_63_bias^Read_5/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0j
Identity_10IdentityRead_5/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_11IdentityIdentity_10:output:0"/device:CPU:0*
T0*
_output_shapes
:|
Read_6/DisableCopyOnReadDisableCopyOnRead(read_6_disablecopyonread_dense_64_kernel"/device:CPU:0*
_output_shapes
 �
Read_6/ReadVariableOpReadVariableOp(read_6_disablecopyonread_dense_64_kernel^Read_6/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:$*
dtype0n
Identity_12IdentityRead_6/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:$e
Identity_13IdentityIdentity_12:output:0"/device:CPU:0*
T0*
_output_shapes

:$z
Read_7/DisableCopyOnReadDisableCopyOnRead&read_7_disablecopyonread_dense_64_bias"/device:CPU:0*
_output_shapes
 �
Read_7/ReadVariableOpReadVariableOp&read_7_disablecopyonread_dense_64_bias^Read_7/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:$*
dtype0j
Identity_14IdentityRead_7/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:$a
Identity_15IdentityIdentity_14:output:0"/device:CPU:0*
T0*
_output_shapes
:$|
Read_8/DisableCopyOnReadDisableCopyOnRead(read_8_disablecopyonread_dense_65_kernel"/device:CPU:0*
_output_shapes
 �
Read_8/ReadVariableOpReadVariableOp(read_8_disablecopyonread_dense_65_kernel^Read_8/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:$*
dtype0n
Identity_16IdentityRead_8/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:$e
Identity_17IdentityIdentity_16:output:0"/device:CPU:0*
T0*
_output_shapes

:$z
Read_9/DisableCopyOnReadDisableCopyOnRead&read_9_disablecopyonread_dense_65_bias"/device:CPU:0*
_output_shapes
 �
Read_9/ReadVariableOpReadVariableOp&read_9_disablecopyonread_dense_65_bias^Read_9/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0j
Identity_18IdentityRead_9/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_19IdentityIdentity_18:output:0"/device:CPU:0*
T0*
_output_shapes
:~
Read_10/DisableCopyOnReadDisableCopyOnRead)read_10_disablecopyonread_dense_66_kernel"/device:CPU:0*
_output_shapes
 �
Read_10/ReadVariableOpReadVariableOp)read_10_disablecopyonread_dense_66_kernel^Read_10/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:X*
dtype0o
Identity_20IdentityRead_10/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:Xe
Identity_21IdentityIdentity_20:output:0"/device:CPU:0*
T0*
_output_shapes

:X|
Read_11/DisableCopyOnReadDisableCopyOnRead'read_11_disablecopyonread_dense_66_bias"/device:CPU:0*
_output_shapes
 �
Read_11/ReadVariableOpReadVariableOp'read_11_disablecopyonread_dense_66_bias^Read_11/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:X*
dtype0k
Identity_22IdentityRead_11/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Xa
Identity_23IdentityIdentity_22:output:0"/device:CPU:0*
T0*
_output_shapes
:X~
Read_12/DisableCopyOnReadDisableCopyOnRead)read_12_disablecopyonread_dense_67_kernel"/device:CPU:0*
_output_shapes
 �
Read_12/ReadVariableOpReadVariableOp)read_12_disablecopyonread_dense_67_kernel^Read_12/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:X*
dtype0o
Identity_24IdentityRead_12/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:Xe
Identity_25IdentityIdentity_24:output:0"/device:CPU:0*
T0*
_output_shapes

:X|
Read_13/DisableCopyOnReadDisableCopyOnRead'read_13_disablecopyonread_dense_67_bias"/device:CPU:0*
_output_shapes
 �
Read_13/ReadVariableOpReadVariableOp'read_13_disablecopyonread_dense_67_bias^Read_13/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_26IdentityRead_13/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_27IdentityIdentity_26:output:0"/device:CPU:0*
T0*
_output_shapes
:�
Read_14/DisableCopyOnReadDisableCopyOnRead-read_14_disablecopyonread_layer_output_kernel"/device:CPU:0*
_output_shapes
 �
Read_14/ReadVariableOpReadVariableOp-read_14_disablecopyonread_layer_output_kernel^Read_14/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:*
dtype0o
Identity_28IdentityRead_14/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:e
Identity_29IdentityIdentity_28:output:0"/device:CPU:0*
T0*
_output_shapes

:�
Read_15/DisableCopyOnReadDisableCopyOnRead+read_15_disablecopyonread_layer_output_bias"/device:CPU:0*
_output_shapes
 �
Read_15/ReadVariableOpReadVariableOp+read_15_disablecopyonread_layer_output_bias^Read_15/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0k
Identity_30IdentityRead_15/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_31IdentityIdentity_30:output:0"/device:CPU:0*
T0*
_output_shapes
:x
Read_16/DisableCopyOnReadDisableCopyOnRead#read_16_disablecopyonread_iteration"/device:CPU:0*
_output_shapes
 �
Read_16/ReadVariableOpReadVariableOp#read_16_disablecopyonread_iteration^Read_16/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0	g
Identity_32IdentityRead_16/ReadVariableOp:value:0"/device:CPU:0*
T0	*
_output_shapes
: ]
Identity_33IdentityIdentity_32:output:0"/device:CPU:0*
T0	*
_output_shapes
: �
Read_17/DisableCopyOnReadDisableCopyOnRead/read_17_disablecopyonread_current_learning_rate"/device:CPU:0*
_output_shapes
 �
Read_17/ReadVariableOpReadVariableOp/read_17_disablecopyonread_current_learning_rate^Read_17/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_34IdentityRead_17/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_35IdentityIdentity_34:output:0"/device:CPU:0*
T0*
_output_shapes
: t
Read_18/DisableCopyOnReadDisableCopyOnReadread_18_disablecopyonread_total"/device:CPU:0*
_output_shapes
 �
Read_18/ReadVariableOpReadVariableOpread_18_disablecopyonread_total^Read_18/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_36IdentityRead_18/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_37IdentityIdentity_36:output:0"/device:CPU:0*
T0*
_output_shapes
: t
Read_19/DisableCopyOnReadDisableCopyOnReadread_19_disablecopyonread_count"/device:CPU:0*
_output_shapes
 �
Read_19/ReadVariableOpReadVariableOpread_19_disablecopyonread_count^Read_19/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_38IdentityRead_19/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_39IdentityIdentity_38:output:0"/device:CPU:0*
T0*
_output_shapes
: �	
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB;optimizer/_current_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*=
value4B2B B B B B B B B B B B B B B B B B B B B B �
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0Identity_1:output:0Identity_3:output:0Identity_5:output:0Identity_7:output:0Identity_9:output:0Identity_11:output:0Identity_13:output:0Identity_15:output:0Identity_17:output:0Identity_19:output:0Identity_21:output:0Identity_23:output:0Identity_25:output:0Identity_27:output:0Identity_29:output:0Identity_31:output:0Identity_33:output:0Identity_35:output:0Identity_37:output:0Identity_39:output:0savev2_const"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *#
dtypes
2	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 i
Identity_40Identityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: U
Identity_41IdentityIdentity_40:output:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^MergeV2Checkpoints^Read/DisableCopyOnRead^Read/ReadVariableOp^Read_1/DisableCopyOnRead^Read_1/ReadVariableOp^Read_10/DisableCopyOnRead^Read_10/ReadVariableOp^Read_11/DisableCopyOnRead^Read_11/ReadVariableOp^Read_12/DisableCopyOnRead^Read_12/ReadVariableOp^Read_13/DisableCopyOnRead^Read_13/ReadVariableOp^Read_14/DisableCopyOnRead^Read_14/ReadVariableOp^Read_15/DisableCopyOnRead^Read_15/ReadVariableOp^Read_16/DisableCopyOnRead^Read_16/ReadVariableOp^Read_17/DisableCopyOnRead^Read_17/ReadVariableOp^Read_18/DisableCopyOnRead^Read_18/ReadVariableOp^Read_19/DisableCopyOnRead^Read_19/ReadVariableOp^Read_2/DisableCopyOnRead^Read_2/ReadVariableOp^Read_3/DisableCopyOnRead^Read_3/ReadVariableOp^Read_4/DisableCopyOnRead^Read_4/ReadVariableOp^Read_5/DisableCopyOnRead^Read_5/ReadVariableOp^Read_6/DisableCopyOnRead^Read_6/ReadVariableOp^Read_7/DisableCopyOnRead^Read_7/ReadVariableOp^Read_8/DisableCopyOnRead^Read_8/ReadVariableOp^Read_9/DisableCopyOnRead^Read_9/ReadVariableOp*
_output_shapes
 "#
identity_41Identity_41:output:0*(
_construction_contextkEagerRuntime*?
_input_shapes.
,: : : : : : : : : : : : : : : : : : : : : : 2(
MergeV2CheckpointsMergeV2Checkpoints20
Read/DisableCopyOnReadRead/DisableCopyOnRead2*
Read/ReadVariableOpRead/ReadVariableOp24
Read_1/DisableCopyOnReadRead_1/DisableCopyOnRead2.
Read_1/ReadVariableOpRead_1/ReadVariableOp26
Read_10/DisableCopyOnReadRead_10/DisableCopyOnRead20
Read_10/ReadVariableOpRead_10/ReadVariableOp26
Read_11/DisableCopyOnReadRead_11/DisableCopyOnRead20
Read_11/ReadVariableOpRead_11/ReadVariableOp26
Read_12/DisableCopyOnReadRead_12/DisableCopyOnRead20
Read_12/ReadVariableOpRead_12/ReadVariableOp26
Read_13/DisableCopyOnReadRead_13/DisableCopyOnRead20
Read_13/ReadVariableOpRead_13/ReadVariableOp26
Read_14/DisableCopyOnReadRead_14/DisableCopyOnRead20
Read_14/ReadVariableOpRead_14/ReadVariableOp26
Read_15/DisableCopyOnReadRead_15/DisableCopyOnRead20
Read_15/ReadVariableOpRead_15/ReadVariableOp26
Read_16/DisableCopyOnReadRead_16/DisableCopyOnRead20
Read_16/ReadVariableOpRead_16/ReadVariableOp26
Read_17/DisableCopyOnReadRead_17/DisableCopyOnRead20
Read_17/ReadVariableOpRead_17/ReadVariableOp26
Read_18/DisableCopyOnReadRead_18/DisableCopyOnRead20
Read_18/ReadVariableOpRead_18/ReadVariableOp26
Read_19/DisableCopyOnReadRead_19/DisableCopyOnRead20
Read_19/ReadVariableOpRead_19/ReadVariableOp24
Read_2/DisableCopyOnReadRead_2/DisableCopyOnRead2.
Read_2/ReadVariableOpRead_2/ReadVariableOp24
Read_3/DisableCopyOnReadRead_3/DisableCopyOnRead2.
Read_3/ReadVariableOpRead_3/ReadVariableOp24
Read_4/DisableCopyOnReadRead_4/DisableCopyOnRead2.
Read_4/ReadVariableOpRead_4/ReadVariableOp24
Read_5/DisableCopyOnReadRead_5/DisableCopyOnRead2.
Read_5/ReadVariableOpRead_5/ReadVariableOp24
Read_6/DisableCopyOnReadRead_6/DisableCopyOnRead2.
Read_6/ReadVariableOpRead_6/ReadVariableOp24
Read_7/DisableCopyOnReadRead_7/DisableCopyOnRead2.
Read_7/ReadVariableOpRead_7/ReadVariableOp24
Read_8/DisableCopyOnReadRead_8/DisableCopyOnRead2.
Read_8/ReadVariableOpRead_8/ReadVariableOp24
Read_9/DisableCopyOnReadRead_9/DisableCopyOnRead2.
Read_9/ReadVariableOpRead_9/ReadVariableOp:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:.*
(
_user_specified_namelayer_1/kernel:,(
&
_user_specified_namelayer_1/bias:/+
)
_user_specified_namedense_62/kernel:-)
'
_user_specified_namedense_62/bias:/+
)
_user_specified_namedense_63/kernel:-)
'
_user_specified_namedense_63/bias:/+
)
_user_specified_namedense_64/kernel:-)
'
_user_specified_namedense_64/bias:/	+
)
_user_specified_namedense_65/kernel:-
)
'
_user_specified_namedense_65/bias:/+
)
_user_specified_namedense_66/kernel:-)
'
_user_specified_namedense_66/bias:/+
)
_user_specified_namedense_67/kernel:-)
'
_user_specified_namedense_67/bias:3/
-
_user_specified_namelayer_output/kernel:1-
+
_user_specified_namelayer_output/bias:)%
#
_user_specified_name	iteration:51
/
_user_specified_namecurrent_learning_rate:%!

_user_specified_nametotal:%!

_user_specified_namecount:=9

_output_shapes
: 

_user_specified_nameConst
�
�
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128720
layer_1_input"
layer_1_12128511:

layer_1_12128513:
#
dense_62_12128524:
c
dense_62_12128526:c#
dense_63_12128537:c
dense_63_12128539:#
dense_64_12128550:$
dense_64_12128552:$#
dense_65_12128563:$
dense_65_12128565:#
dense_66_12128576:X
dense_66_12128578:X#
dense_67_12128589:X
dense_67_12128591:'
layer_output_12128602:#
layer_output_12128604:
identity

identity_1

identity_2

identity_3

identity_4

identity_5

identity_6

identity_7

identity_8�� dense_62/StatefulPartitionedCall�,dense_62/bias/Regularizer/Abs/ReadVariableOp�.dense_62/kernel/Regularizer/Abs/ReadVariableOp� dense_63/StatefulPartitionedCall�,dense_63/bias/Regularizer/Abs/ReadVariableOp�.dense_63/kernel/Regularizer/Abs/ReadVariableOp� dense_64/StatefulPartitionedCall�,dense_64/bias/Regularizer/Abs/ReadVariableOp�.dense_64/kernel/Regularizer/Abs/ReadVariableOp� dense_65/StatefulPartitionedCall�,dense_65/bias/Regularizer/Abs/ReadVariableOp�.dense_65/kernel/Regularizer/Abs/ReadVariableOp� dense_66/StatefulPartitionedCall�,dense_66/bias/Regularizer/Abs/ReadVariableOp�.dense_66/kernel/Regularizer/Abs/ReadVariableOp� dense_67/StatefulPartitionedCall�,dense_67/bias/Regularizer/Abs/ReadVariableOp�.dense_67/kernel/Regularizer/Abs/ReadVariableOp�layer_1/StatefulPartitionedCall�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOp�$layer_output/StatefulPartitionedCall�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOp�
layer_1/StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputlayer_1_12128511layer_1_12128513*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_12128138�
+layer_1/ActivityRegularizer/PartitionedCallPartitionedCall(layer_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *:
f5R3
1__inference_layer_1_activity_regularizer_12128064�
!layer_1/ActivityRegularizer/ShapeShape(layer_1/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��y
/layer_1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1layer_1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1layer_1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)layer_1/ActivityRegularizer/strided_sliceStridedSlice*layer_1/ActivityRegularizer/Shape:output:08layer_1/ActivityRegularizer/strided_slice/stack:output:0:layer_1/ActivityRegularizer/strided_slice/stack_1:output:0:layer_1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
 layer_1/ActivityRegularizer/CastCast2layer_1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
#layer_1/ActivityRegularizer/truedivRealDiv4layer_1/ActivityRegularizer/PartitionedCall:output:0$layer_1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_62/StatefulPartitionedCallStatefulPartitionedCall(layer_1/StatefulPartitionedCall:output:0dense_62_12128524dense_62_12128526*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������c*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_62_layer_call_and_return_conditional_losses_12128174�
,dense_62/ActivityRegularizer/PartitionedCallPartitionedCall)dense_62/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_62_activity_regularizer_12128071�
"dense_62/ActivityRegularizer/ShapeShape)dense_62/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_62/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_62/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_62/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_62/ActivityRegularizer/strided_sliceStridedSlice+dense_62/ActivityRegularizer/Shape:output:09dense_62/ActivityRegularizer/strided_slice/stack:output:0;dense_62/ActivityRegularizer/strided_slice/stack_1:output:0;dense_62/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_62/ActivityRegularizer/CastCast3dense_62/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_62/ActivityRegularizer/truedivRealDiv5dense_62/ActivityRegularizer/PartitionedCall:output:0%dense_62/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_63/StatefulPartitionedCallStatefulPartitionedCall)dense_62/StatefulPartitionedCall:output:0dense_63_12128537dense_63_12128539*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_63_layer_call_and_return_conditional_losses_12128210�
,dense_63/ActivityRegularizer/PartitionedCallPartitionedCall)dense_63/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_63_activity_regularizer_12128078�
"dense_63/ActivityRegularizer/ShapeShape)dense_63/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_63/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_63/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_63/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_63/ActivityRegularizer/strided_sliceStridedSlice+dense_63/ActivityRegularizer/Shape:output:09dense_63/ActivityRegularizer/strided_slice/stack:output:0;dense_63/ActivityRegularizer/strided_slice/stack_1:output:0;dense_63/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_63/ActivityRegularizer/CastCast3dense_63/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_63/ActivityRegularizer/truedivRealDiv5dense_63/ActivityRegularizer/PartitionedCall:output:0%dense_63/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_64/StatefulPartitionedCallStatefulPartitionedCall)dense_63/StatefulPartitionedCall:output:0dense_64_12128550dense_64_12128552*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������$*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_64_layer_call_and_return_conditional_losses_12128246�
,dense_64/ActivityRegularizer/PartitionedCallPartitionedCall)dense_64/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_64_activity_regularizer_12128085�
"dense_64/ActivityRegularizer/ShapeShape)dense_64/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_64/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_64/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_64/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_64/ActivityRegularizer/strided_sliceStridedSlice+dense_64/ActivityRegularizer/Shape:output:09dense_64/ActivityRegularizer/strided_slice/stack:output:0;dense_64/ActivityRegularizer/strided_slice/stack_1:output:0;dense_64/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_64/ActivityRegularizer/CastCast3dense_64/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_64/ActivityRegularizer/truedivRealDiv5dense_64/ActivityRegularizer/PartitionedCall:output:0%dense_64/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_65/StatefulPartitionedCallStatefulPartitionedCall)dense_64/StatefulPartitionedCall:output:0dense_65_12128563dense_65_12128565*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_65_layer_call_and_return_conditional_losses_12128282�
,dense_65/ActivityRegularizer/PartitionedCallPartitionedCall)dense_65/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_65_activity_regularizer_12128092�
"dense_65/ActivityRegularizer/ShapeShape)dense_65/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_65/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_65/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_65/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_65/ActivityRegularizer/strided_sliceStridedSlice+dense_65/ActivityRegularizer/Shape:output:09dense_65/ActivityRegularizer/strided_slice/stack:output:0;dense_65/ActivityRegularizer/strided_slice/stack_1:output:0;dense_65/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_65/ActivityRegularizer/CastCast3dense_65/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_65/ActivityRegularizer/truedivRealDiv5dense_65/ActivityRegularizer/PartitionedCall:output:0%dense_65/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_66/StatefulPartitionedCallStatefulPartitionedCall)dense_65/StatefulPartitionedCall:output:0dense_66_12128576dense_66_12128578*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������X*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_66_layer_call_and_return_conditional_losses_12128318�
,dense_66/ActivityRegularizer/PartitionedCallPartitionedCall)dense_66/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_66_activity_regularizer_12128099�
"dense_66/ActivityRegularizer/ShapeShape)dense_66/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_66/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_66/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_66/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_66/ActivityRegularizer/strided_sliceStridedSlice+dense_66/ActivityRegularizer/Shape:output:09dense_66/ActivityRegularizer/strided_slice/stack:output:0;dense_66/ActivityRegularizer/strided_slice/stack_1:output:0;dense_66/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_66/ActivityRegularizer/CastCast3dense_66/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_66/ActivityRegularizer/truedivRealDiv5dense_66/ActivityRegularizer/PartitionedCall:output:0%dense_66/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_67/StatefulPartitionedCallStatefulPartitionedCall)dense_66/StatefulPartitionedCall:output:0dense_67_12128589dense_67_12128591*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_67_layer_call_and_return_conditional_losses_12128354�
,dense_67/ActivityRegularizer/PartitionedCallPartitionedCall)dense_67/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_67_activity_regularizer_12128106�
"dense_67/ActivityRegularizer/ShapeShape)dense_67/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_67/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_67/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_67/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_67/ActivityRegularizer/strided_sliceStridedSlice+dense_67/ActivityRegularizer/Shape:output:09dense_67/ActivityRegularizer/strided_slice/stack:output:0;dense_67/ActivityRegularizer/strided_slice/stack_1:output:0;dense_67/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_67/ActivityRegularizer/CastCast3dense_67/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_67/ActivityRegularizer/truedivRealDiv5dense_67/ActivityRegularizer/PartitionedCall:output:0%dense_67/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
$layer_output/StatefulPartitionedCallStatefulPartitionedCall)dense_67/StatefulPartitionedCall:output:0layer_output_12128602layer_output_12128604*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_12128389�
0layer_output/ActivityRegularizer/PartitionedCallPartitionedCall-layer_output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *?
f:R8
6__inference_layer_output_activity_regularizer_12128113�
&layer_output/ActivityRegularizer/ShapeShape-layer_output/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��~
4layer_output/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6layer_output/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6layer_output/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.layer_output/ActivityRegularizer/strided_sliceStridedSlice/layer_output/ActivityRegularizer/Shape:output:0=layer_output/ActivityRegularizer/strided_slice/stack:output:0?layer_output/ActivityRegularizer/strided_slice/stack_1:output:0?layer_output/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
%layer_output/ActivityRegularizer/CastCast7layer_output/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
(layer_output/ActivityRegularizer/truedivRealDiv9layer_output/ActivityRegularizer/PartitionedCall:output:0)layer_output/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: ~
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_12128511*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: x
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_12128513*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_62/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_62_12128524*
_output_shapes

:
c*
dtype0�
dense_62/kernel/Regularizer/AbsAbs6dense_62/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
cr
!dense_62/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_62/kernel/Regularizer/SumSum#dense_62/kernel/Regularizer/Abs:y:0*dense_62/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_62/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_62/kernel/Regularizer/mulMul*dense_62/kernel/Regularizer/mul/x:output:0(dense_62/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_62/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_62_12128526*
_output_shapes
:c*
dtype0
dense_62/bias/Regularizer/AbsAbs4dense_62/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:ci
dense_62/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_62/bias/Regularizer/SumSum!dense_62/bias/Regularizer/Abs:y:0(dense_62/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_62/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_62/bias/Regularizer/mulMul(dense_62/bias/Regularizer/mul/x:output:0&dense_62/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_63/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_63_12128537*
_output_shapes

:c*
dtype0�
dense_63/kernel/Regularizer/AbsAbs6dense_63/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:cr
!dense_63/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_63/kernel/Regularizer/SumSum#dense_63/kernel/Regularizer/Abs:y:0*dense_63/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_63/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_63/kernel/Regularizer/mulMul*dense_63/kernel/Regularizer/mul/x:output:0(dense_63/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_63/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_63_12128539*
_output_shapes
:*
dtype0
dense_63/bias/Regularizer/AbsAbs4dense_63/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_63/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_63/bias/Regularizer/SumSum!dense_63/bias/Regularizer/Abs:y:0(dense_63/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_63/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_63/bias/Regularizer/mulMul(dense_63/bias/Regularizer/mul/x:output:0&dense_63/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_64/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_64_12128550*
_output_shapes

:$*
dtype0�
dense_64/kernel/Regularizer/AbsAbs6dense_64/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_64/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_64/kernel/Regularizer/SumSum#dense_64/kernel/Regularizer/Abs:y:0*dense_64/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_64/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_64/kernel/Regularizer/mulMul*dense_64/kernel/Regularizer/mul/x:output:0(dense_64/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_64/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_64_12128552*
_output_shapes
:$*
dtype0
dense_64/bias/Regularizer/AbsAbs4dense_64/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:$i
dense_64/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_64/bias/Regularizer/SumSum!dense_64/bias/Regularizer/Abs:y:0(dense_64/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_64/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_64/bias/Regularizer/mulMul(dense_64/bias/Regularizer/mul/x:output:0&dense_64/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_65/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_65_12128563*
_output_shapes

:$*
dtype0�
dense_65/kernel/Regularizer/AbsAbs6dense_65/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_65/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_65/kernel/Regularizer/SumSum#dense_65/kernel/Regularizer/Abs:y:0*dense_65/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_65/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_65/kernel/Regularizer/mulMul*dense_65/kernel/Regularizer/mul/x:output:0(dense_65/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_65/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_65_12128565*
_output_shapes
:*
dtype0
dense_65/bias/Regularizer/AbsAbs4dense_65/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_65/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_65/bias/Regularizer/SumSum!dense_65/bias/Regularizer/Abs:y:0(dense_65/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_65/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_65/bias/Regularizer/mulMul(dense_65/bias/Regularizer/mul/x:output:0&dense_65/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_66/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_66_12128576*
_output_shapes

:X*
dtype0�
dense_66/kernel/Regularizer/AbsAbs6dense_66/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_66/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_66/kernel/Regularizer/SumSum#dense_66/kernel/Regularizer/Abs:y:0*dense_66/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_66/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_66/kernel/Regularizer/mulMul*dense_66/kernel/Regularizer/mul/x:output:0(dense_66/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_66/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_66_12128578*
_output_shapes
:X*
dtype0
dense_66/bias/Regularizer/AbsAbs4dense_66/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Xi
dense_66/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_66/bias/Regularizer/SumSum!dense_66/bias/Regularizer/Abs:y:0(dense_66/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_66/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_66/bias/Regularizer/mulMul(dense_66/bias/Regularizer/mul/x:output:0&dense_66/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_67/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_67_12128589*
_output_shapes

:X*
dtype0�
dense_67/kernel/Regularizer/AbsAbs6dense_67/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_67/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_67/kernel/Regularizer/SumSum#dense_67/kernel/Regularizer/Abs:y:0*dense_67/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_67/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_67/kernel/Regularizer/mulMul*dense_67/kernel/Regularizer/mul/x:output:0(dense_67/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_67/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_67_12128591*
_output_shapes
:*
dtype0
dense_67/bias/Regularizer/AbsAbs4dense_67/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_67/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_67/bias/Regularizer/SumSum!dense_67/bias/Regularizer/Abs:y:0(dense_67/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_67/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_67/bias/Regularizer/mulMul(dense_67/bias/Regularizer/mul/x:output:0&dense_67/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_12128602*
_output_shapes

:*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_12128604*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
IdentityIdentity-layer_output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������g

Identity_1Identity'layer_1/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_2Identity(dense_62/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_3Identity(dense_63/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_4Identity(dense_64/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_5Identity(dense_65/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_6Identity(dense_66/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_7Identity(dense_67/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: l

Identity_8Identity,layer_output/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp!^dense_62/StatefulPartitionedCall-^dense_62/bias/Regularizer/Abs/ReadVariableOp/^dense_62/kernel/Regularizer/Abs/ReadVariableOp!^dense_63/StatefulPartitionedCall-^dense_63/bias/Regularizer/Abs/ReadVariableOp/^dense_63/kernel/Regularizer/Abs/ReadVariableOp!^dense_64/StatefulPartitionedCall-^dense_64/bias/Regularizer/Abs/ReadVariableOp/^dense_64/kernel/Regularizer/Abs/ReadVariableOp!^dense_65/StatefulPartitionedCall-^dense_65/bias/Regularizer/Abs/ReadVariableOp/^dense_65/kernel/Regularizer/Abs/ReadVariableOp!^dense_66/StatefulPartitionedCall-^dense_66/bias/Regularizer/Abs/ReadVariableOp/^dense_66/kernel/Regularizer/Abs/ReadVariableOp!^dense_67/StatefulPartitionedCall-^dense_67/bias/Regularizer/Abs/ReadVariableOp/^dense_67/kernel/Regularizer/Abs/ReadVariableOp ^layer_1/StatefulPartitionedCall,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp%^layer_output/StatefulPartitionedCall1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0"!

identity_6Identity_6:output:0"!

identity_7Identity_7:output:0"!

identity_8Identity_8:output:0*(
_construction_contextkEagerRuntime*F
_input_shapes5
3:���������: : : : : : : : : : : : : : : : 2D
 dense_62/StatefulPartitionedCall dense_62/StatefulPartitionedCall2\
,dense_62/bias/Regularizer/Abs/ReadVariableOp,dense_62/bias/Regularizer/Abs/ReadVariableOp2`
.dense_62/kernel/Regularizer/Abs/ReadVariableOp.dense_62/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_63/StatefulPartitionedCall dense_63/StatefulPartitionedCall2\
,dense_63/bias/Regularizer/Abs/ReadVariableOp,dense_63/bias/Regularizer/Abs/ReadVariableOp2`
.dense_63/kernel/Regularizer/Abs/ReadVariableOp.dense_63/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_64/StatefulPartitionedCall dense_64/StatefulPartitionedCall2\
,dense_64/bias/Regularizer/Abs/ReadVariableOp,dense_64/bias/Regularizer/Abs/ReadVariableOp2`
.dense_64/kernel/Regularizer/Abs/ReadVariableOp.dense_64/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_65/StatefulPartitionedCall dense_65/StatefulPartitionedCall2\
,dense_65/bias/Regularizer/Abs/ReadVariableOp,dense_65/bias/Regularizer/Abs/ReadVariableOp2`
.dense_65/kernel/Regularizer/Abs/ReadVariableOp.dense_65/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_66/StatefulPartitionedCall dense_66/StatefulPartitionedCall2\
,dense_66/bias/Regularizer/Abs/ReadVariableOp,dense_66/bias/Regularizer/Abs/ReadVariableOp2`
.dense_66/kernel/Regularizer/Abs/ReadVariableOp.dense_66/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_67/StatefulPartitionedCall dense_67/StatefulPartitionedCall2\
,dense_67/bias/Regularizer/Abs/ReadVariableOp,dense_67/bias/Regularizer/Abs/ReadVariableOp2`
.dense_67/kernel/Regularizer/Abs/ReadVariableOp.dense_67/kernel/Regularizer/Abs/ReadVariableOp2B
layer_1/StatefulPartitionedCalllayer_1/StatefulPartitionedCall2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp2L
$layer_output/StatefulPartitionedCall$layer_output/StatefulPartitionedCall2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
12128511:($
"
_user_specified_name
12128513:($
"
_user_specified_name
12128524:($
"
_user_specified_name
12128526:($
"
_user_specified_name
12128537:($
"
_user_specified_name
12128539:($
"
_user_specified_name
12128550:($
"
_user_specified_name
12128552:(	$
"
_user_specified_name
12128563:(
$
"
_user_specified_name
12128565:($
"
_user_specified_name
12128576:($
"
_user_specified_name
12128578:($
"
_user_specified_name
12128589:($
"
_user_specified_name
12128591:($
"
_user_specified_name
12128602:($
"
_user_specified_name
12128604
�
�
__inference_loss_fn_14_12129693M
;layer_output_kernel_regularizer_abs_readvariableop_resource:
identity��2layer_output/kernel/Regularizer/Abs/ReadVariableOp�
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp;layer_output_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: e
IdentityIdentity'layer_output/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: W
NoOpNoOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
__inference_loss_fn_0_12129553H
6layer_1_kernel_regularizer_abs_readvariableop_resource:

identity��-layer_1/kernel/Regularizer/Abs/ReadVariableOp�
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp6layer_1_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: `
IdentityIdentity"layer_1/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: R
NoOpNoOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�]
�
$__inference__traced_restore_12129914
file_prefix1
assignvariableop_layer_1_kernel:
-
assignvariableop_1_layer_1_bias:
4
"assignvariableop_2_dense_62_kernel:
c.
 assignvariableop_3_dense_62_bias:c4
"assignvariableop_4_dense_63_kernel:c.
 assignvariableop_5_dense_63_bias:4
"assignvariableop_6_dense_64_kernel:$.
 assignvariableop_7_dense_64_bias:$4
"assignvariableop_8_dense_65_kernel:$.
 assignvariableop_9_dense_65_bias:5
#assignvariableop_10_dense_66_kernel:X/
!assignvariableop_11_dense_66_bias:X5
#assignvariableop_12_dense_67_kernel:X/
!assignvariableop_13_dense_67_bias:9
'assignvariableop_14_layer_output_kernel:3
%assignvariableop_15_layer_output_bias:'
assignvariableop_16_iteration:	 3
)assignvariableop_17_current_learning_rate: #
assignvariableop_18_total: #
assignvariableop_19_count: 
identity_21��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�	
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-4/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-4/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-5/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-5/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-6/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-6/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-7/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-7/bias/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB;optimizer/_current_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*=
value4B2B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*h
_output_shapesV
T:::::::::::::::::::::*#
dtypes
2	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOpassignvariableop_layer_1_kernelIdentity:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOpassignvariableop_1_layer_1_biasIdentity_1:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp"assignvariableop_2_dense_62_kernelIdentity_2:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOp assignvariableop_3_dense_62_biasIdentity_3:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOp"assignvariableop_4_dense_63_kernelIdentity_4:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOp assignvariableop_5_dense_63_biasIdentity_5:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOp"assignvariableop_6_dense_64_kernelIdentity_6:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOp assignvariableop_7_dense_64_biasIdentity_7:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOp"assignvariableop_8_dense_65_kernelIdentity_8:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp assignvariableop_9_dense_65_biasIdentity_9:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOp#assignvariableop_10_dense_66_kernelIdentity_10:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOp!assignvariableop_11_dense_66_biasIdentity_11:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOp#assignvariableop_12_dense_67_kernelIdentity_12:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOp!assignvariableop_13_dense_67_biasIdentity_13:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOp'assignvariableop_14_layer_output_kernelIdentity_14:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOp%assignvariableop_15_layer_output_biasIdentity_15:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:16"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_16AssignVariableOpassignvariableop_16_iterationIdentity_16:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0	_
Identity_17IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp)assignvariableop_17_current_learning_rateIdentity_17:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOpassignvariableop_18_totalIdentity_18:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOpassignvariableop_19_countIdentity_19:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0Y
NoOpNoOp"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 �
Identity_20Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_21IdentityIdentity_20:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
_output_shapes
 "#
identity_21Identity_21:output:0*(
_construction_contextkEagerRuntime*=
_input_shapes,
*: : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:.*
(
_user_specified_namelayer_1/kernel:,(
&
_user_specified_namelayer_1/bias:/+
)
_user_specified_namedense_62/kernel:-)
'
_user_specified_namedense_62/bias:/+
)
_user_specified_namedense_63/kernel:-)
'
_user_specified_namedense_63/bias:/+
)
_user_specified_namedense_64/kernel:-)
'
_user_specified_namedense_64/bias:/	+
)
_user_specified_namedense_65/kernel:-
)
'
_user_specified_namedense_65/bias:/+
)
_user_specified_namedense_66/kernel:-)
'
_user_specified_namedense_66/bias:/+
)
_user_specified_namedense_67/kernel:-)
'
_user_specified_namedense_67/bias:3/
-
_user_specified_namelayer_output/kernel:1-
+
_user_specified_namelayer_output/bias:)%
#
_user_specified_name	iteration:51
/
_user_specified_namecurrent_learning_rate:%!

_user_specified_nametotal:%!

_user_specified_namecount
�
�
+__inference_dense_63_layer_call_fn_12129295

inputs
unknown:c
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_63_layer_call_and_return_conditional_losses_12128210o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������c: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������c
 
_user_specified_nameinputs:($
"
_user_specified_name
12129289:($
"
_user_specified_name
12129291
�
�
J__inference_layer_output_layer_call_and_return_conditional_losses_12129543

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128508
layer_1_input"
layer_1_12128139:

layer_1_12128141:
#
dense_62_12128175:
c
dense_62_12128177:c#
dense_63_12128211:c
dense_63_12128213:#
dense_64_12128247:$
dense_64_12128249:$#
dense_65_12128283:$
dense_65_12128285:#
dense_66_12128319:X
dense_66_12128321:X#
dense_67_12128355:X
dense_67_12128357:'
layer_output_12128390:#
layer_output_12128392:
identity

identity_1

identity_2

identity_3

identity_4

identity_5

identity_6

identity_7

identity_8�� dense_62/StatefulPartitionedCall�,dense_62/bias/Regularizer/Abs/ReadVariableOp�.dense_62/kernel/Regularizer/Abs/ReadVariableOp� dense_63/StatefulPartitionedCall�,dense_63/bias/Regularizer/Abs/ReadVariableOp�.dense_63/kernel/Regularizer/Abs/ReadVariableOp� dense_64/StatefulPartitionedCall�,dense_64/bias/Regularizer/Abs/ReadVariableOp�.dense_64/kernel/Regularizer/Abs/ReadVariableOp� dense_65/StatefulPartitionedCall�,dense_65/bias/Regularizer/Abs/ReadVariableOp�.dense_65/kernel/Regularizer/Abs/ReadVariableOp� dense_66/StatefulPartitionedCall�,dense_66/bias/Regularizer/Abs/ReadVariableOp�.dense_66/kernel/Regularizer/Abs/ReadVariableOp� dense_67/StatefulPartitionedCall�,dense_67/bias/Regularizer/Abs/ReadVariableOp�.dense_67/kernel/Regularizer/Abs/ReadVariableOp�layer_1/StatefulPartitionedCall�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOp�$layer_output/StatefulPartitionedCall�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOp�
layer_1/StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputlayer_1_12128139layer_1_12128141*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_12128138�
+layer_1/ActivityRegularizer/PartitionedCallPartitionedCall(layer_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *:
f5R3
1__inference_layer_1_activity_regularizer_12128064�
!layer_1/ActivityRegularizer/ShapeShape(layer_1/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��y
/layer_1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1layer_1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1layer_1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)layer_1/ActivityRegularizer/strided_sliceStridedSlice*layer_1/ActivityRegularizer/Shape:output:08layer_1/ActivityRegularizer/strided_slice/stack:output:0:layer_1/ActivityRegularizer/strided_slice/stack_1:output:0:layer_1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
 layer_1/ActivityRegularizer/CastCast2layer_1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
#layer_1/ActivityRegularizer/truedivRealDiv4layer_1/ActivityRegularizer/PartitionedCall:output:0$layer_1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_62/StatefulPartitionedCallStatefulPartitionedCall(layer_1/StatefulPartitionedCall:output:0dense_62_12128175dense_62_12128177*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������c*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_62_layer_call_and_return_conditional_losses_12128174�
,dense_62/ActivityRegularizer/PartitionedCallPartitionedCall)dense_62/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_62_activity_regularizer_12128071�
"dense_62/ActivityRegularizer/ShapeShape)dense_62/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_62/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_62/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_62/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_62/ActivityRegularizer/strided_sliceStridedSlice+dense_62/ActivityRegularizer/Shape:output:09dense_62/ActivityRegularizer/strided_slice/stack:output:0;dense_62/ActivityRegularizer/strided_slice/stack_1:output:0;dense_62/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_62/ActivityRegularizer/CastCast3dense_62/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_62/ActivityRegularizer/truedivRealDiv5dense_62/ActivityRegularizer/PartitionedCall:output:0%dense_62/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_63/StatefulPartitionedCallStatefulPartitionedCall)dense_62/StatefulPartitionedCall:output:0dense_63_12128211dense_63_12128213*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_63_layer_call_and_return_conditional_losses_12128210�
,dense_63/ActivityRegularizer/PartitionedCallPartitionedCall)dense_63/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_63_activity_regularizer_12128078�
"dense_63/ActivityRegularizer/ShapeShape)dense_63/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_63/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_63/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_63/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_63/ActivityRegularizer/strided_sliceStridedSlice+dense_63/ActivityRegularizer/Shape:output:09dense_63/ActivityRegularizer/strided_slice/stack:output:0;dense_63/ActivityRegularizer/strided_slice/stack_1:output:0;dense_63/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_63/ActivityRegularizer/CastCast3dense_63/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_63/ActivityRegularizer/truedivRealDiv5dense_63/ActivityRegularizer/PartitionedCall:output:0%dense_63/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_64/StatefulPartitionedCallStatefulPartitionedCall)dense_63/StatefulPartitionedCall:output:0dense_64_12128247dense_64_12128249*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������$*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_64_layer_call_and_return_conditional_losses_12128246�
,dense_64/ActivityRegularizer/PartitionedCallPartitionedCall)dense_64/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_64_activity_regularizer_12128085�
"dense_64/ActivityRegularizer/ShapeShape)dense_64/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_64/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_64/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_64/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_64/ActivityRegularizer/strided_sliceStridedSlice+dense_64/ActivityRegularizer/Shape:output:09dense_64/ActivityRegularizer/strided_slice/stack:output:0;dense_64/ActivityRegularizer/strided_slice/stack_1:output:0;dense_64/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_64/ActivityRegularizer/CastCast3dense_64/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_64/ActivityRegularizer/truedivRealDiv5dense_64/ActivityRegularizer/PartitionedCall:output:0%dense_64/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_65/StatefulPartitionedCallStatefulPartitionedCall)dense_64/StatefulPartitionedCall:output:0dense_65_12128283dense_65_12128285*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_65_layer_call_and_return_conditional_losses_12128282�
,dense_65/ActivityRegularizer/PartitionedCallPartitionedCall)dense_65/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_65_activity_regularizer_12128092�
"dense_65/ActivityRegularizer/ShapeShape)dense_65/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_65/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_65/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_65/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_65/ActivityRegularizer/strided_sliceStridedSlice+dense_65/ActivityRegularizer/Shape:output:09dense_65/ActivityRegularizer/strided_slice/stack:output:0;dense_65/ActivityRegularizer/strided_slice/stack_1:output:0;dense_65/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_65/ActivityRegularizer/CastCast3dense_65/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_65/ActivityRegularizer/truedivRealDiv5dense_65/ActivityRegularizer/PartitionedCall:output:0%dense_65/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_66/StatefulPartitionedCallStatefulPartitionedCall)dense_65/StatefulPartitionedCall:output:0dense_66_12128319dense_66_12128321*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������X*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_66_layer_call_and_return_conditional_losses_12128318�
,dense_66/ActivityRegularizer/PartitionedCallPartitionedCall)dense_66/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_66_activity_regularizer_12128099�
"dense_66/ActivityRegularizer/ShapeShape)dense_66/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_66/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_66/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_66/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_66/ActivityRegularizer/strided_sliceStridedSlice+dense_66/ActivityRegularizer/Shape:output:09dense_66/ActivityRegularizer/strided_slice/stack:output:0;dense_66/ActivityRegularizer/strided_slice/stack_1:output:0;dense_66/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_66/ActivityRegularizer/CastCast3dense_66/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_66/ActivityRegularizer/truedivRealDiv5dense_66/ActivityRegularizer/PartitionedCall:output:0%dense_66/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
 dense_67/StatefulPartitionedCallStatefulPartitionedCall)dense_66/StatefulPartitionedCall:output:0dense_67_12128355dense_67_12128357*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_67_layer_call_and_return_conditional_losses_12128354�
,dense_67/ActivityRegularizer/PartitionedCallPartitionedCall)dense_67/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_67_activity_regularizer_12128106�
"dense_67/ActivityRegularizer/ShapeShape)dense_67/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��z
0dense_67/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: |
2dense_67/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:|
2dense_67/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
*dense_67/ActivityRegularizer/strided_sliceStridedSlice+dense_67/ActivityRegularizer/Shape:output:09dense_67/ActivityRegularizer/strided_slice/stack:output:0;dense_67/ActivityRegularizer/strided_slice/stack_1:output:0;dense_67/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
!dense_67/ActivityRegularizer/CastCast3dense_67/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
$dense_67/ActivityRegularizer/truedivRealDiv5dense_67/ActivityRegularizer/PartitionedCall:output:0%dense_67/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
$layer_output/StatefulPartitionedCallStatefulPartitionedCall)dense_67/StatefulPartitionedCall:output:0layer_output_12128390layer_output_12128392*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_12128389�
0layer_output/ActivityRegularizer/PartitionedCallPartitionedCall-layer_output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *?
f:R8
6__inference_layer_output_activity_regularizer_12128113�
&layer_output/ActivityRegularizer/ShapeShape-layer_output/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��~
4layer_output/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6layer_output/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6layer_output/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.layer_output/ActivityRegularizer/strided_sliceStridedSlice/layer_output/ActivityRegularizer/Shape:output:0=layer_output/ActivityRegularizer/strided_slice/stack:output:0?layer_output/ActivityRegularizer/strided_slice/stack_1:output:0?layer_output/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
%layer_output/ActivityRegularizer/CastCast7layer_output/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
(layer_output/ActivityRegularizer/truedivRealDiv9layer_output/ActivityRegularizer/PartitionedCall:output:0)layer_output/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: ~
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_12128139*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: x
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_12128141*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_62/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_62_12128175*
_output_shapes

:
c*
dtype0�
dense_62/kernel/Regularizer/AbsAbs6dense_62/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
cr
!dense_62/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_62/kernel/Regularizer/SumSum#dense_62/kernel/Regularizer/Abs:y:0*dense_62/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_62/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_62/kernel/Regularizer/mulMul*dense_62/kernel/Regularizer/mul/x:output:0(dense_62/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_62/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_62_12128177*
_output_shapes
:c*
dtype0
dense_62/bias/Regularizer/AbsAbs4dense_62/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:ci
dense_62/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_62/bias/Regularizer/SumSum!dense_62/bias/Regularizer/Abs:y:0(dense_62/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_62/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_62/bias/Regularizer/mulMul(dense_62/bias/Regularizer/mul/x:output:0&dense_62/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_63/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_63_12128211*
_output_shapes

:c*
dtype0�
dense_63/kernel/Regularizer/AbsAbs6dense_63/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:cr
!dense_63/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_63/kernel/Regularizer/SumSum#dense_63/kernel/Regularizer/Abs:y:0*dense_63/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_63/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_63/kernel/Regularizer/mulMul*dense_63/kernel/Regularizer/mul/x:output:0(dense_63/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_63/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_63_12128213*
_output_shapes
:*
dtype0
dense_63/bias/Regularizer/AbsAbs4dense_63/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_63/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_63/bias/Regularizer/SumSum!dense_63/bias/Regularizer/Abs:y:0(dense_63/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_63/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_63/bias/Regularizer/mulMul(dense_63/bias/Regularizer/mul/x:output:0&dense_63/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_64/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_64_12128247*
_output_shapes

:$*
dtype0�
dense_64/kernel/Regularizer/AbsAbs6dense_64/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_64/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_64/kernel/Regularizer/SumSum#dense_64/kernel/Regularizer/Abs:y:0*dense_64/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_64/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_64/kernel/Regularizer/mulMul*dense_64/kernel/Regularizer/mul/x:output:0(dense_64/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_64/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_64_12128249*
_output_shapes
:$*
dtype0
dense_64/bias/Regularizer/AbsAbs4dense_64/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:$i
dense_64/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_64/bias/Regularizer/SumSum!dense_64/bias/Regularizer/Abs:y:0(dense_64/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_64/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_64/bias/Regularizer/mulMul(dense_64/bias/Regularizer/mul/x:output:0&dense_64/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_65/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_65_12128283*
_output_shapes

:$*
dtype0�
dense_65/kernel/Regularizer/AbsAbs6dense_65/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_65/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_65/kernel/Regularizer/SumSum#dense_65/kernel/Regularizer/Abs:y:0*dense_65/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_65/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_65/kernel/Regularizer/mulMul*dense_65/kernel/Regularizer/mul/x:output:0(dense_65/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_65/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_65_12128285*
_output_shapes
:*
dtype0
dense_65/bias/Regularizer/AbsAbs4dense_65/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_65/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_65/bias/Regularizer/SumSum!dense_65/bias/Regularizer/Abs:y:0(dense_65/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_65/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_65/bias/Regularizer/mulMul(dense_65/bias/Regularizer/mul/x:output:0&dense_65/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_66/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_66_12128319*
_output_shapes

:X*
dtype0�
dense_66/kernel/Regularizer/AbsAbs6dense_66/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_66/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_66/kernel/Regularizer/SumSum#dense_66/kernel/Regularizer/Abs:y:0*dense_66/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_66/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_66/kernel/Regularizer/mulMul*dense_66/kernel/Regularizer/mul/x:output:0(dense_66/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_66/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_66_12128321*
_output_shapes
:X*
dtype0
dense_66/bias/Regularizer/AbsAbs4dense_66/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Xi
dense_66/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_66/bias/Regularizer/SumSum!dense_66/bias/Regularizer/Abs:y:0(dense_66/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_66/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_66/bias/Regularizer/mulMul(dense_66/bias/Regularizer/mul/x:output:0&dense_66/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
.dense_67/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_67_12128355*
_output_shapes

:X*
dtype0�
dense_67/kernel/Regularizer/AbsAbs6dense_67/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_67/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_67/kernel/Regularizer/SumSum#dense_67/kernel/Regularizer/Abs:y:0*dense_67/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_67/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_67/kernel/Regularizer/mulMul*dense_67/kernel/Regularizer/mul/x:output:0(dense_67/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: z
,dense_67/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_67_12128357*
_output_shapes
:*
dtype0
dense_67/bias/Regularizer/AbsAbs4dense_67/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_67/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_67/bias/Regularizer/SumSum!dense_67/bias/Regularizer/Abs:y:0(dense_67/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_67/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_67/bias/Regularizer/mulMul(dense_67/bias/Regularizer/mul/x:output:0&dense_67/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_12128390*
_output_shapes

:*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_12128392*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
IdentityIdentity-layer_output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������g

Identity_1Identity'layer_1/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_2Identity(dense_62/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_3Identity(dense_63/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_4Identity(dense_64/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_5Identity(dense_65/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_6Identity(dense_66/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: h

Identity_7Identity(dense_67/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: l

Identity_8Identity,layer_output/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp!^dense_62/StatefulPartitionedCall-^dense_62/bias/Regularizer/Abs/ReadVariableOp/^dense_62/kernel/Regularizer/Abs/ReadVariableOp!^dense_63/StatefulPartitionedCall-^dense_63/bias/Regularizer/Abs/ReadVariableOp/^dense_63/kernel/Regularizer/Abs/ReadVariableOp!^dense_64/StatefulPartitionedCall-^dense_64/bias/Regularizer/Abs/ReadVariableOp/^dense_64/kernel/Regularizer/Abs/ReadVariableOp!^dense_65/StatefulPartitionedCall-^dense_65/bias/Regularizer/Abs/ReadVariableOp/^dense_65/kernel/Regularizer/Abs/ReadVariableOp!^dense_66/StatefulPartitionedCall-^dense_66/bias/Regularizer/Abs/ReadVariableOp/^dense_66/kernel/Regularizer/Abs/ReadVariableOp!^dense_67/StatefulPartitionedCall-^dense_67/bias/Regularizer/Abs/ReadVariableOp/^dense_67/kernel/Regularizer/Abs/ReadVariableOp ^layer_1/StatefulPartitionedCall,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp%^layer_output/StatefulPartitionedCall1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0"!

identity_6Identity_6:output:0"!

identity_7Identity_7:output:0"!

identity_8Identity_8:output:0*(
_construction_contextkEagerRuntime*F
_input_shapes5
3:���������: : : : : : : : : : : : : : : : 2D
 dense_62/StatefulPartitionedCall dense_62/StatefulPartitionedCall2\
,dense_62/bias/Regularizer/Abs/ReadVariableOp,dense_62/bias/Regularizer/Abs/ReadVariableOp2`
.dense_62/kernel/Regularizer/Abs/ReadVariableOp.dense_62/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_63/StatefulPartitionedCall dense_63/StatefulPartitionedCall2\
,dense_63/bias/Regularizer/Abs/ReadVariableOp,dense_63/bias/Regularizer/Abs/ReadVariableOp2`
.dense_63/kernel/Regularizer/Abs/ReadVariableOp.dense_63/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_64/StatefulPartitionedCall dense_64/StatefulPartitionedCall2\
,dense_64/bias/Regularizer/Abs/ReadVariableOp,dense_64/bias/Regularizer/Abs/ReadVariableOp2`
.dense_64/kernel/Regularizer/Abs/ReadVariableOp.dense_64/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_65/StatefulPartitionedCall dense_65/StatefulPartitionedCall2\
,dense_65/bias/Regularizer/Abs/ReadVariableOp,dense_65/bias/Regularizer/Abs/ReadVariableOp2`
.dense_65/kernel/Regularizer/Abs/ReadVariableOp.dense_65/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_66/StatefulPartitionedCall dense_66/StatefulPartitionedCall2\
,dense_66/bias/Regularizer/Abs/ReadVariableOp,dense_66/bias/Regularizer/Abs/ReadVariableOp2`
.dense_66/kernel/Regularizer/Abs/ReadVariableOp.dense_66/kernel/Regularizer/Abs/ReadVariableOp2D
 dense_67/StatefulPartitionedCall dense_67/StatefulPartitionedCall2\
,dense_67/bias/Regularizer/Abs/ReadVariableOp,dense_67/bias/Regularizer/Abs/ReadVariableOp2`
.dense_67/kernel/Regularizer/Abs/ReadVariableOp.dense_67/kernel/Regularizer/Abs/ReadVariableOp2B
layer_1/StatefulPartitionedCalllayer_1/StatefulPartitionedCall2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp2L
$layer_output/StatefulPartitionedCall$layer_output/StatefulPartitionedCall2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
12128139:($
"
_user_specified_name
12128141:($
"
_user_specified_name
12128175:($
"
_user_specified_name
12128177:($
"
_user_specified_name
12128211:($
"
_user_specified_name
12128213:($
"
_user_specified_name
12128247:($
"
_user_specified_name
12128249:(	$
"
_user_specified_name
12128283:(
$
"
_user_specified_name
12128285:($
"
_user_specified_name
12128319:($
"
_user_specified_name
12128321:($
"
_user_specified_name
12128355:($
"
_user_specified_name
12128357:($
"
_user_specified_name
12128390:($
"
_user_specified_name
12128392
�
�
0__inference_sequential_15_layer_call_fn_12128765
layer_1_input
unknown:

	unknown_0:

	unknown_1:
c
	unknown_2:c
	unknown_3:c
	unknown_4:
	unknown_5:$
	unknown_6:$
	unknown_7:$
	unknown_8:
	unknown_9:X

unknown_10:X

unknown_11:X

unknown_12:

unknown_13:

unknown_14:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14*
Tin
2*
Tout
2	*
_collective_manager_ids
 *7
_output_shapes%
#:���������: : : : : : : : *2
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128508o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*F
_input_shapes5
3:���������: : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
12128723:($
"
_user_specified_name
12128725:($
"
_user_specified_name
12128727:($
"
_user_specified_name
12128729:($
"
_user_specified_name
12128731:($
"
_user_specified_name
12128733:($
"
_user_specified_name
12128735:($
"
_user_specified_name
12128737:(	$
"
_user_specified_name
12128739:(
$
"
_user_specified_name
12128741:($
"
_user_specified_name
12128743:($
"
_user_specified_name
12128745:($
"
_user_specified_name
12128747:($
"
_user_specified_name
12128749:($
"
_user_specified_name
12128751:($
"
_user_specified_name
12128753
�
�
__inference_loss_fn_12_12129673I
7dense_67_kernel_regularizer_abs_readvariableop_resource:X
identity��.dense_67/kernel/Regularizer/Abs/ReadVariableOp�
.dense_67/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp7dense_67_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:X*
dtype0�
dense_67/kernel/Regularizer/AbsAbs6dense_67/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_67/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_67/kernel/Regularizer/SumSum#dense_67/kernel/Regularizer/Abs:y:0*dense_67/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_67/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_67/kernel/Regularizer/mulMul*dense_67/kernel/Regularizer/mul/x:output:0(dense_67/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentity#dense_67/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp/^dense_67/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2`
.dense_67/kernel/Regularizer/Abs/ReadVariableOp.dense_67/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
H
1__inference_layer_1_activity_regularizer_12128064
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
+__inference_dense_64_layer_call_fn_12129338

inputs
unknown:$
	unknown_0:$
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������$*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_64_layer_call_and_return_conditional_losses_12128246o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������$<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129332:($
"
_user_specified_name
12129334
�
�
F__inference_dense_65_layer_call_and_return_conditional_losses_12129415

inputs0
matmul_readvariableop_resource:$-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_65/bias/Regularizer/Abs/ReadVariableOp�.dense_65/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:����������
.dense_65/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0�
dense_65/kernel/Regularizer/AbsAbs6dense_65/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_65/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_65/kernel/Regularizer/SumSum#dense_65/kernel/Regularizer/Abs:y:0*dense_65/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_65/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_65/kernel/Regularizer/mulMul*dense_65/kernel/Regularizer/mul/x:output:0(dense_65/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_65/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
dense_65/bias/Regularizer/AbsAbs4dense_65/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_65/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_65/bias/Regularizer/SumSum!dense_65/bias/Regularizer/Abs:y:0(dense_65/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_65/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_65/bias/Regularizer/mulMul(dense_65/bias/Regularizer/mul/x:output:0&dense_65/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_65/bias/Regularizer/Abs/ReadVariableOp/^dense_65/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������$: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_65/bias/Regularizer/Abs/ReadVariableOp,dense_65/bias/Regularizer/Abs/ReadVariableOp2`
.dense_65/kernel/Regularizer/Abs/ReadVariableOp.dense_65/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������$
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
__inference_loss_fn_4_12129593I
7dense_63_kernel_regularizer_abs_readvariableop_resource:c
identity��.dense_63/kernel/Regularizer/Abs/ReadVariableOp�
.dense_63/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp7dense_63_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:c*
dtype0�
dense_63/kernel/Regularizer/AbsAbs6dense_63/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:cr
!dense_63/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_63/kernel/Regularizer/SumSum#dense_63/kernel/Regularizer/Abs:y:0*dense_63/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_63/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_63/kernel/Regularizer/mulMul*dense_63/kernel/Regularizer/mul/x:output:0(dense_63/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentity#dense_63/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp/^dense_63/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2`
.dense_63/kernel/Regularizer/Abs/ReadVariableOp.dense_63/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
F__inference_dense_66_layer_call_and_return_conditional_losses_12128318

inputs0
matmul_readvariableop_resource:X-
biasadd_readvariableop_resource:X
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_66/bias/Regularizer/Abs/ReadVariableOp�.dense_66/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Xr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:X*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������XP
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������X�
.dense_66/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0�
dense_66/kernel/Regularizer/AbsAbs6dense_66/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_66/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_66/kernel/Regularizer/SumSum#dense_66/kernel/Regularizer/Abs:y:0*dense_66/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_66/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_66/kernel/Regularizer/mulMul*dense_66/kernel/Regularizer/mul/x:output:0(dense_66/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_66/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:X*
dtype0
dense_66/bias/Regularizer/AbsAbs4dense_66/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Xi
dense_66/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_66/bias/Regularizer/SumSum!dense_66/bias/Regularizer/Abs:y:0(dense_66/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_66/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_66/bias/Regularizer/mulMul(dense_66/bias/Regularizer/mul/x:output:0&dense_66/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������X�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_66/bias/Regularizer/Abs/ReadVariableOp/^dense_66/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_66/bias/Regularizer/Abs/ReadVariableOp,dense_66/bias/Regularizer/Abs/ReadVariableOp2`
.dense_66/kernel/Regularizer/Abs/ReadVariableOp.dense_66/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
J__inference_dense_64_layer_call_and_return_all_conditional_losses_12129349

inputs
unknown:$
	unknown_0:$
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������$*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_64_layer_call_and_return_conditional_losses_12128246�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_64_activity_regularizer_12128085o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������$X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129341:($
"
_user_specified_name
12129343
�
�
__inference_loss_fn_10_12129653I
7dense_66_kernel_regularizer_abs_readvariableop_resource:X
identity��.dense_66/kernel/Regularizer/Abs/ReadVariableOp�
.dense_66/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp7dense_66_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:X*
dtype0�
dense_66/kernel/Regularizer/AbsAbs6dense_66/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_66/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_66/kernel/Regularizer/SumSum#dense_66/kernel/Regularizer/Abs:y:0*dense_66/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_66/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_66/kernel/Regularizer/mulMul*dense_66/kernel/Regularizer/mul/x:output:0(dense_66/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentity#dense_66/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp/^dense_66/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2`
.dense_66/kernel/Regularizer/Abs/ReadVariableOp.dense_66/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
I
2__inference_dense_65_activity_regularizer_12128092
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
F__inference_dense_63_layer_call_and_return_conditional_losses_12128210

inputs0
matmul_readvariableop_resource:c-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_63/bias/Regularizer/Abs/ReadVariableOp�.dense_63/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:c*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:����������
.dense_63/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:c*
dtype0�
dense_63/kernel/Regularizer/AbsAbs6dense_63/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:cr
!dense_63/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_63/kernel/Regularizer/SumSum#dense_63/kernel/Regularizer/Abs:y:0*dense_63/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_63/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_63/kernel/Regularizer/mulMul*dense_63/kernel/Regularizer/mul/x:output:0(dense_63/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_63/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
dense_63/bias/Regularizer/AbsAbs4dense_63/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_63/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_63/bias/Regularizer/SumSum!dense_63/bias/Regularizer/Abs:y:0(dense_63/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_63/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_63/bias/Regularizer/mulMul(dense_63/bias/Regularizer/mul/x:output:0&dense_63/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_63/bias/Regularizer/Abs/ReadVariableOp/^dense_63/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������c: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_63/bias/Regularizer/Abs/ReadVariableOp,dense_63/bias/Regularizer/Abs/ReadVariableOp2`
.dense_63/kernel/Regularizer/Abs/ReadVariableOp.dense_63/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������c
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
+__inference_dense_66_layer_call_fn_12129424

inputs
unknown:X
	unknown_0:X
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������X*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_66_layer_call_and_return_conditional_losses_12128318o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������X<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129418:($
"
_user_specified_name
12129420
�
�
__inference_loss_fn_8_12129633I
7dense_65_kernel_regularizer_abs_readvariableop_resource:$
identity��.dense_65/kernel/Regularizer/Abs/ReadVariableOp�
.dense_65/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp7dense_65_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:$*
dtype0�
dense_65/kernel/Regularizer/AbsAbs6dense_65/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_65/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_65/kernel/Regularizer/SumSum#dense_65/kernel/Regularizer/Abs:y:0*dense_65/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_65/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_65/kernel/Regularizer/mulMul*dense_65/kernel/Regularizer/mul/x:output:0(dense_65/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentity#dense_65/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp/^dense_65/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2`
.dense_65/kernel/Regularizer/Abs/ReadVariableOp.dense_65/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�

�
__inference_loss_fn_7_12129623C
5dense_64_bias_regularizer_abs_readvariableop_resource:$
identity��,dense_64/bias/Regularizer/Abs/ReadVariableOp�
,dense_64/bias/Regularizer/Abs/ReadVariableOpReadVariableOp5dense_64_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:$*
dtype0
dense_64/bias/Regularizer/AbsAbs4dense_64/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:$i
dense_64/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_64/bias/Regularizer/SumSum!dense_64/bias/Regularizer/Abs:y:0(dense_64/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_64/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_64/bias/Regularizer/mulMul(dense_64/bias/Regularizer/mul/x:output:0&dense_64/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentity!dense_64/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: Q
NoOpNoOp-^dense_64/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2\
,dense_64/bias/Regularizer/Abs/ReadVariableOp,dense_64/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
F__inference_dense_64_layer_call_and_return_conditional_losses_12128246

inputs0
matmul_readvariableop_resource:$-
biasadd_readvariableop_resource:$
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_64/bias/Regularizer/Abs/ReadVariableOp�.dense_64/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������$r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:$*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������$P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������$�
.dense_64/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0�
dense_64/kernel/Regularizer/AbsAbs6dense_64/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_64/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_64/kernel/Regularizer/SumSum#dense_64/kernel/Regularizer/Abs:y:0*dense_64/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_64/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_64/kernel/Regularizer/mulMul*dense_64/kernel/Regularizer/mul/x:output:0(dense_64/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_64/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:$*
dtype0
dense_64/bias/Regularizer/AbsAbs4dense_64/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:$i
dense_64/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_64/bias/Regularizer/SumSum!dense_64/bias/Regularizer/Abs:y:0(dense_64/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_64/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_64/bias/Regularizer/mulMul(dense_64/bias/Regularizer/mul/x:output:0&dense_64/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������$�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_64/bias/Regularizer/Abs/ReadVariableOp/^dense_64/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_64/bias/Regularizer/Abs/ReadVariableOp,dense_64/bias/Regularizer/Abs/ReadVariableOp2`
.dense_64/kernel/Regularizer/Abs/ReadVariableOp.dense_64/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
J__inference_dense_62_layer_call_and_return_all_conditional_losses_12129263

inputs
unknown:
c
	unknown_0:c
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������c*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_62_layer_call_and_return_conditional_losses_12128174�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_62_activity_regularizer_12128071o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������cX

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
12129255:($
"
_user_specified_name
12129257
�
I
2__inference_dense_66_activity_regularizer_12128099
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
I
2__inference_dense_67_activity_regularizer_12128106
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
I
2__inference_dense_64_activity_regularizer_12128085
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
F__inference_dense_64_layer_call_and_return_conditional_losses_12129372

inputs0
matmul_readvariableop_resource:$-
biasadd_readvariableop_resource:$
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_64/bias/Regularizer/Abs/ReadVariableOp�.dense_64/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������$r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:$*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������$P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������$�
.dense_64/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:$*
dtype0�
dense_64/kernel/Regularizer/AbsAbs6dense_64/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_64/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_64/kernel/Regularizer/SumSum#dense_64/kernel/Regularizer/Abs:y:0*dense_64/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_64/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_64/kernel/Regularizer/mulMul*dense_64/kernel/Regularizer/mul/x:output:0(dense_64/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_64/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:$*
dtype0
dense_64/bias/Regularizer/AbsAbs4dense_64/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:$i
dense_64/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_64/bias/Regularizer/SumSum!dense_64/bias/Regularizer/Abs:y:0(dense_64/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_64/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_64/bias/Regularizer/mulMul(dense_64/bias/Regularizer/mul/x:output:0&dense_64/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������$�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_64/bias/Regularizer/Abs/ReadVariableOp/^dense_64/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_64/bias/Regularizer/Abs/ReadVariableOp,dense_64/bias/Regularizer/Abs/ReadVariableOp2`
.dense_64/kernel/Regularizer/Abs/ReadVariableOp.dense_64/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
+__inference_dense_67_layer_call_fn_12129467

inputs
unknown:X
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_67_layer_call_and_return_conditional_losses_12128354o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������X: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������X
 
_user_specified_nameinputs:($
"
_user_specified_name
12129461:($
"
_user_specified_name
12129463
�
�
J__inference_layer_output_layer_call_and_return_conditional_losses_12128389

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
F__inference_dense_66_layer_call_and_return_conditional_losses_12129458

inputs0
matmul_readvariableop_resource:X-
biasadd_readvariableop_resource:X
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_66/bias/Regularizer/Abs/ReadVariableOp�.dense_66/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Xr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:X*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������XP
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������X�
.dense_66/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0�
dense_66/kernel/Regularizer/AbsAbs6dense_66/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_66/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_66/kernel/Regularizer/SumSum#dense_66/kernel/Regularizer/Abs:y:0*dense_66/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_66/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_66/kernel/Regularizer/mulMul*dense_66/kernel/Regularizer/mul/x:output:0(dense_66/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_66/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:X*
dtype0
dense_66/bias/Regularizer/AbsAbs4dense_66/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Xi
dense_66/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_66/bias/Regularizer/SumSum!dense_66/bias/Regularizer/Abs:y:0(dense_66/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_66/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_66/bias/Regularizer/mulMul(dense_66/bias/Regularizer/mul/x:output:0&dense_66/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������X�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_66/bias/Regularizer/Abs/ReadVariableOp/^dense_66/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_66/bias/Regularizer/Abs/ReadVariableOp,dense_66/bias/Regularizer/Abs/ReadVariableOp2`
.dense_66/kernel/Regularizer/Abs/ReadVariableOp.dense_66/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
__inference_loss_fn_6_12129613I
7dense_64_kernel_regularizer_abs_readvariableop_resource:$
identity��.dense_64/kernel/Regularizer/Abs/ReadVariableOp�
.dense_64/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp7dense_64_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:$*
dtype0�
dense_64/kernel/Regularizer/AbsAbs6dense_64/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:$r
!dense_64/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_64/kernel/Regularizer/SumSum#dense_64/kernel/Regularizer/Abs:y:0*dense_64/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_64/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_64/kernel/Regularizer/mulMul*dense_64/kernel/Regularizer/mul/x:output:0(dense_64/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentity#dense_64/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp/^dense_64/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2`
.dense_64/kernel/Regularizer/Abs/ReadVariableOp.dense_64/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
*__inference_layer_1_layer_call_fn_12129209

inputs
unknown:

	unknown_0:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_12128138o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129203:($
"
_user_specified_name
12129205
�
I
2__inference_dense_63_activity_regularizer_12128078
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
+__inference_dense_65_layer_call_fn_12129381

inputs
unknown:$
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_65_layer_call_and_return_conditional_losses_12128282o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������$: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������$
 
_user_specified_nameinputs:($
"
_user_specified_name
12129375:($
"
_user_specified_name
12129377
�
�
0__inference_sequential_15_layer_call_fn_12128810
layer_1_input
unknown:

	unknown_0:

	unknown_1:
c
	unknown_2:c
	unknown_3:c
	unknown_4:
	unknown_5:$
	unknown_6:$
	unknown_7:$
	unknown_8:
	unknown_9:X

unknown_10:X

unknown_11:X

unknown_12:

unknown_13:

unknown_14:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6	unknown_7	unknown_8	unknown_9
unknown_10
unknown_11
unknown_12
unknown_13
unknown_14*
Tin
2*
Tout
2	*
_collective_manager_ids
 *7
_output_shapes%
#:���������: : : : : : : : *2
_read_only_resource_inputs
	
*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128720o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*F
_input_shapes5
3:���������: : : : : : : : : : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
12128768:($
"
_user_specified_name
12128770:($
"
_user_specified_name
12128772:($
"
_user_specified_name
12128774:($
"
_user_specified_name
12128776:($
"
_user_specified_name
12128778:($
"
_user_specified_name
12128780:($
"
_user_specified_name
12128782:(	$
"
_user_specified_name
12128784:(
$
"
_user_specified_name
12128786:($
"
_user_specified_name
12128788:($
"
_user_specified_name
12128790:($
"
_user_specified_name
12128792:($
"
_user_specified_name
12128794:($
"
_user_specified_name
12128796:($
"
_user_specified_name
12128798
�
�
J__inference_dense_66_layer_call_and_return_all_conditional_losses_12129435

inputs
unknown:X
	unknown_0:X
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������X*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_66_layer_call_and_return_conditional_losses_12128318�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_66_activity_regularizer_12128099o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������XX

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129427:($
"
_user_specified_name
12129429
�
�
I__inference_layer_1_layer_call_and_return_all_conditional_losses_12129220

inputs
unknown:

	unknown_0:

identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_12128138�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *:
f5R3
1__inference_layer_1_activity_regularizer_12128064o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
12129212:($
"
_user_specified_name
12129214
��
�
#__inference__wrapped_model_12128057
layer_1_inputF
4sequential_15_layer_1_matmul_readvariableop_resource:
C
5sequential_15_layer_1_biasadd_readvariableop_resource:
G
5sequential_15_dense_62_matmul_readvariableop_resource:
cD
6sequential_15_dense_62_biasadd_readvariableop_resource:cG
5sequential_15_dense_63_matmul_readvariableop_resource:cD
6sequential_15_dense_63_biasadd_readvariableop_resource:G
5sequential_15_dense_64_matmul_readvariableop_resource:$D
6sequential_15_dense_64_biasadd_readvariableop_resource:$G
5sequential_15_dense_65_matmul_readvariableop_resource:$D
6sequential_15_dense_65_biasadd_readvariableop_resource:G
5sequential_15_dense_66_matmul_readvariableop_resource:XD
6sequential_15_dense_66_biasadd_readvariableop_resource:XG
5sequential_15_dense_67_matmul_readvariableop_resource:XD
6sequential_15_dense_67_biasadd_readvariableop_resource:K
9sequential_15_layer_output_matmul_readvariableop_resource:H
:sequential_15_layer_output_biasadd_readvariableop_resource:
identity��-sequential_15/dense_62/BiasAdd/ReadVariableOp�,sequential_15/dense_62/MatMul/ReadVariableOp�-sequential_15/dense_63/BiasAdd/ReadVariableOp�,sequential_15/dense_63/MatMul/ReadVariableOp�-sequential_15/dense_64/BiasAdd/ReadVariableOp�,sequential_15/dense_64/MatMul/ReadVariableOp�-sequential_15/dense_65/BiasAdd/ReadVariableOp�,sequential_15/dense_65/MatMul/ReadVariableOp�-sequential_15/dense_66/BiasAdd/ReadVariableOp�,sequential_15/dense_66/MatMul/ReadVariableOp�-sequential_15/dense_67/BiasAdd/ReadVariableOp�,sequential_15/dense_67/MatMul/ReadVariableOp�,sequential_15/layer_1/BiasAdd/ReadVariableOp�+sequential_15/layer_1/MatMul/ReadVariableOp�1sequential_15/layer_output/BiasAdd/ReadVariableOp�0sequential_15/layer_output/MatMul/ReadVariableOp�
+sequential_15/layer_1/MatMul/ReadVariableOpReadVariableOp4sequential_15_layer_1_matmul_readvariableop_resource*
_output_shapes

:
*
dtype0�
sequential_15/layer_1/MatMulMatMullayer_1_input3sequential_15/layer_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
�
,sequential_15/layer_1/BiasAdd/ReadVariableOpReadVariableOp5sequential_15_layer_1_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype0�
sequential_15/layer_1/BiasAddBiasAdd&sequential_15/layer_1/MatMul:product:04sequential_15/layer_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
|
sequential_15/layer_1/ReluRelu&sequential_15/layer_1/BiasAdd:output:0*
T0*'
_output_shapes
:���������
�
0sequential_15/layer_1/ActivityRegularizer/L2LossL2Loss(sequential_15/layer_1/Relu:activations:0*
T0*
_output_shapes
: t
/sequential_15/layer_1/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
-sequential_15/layer_1/ActivityRegularizer/mulMul8sequential_15/layer_1/ActivityRegularizer/mul/x:output:09sequential_15/layer_1/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
/sequential_15/layer_1/ActivityRegularizer/ShapeShape(sequential_15/layer_1/Relu:activations:0*
T0*
_output_shapes
::���
=sequential_15/layer_1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
?sequential_15/layer_1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
?sequential_15/layer_1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
7sequential_15/layer_1/ActivityRegularizer/strided_sliceStridedSlice8sequential_15/layer_1/ActivityRegularizer/Shape:output:0Fsequential_15/layer_1/ActivityRegularizer/strided_slice/stack:output:0Hsequential_15/layer_1/ActivityRegularizer/strided_slice/stack_1:output:0Hsequential_15/layer_1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
.sequential_15/layer_1/ActivityRegularizer/CastCast@sequential_15/layer_1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
1sequential_15/layer_1/ActivityRegularizer/truedivRealDiv1sequential_15/layer_1/ActivityRegularizer/mul:z:02sequential_15/layer_1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
,sequential_15/dense_62/MatMul/ReadVariableOpReadVariableOp5sequential_15_dense_62_matmul_readvariableop_resource*
_output_shapes

:
c*
dtype0�
sequential_15/dense_62/MatMulMatMul(sequential_15/layer_1/Relu:activations:04sequential_15/dense_62/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������c�
-sequential_15/dense_62/BiasAdd/ReadVariableOpReadVariableOp6sequential_15_dense_62_biasadd_readvariableop_resource*
_output_shapes
:c*
dtype0�
sequential_15/dense_62/BiasAddBiasAdd'sequential_15/dense_62/MatMul:product:05sequential_15/dense_62/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������c~
sequential_15/dense_62/ReluRelu'sequential_15/dense_62/BiasAdd:output:0*
T0*'
_output_shapes
:���������c�
1sequential_15/dense_62/ActivityRegularizer/L2LossL2Loss)sequential_15/dense_62/Relu:activations:0*
T0*
_output_shapes
: u
0sequential_15/dense_62/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
.sequential_15/dense_62/ActivityRegularizer/mulMul9sequential_15/dense_62/ActivityRegularizer/mul/x:output:0:sequential_15/dense_62/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
0sequential_15/dense_62/ActivityRegularizer/ShapeShape)sequential_15/dense_62/Relu:activations:0*
T0*
_output_shapes
::���
>sequential_15/dense_62/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
@sequential_15/dense_62/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
@sequential_15/dense_62/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
8sequential_15/dense_62/ActivityRegularizer/strided_sliceStridedSlice9sequential_15/dense_62/ActivityRegularizer/Shape:output:0Gsequential_15/dense_62/ActivityRegularizer/strided_slice/stack:output:0Isequential_15/dense_62/ActivityRegularizer/strided_slice/stack_1:output:0Isequential_15/dense_62/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
/sequential_15/dense_62/ActivityRegularizer/CastCastAsequential_15/dense_62/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
2sequential_15/dense_62/ActivityRegularizer/truedivRealDiv2sequential_15/dense_62/ActivityRegularizer/mul:z:03sequential_15/dense_62/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
,sequential_15/dense_63/MatMul/ReadVariableOpReadVariableOp5sequential_15_dense_63_matmul_readvariableop_resource*
_output_shapes

:c*
dtype0�
sequential_15/dense_63/MatMulMatMul)sequential_15/dense_62/Relu:activations:04sequential_15/dense_63/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
-sequential_15/dense_63/BiasAdd/ReadVariableOpReadVariableOp6sequential_15_dense_63_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential_15/dense_63/BiasAddBiasAdd'sequential_15/dense_63/MatMul:product:05sequential_15/dense_63/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
sequential_15/dense_63/ReluRelu'sequential_15/dense_63/BiasAdd:output:0*
T0*'
_output_shapes
:����������
1sequential_15/dense_63/ActivityRegularizer/L2LossL2Loss)sequential_15/dense_63/Relu:activations:0*
T0*
_output_shapes
: u
0sequential_15/dense_63/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
.sequential_15/dense_63/ActivityRegularizer/mulMul9sequential_15/dense_63/ActivityRegularizer/mul/x:output:0:sequential_15/dense_63/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
0sequential_15/dense_63/ActivityRegularizer/ShapeShape)sequential_15/dense_63/Relu:activations:0*
T0*
_output_shapes
::���
>sequential_15/dense_63/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
@sequential_15/dense_63/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
@sequential_15/dense_63/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
8sequential_15/dense_63/ActivityRegularizer/strided_sliceStridedSlice9sequential_15/dense_63/ActivityRegularizer/Shape:output:0Gsequential_15/dense_63/ActivityRegularizer/strided_slice/stack:output:0Isequential_15/dense_63/ActivityRegularizer/strided_slice/stack_1:output:0Isequential_15/dense_63/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
/sequential_15/dense_63/ActivityRegularizer/CastCastAsequential_15/dense_63/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
2sequential_15/dense_63/ActivityRegularizer/truedivRealDiv2sequential_15/dense_63/ActivityRegularizer/mul:z:03sequential_15/dense_63/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
,sequential_15/dense_64/MatMul/ReadVariableOpReadVariableOp5sequential_15_dense_64_matmul_readvariableop_resource*
_output_shapes

:$*
dtype0�
sequential_15/dense_64/MatMulMatMul)sequential_15/dense_63/Relu:activations:04sequential_15/dense_64/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������$�
-sequential_15/dense_64/BiasAdd/ReadVariableOpReadVariableOp6sequential_15_dense_64_biasadd_readvariableop_resource*
_output_shapes
:$*
dtype0�
sequential_15/dense_64/BiasAddBiasAdd'sequential_15/dense_64/MatMul:product:05sequential_15/dense_64/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������$~
sequential_15/dense_64/ReluRelu'sequential_15/dense_64/BiasAdd:output:0*
T0*'
_output_shapes
:���������$�
1sequential_15/dense_64/ActivityRegularizer/L2LossL2Loss)sequential_15/dense_64/Relu:activations:0*
T0*
_output_shapes
: u
0sequential_15/dense_64/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
.sequential_15/dense_64/ActivityRegularizer/mulMul9sequential_15/dense_64/ActivityRegularizer/mul/x:output:0:sequential_15/dense_64/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
0sequential_15/dense_64/ActivityRegularizer/ShapeShape)sequential_15/dense_64/Relu:activations:0*
T0*
_output_shapes
::���
>sequential_15/dense_64/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
@sequential_15/dense_64/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
@sequential_15/dense_64/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
8sequential_15/dense_64/ActivityRegularizer/strided_sliceStridedSlice9sequential_15/dense_64/ActivityRegularizer/Shape:output:0Gsequential_15/dense_64/ActivityRegularizer/strided_slice/stack:output:0Isequential_15/dense_64/ActivityRegularizer/strided_slice/stack_1:output:0Isequential_15/dense_64/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
/sequential_15/dense_64/ActivityRegularizer/CastCastAsequential_15/dense_64/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
2sequential_15/dense_64/ActivityRegularizer/truedivRealDiv2sequential_15/dense_64/ActivityRegularizer/mul:z:03sequential_15/dense_64/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
,sequential_15/dense_65/MatMul/ReadVariableOpReadVariableOp5sequential_15_dense_65_matmul_readvariableop_resource*
_output_shapes

:$*
dtype0�
sequential_15/dense_65/MatMulMatMul)sequential_15/dense_64/Relu:activations:04sequential_15/dense_65/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
-sequential_15/dense_65/BiasAdd/ReadVariableOpReadVariableOp6sequential_15_dense_65_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential_15/dense_65/BiasAddBiasAdd'sequential_15/dense_65/MatMul:product:05sequential_15/dense_65/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
sequential_15/dense_65/ReluRelu'sequential_15/dense_65/BiasAdd:output:0*
T0*'
_output_shapes
:����������
1sequential_15/dense_65/ActivityRegularizer/L2LossL2Loss)sequential_15/dense_65/Relu:activations:0*
T0*
_output_shapes
: u
0sequential_15/dense_65/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
.sequential_15/dense_65/ActivityRegularizer/mulMul9sequential_15/dense_65/ActivityRegularizer/mul/x:output:0:sequential_15/dense_65/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
0sequential_15/dense_65/ActivityRegularizer/ShapeShape)sequential_15/dense_65/Relu:activations:0*
T0*
_output_shapes
::���
>sequential_15/dense_65/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
@sequential_15/dense_65/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
@sequential_15/dense_65/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
8sequential_15/dense_65/ActivityRegularizer/strided_sliceStridedSlice9sequential_15/dense_65/ActivityRegularizer/Shape:output:0Gsequential_15/dense_65/ActivityRegularizer/strided_slice/stack:output:0Isequential_15/dense_65/ActivityRegularizer/strided_slice/stack_1:output:0Isequential_15/dense_65/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
/sequential_15/dense_65/ActivityRegularizer/CastCastAsequential_15/dense_65/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
2sequential_15/dense_65/ActivityRegularizer/truedivRealDiv2sequential_15/dense_65/ActivityRegularizer/mul:z:03sequential_15/dense_65/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
,sequential_15/dense_66/MatMul/ReadVariableOpReadVariableOp5sequential_15_dense_66_matmul_readvariableop_resource*
_output_shapes

:X*
dtype0�
sequential_15/dense_66/MatMulMatMul)sequential_15/dense_65/Relu:activations:04sequential_15/dense_66/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������X�
-sequential_15/dense_66/BiasAdd/ReadVariableOpReadVariableOp6sequential_15_dense_66_biasadd_readvariableop_resource*
_output_shapes
:X*
dtype0�
sequential_15/dense_66/BiasAddBiasAdd'sequential_15/dense_66/MatMul:product:05sequential_15/dense_66/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������X~
sequential_15/dense_66/ReluRelu'sequential_15/dense_66/BiasAdd:output:0*
T0*'
_output_shapes
:���������X�
1sequential_15/dense_66/ActivityRegularizer/L2LossL2Loss)sequential_15/dense_66/Relu:activations:0*
T0*
_output_shapes
: u
0sequential_15/dense_66/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
.sequential_15/dense_66/ActivityRegularizer/mulMul9sequential_15/dense_66/ActivityRegularizer/mul/x:output:0:sequential_15/dense_66/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
0sequential_15/dense_66/ActivityRegularizer/ShapeShape)sequential_15/dense_66/Relu:activations:0*
T0*
_output_shapes
::���
>sequential_15/dense_66/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
@sequential_15/dense_66/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
@sequential_15/dense_66/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
8sequential_15/dense_66/ActivityRegularizer/strided_sliceStridedSlice9sequential_15/dense_66/ActivityRegularizer/Shape:output:0Gsequential_15/dense_66/ActivityRegularizer/strided_slice/stack:output:0Isequential_15/dense_66/ActivityRegularizer/strided_slice/stack_1:output:0Isequential_15/dense_66/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
/sequential_15/dense_66/ActivityRegularizer/CastCastAsequential_15/dense_66/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
2sequential_15/dense_66/ActivityRegularizer/truedivRealDiv2sequential_15/dense_66/ActivityRegularizer/mul:z:03sequential_15/dense_66/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
,sequential_15/dense_67/MatMul/ReadVariableOpReadVariableOp5sequential_15_dense_67_matmul_readvariableop_resource*
_output_shapes

:X*
dtype0�
sequential_15/dense_67/MatMulMatMul)sequential_15/dense_66/Relu:activations:04sequential_15/dense_67/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
-sequential_15/dense_67/BiasAdd/ReadVariableOpReadVariableOp6sequential_15_dense_67_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential_15/dense_67/BiasAddBiasAdd'sequential_15/dense_67/MatMul:product:05sequential_15/dense_67/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
sequential_15/dense_67/ReluRelu'sequential_15/dense_67/BiasAdd:output:0*
T0*'
_output_shapes
:����������
1sequential_15/dense_67/ActivityRegularizer/L2LossL2Loss)sequential_15/dense_67/Relu:activations:0*
T0*
_output_shapes
: u
0sequential_15/dense_67/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
.sequential_15/dense_67/ActivityRegularizer/mulMul9sequential_15/dense_67/ActivityRegularizer/mul/x:output:0:sequential_15/dense_67/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
0sequential_15/dense_67/ActivityRegularizer/ShapeShape)sequential_15/dense_67/Relu:activations:0*
T0*
_output_shapes
::���
>sequential_15/dense_67/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
@sequential_15/dense_67/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
@sequential_15/dense_67/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
8sequential_15/dense_67/ActivityRegularizer/strided_sliceStridedSlice9sequential_15/dense_67/ActivityRegularizer/Shape:output:0Gsequential_15/dense_67/ActivityRegularizer/strided_slice/stack:output:0Isequential_15/dense_67/ActivityRegularizer/strided_slice/stack_1:output:0Isequential_15/dense_67/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
/sequential_15/dense_67/ActivityRegularizer/CastCastAsequential_15/dense_67/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
2sequential_15/dense_67/ActivityRegularizer/truedivRealDiv2sequential_15/dense_67/ActivityRegularizer/mul:z:03sequential_15/dense_67/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
0sequential_15/layer_output/MatMul/ReadVariableOpReadVariableOp9sequential_15_layer_output_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
!sequential_15/layer_output/MatMulMatMul)sequential_15/dense_67/Relu:activations:08sequential_15/layer_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
1sequential_15/layer_output/BiasAdd/ReadVariableOpReadVariableOp:sequential_15_layer_output_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
"sequential_15/layer_output/BiasAddBiasAdd+sequential_15/layer_output/MatMul:product:09sequential_15/layer_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
5sequential_15/layer_output/ActivityRegularizer/L2LossL2Loss+sequential_15/layer_output/BiasAdd:output:0*
T0*
_output_shapes
: y
4sequential_15/layer_output/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
2sequential_15/layer_output/ActivityRegularizer/mulMul=sequential_15/layer_output/ActivityRegularizer/mul/x:output:0>sequential_15/layer_output/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
4sequential_15/layer_output/ActivityRegularizer/ShapeShape+sequential_15/layer_output/BiasAdd:output:0*
T0*
_output_shapes
::���
Bsequential_15/layer_output/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Dsequential_15/layer_output/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Dsequential_15/layer_output/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
<sequential_15/layer_output/ActivityRegularizer/strided_sliceStridedSlice=sequential_15/layer_output/ActivityRegularizer/Shape:output:0Ksequential_15/layer_output/ActivityRegularizer/strided_slice/stack:output:0Msequential_15/layer_output/ActivityRegularizer/strided_slice/stack_1:output:0Msequential_15/layer_output/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3sequential_15/layer_output/ActivityRegularizer/CastCastEsequential_15/layer_output/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
6sequential_15/layer_output/ActivityRegularizer/truedivRealDiv6sequential_15/layer_output/ActivityRegularizer/mul:z:07sequential_15/layer_output/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: z
IdentityIdentity+sequential_15/layer_output/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp.^sequential_15/dense_62/BiasAdd/ReadVariableOp-^sequential_15/dense_62/MatMul/ReadVariableOp.^sequential_15/dense_63/BiasAdd/ReadVariableOp-^sequential_15/dense_63/MatMul/ReadVariableOp.^sequential_15/dense_64/BiasAdd/ReadVariableOp-^sequential_15/dense_64/MatMul/ReadVariableOp.^sequential_15/dense_65/BiasAdd/ReadVariableOp-^sequential_15/dense_65/MatMul/ReadVariableOp.^sequential_15/dense_66/BiasAdd/ReadVariableOp-^sequential_15/dense_66/MatMul/ReadVariableOp.^sequential_15/dense_67/BiasAdd/ReadVariableOp-^sequential_15/dense_67/MatMul/ReadVariableOp-^sequential_15/layer_1/BiasAdd/ReadVariableOp,^sequential_15/layer_1/MatMul/ReadVariableOp2^sequential_15/layer_output/BiasAdd/ReadVariableOp1^sequential_15/layer_output/MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*F
_input_shapes5
3:���������: : : : : : : : : : : : : : : : 2^
-sequential_15/dense_62/BiasAdd/ReadVariableOp-sequential_15/dense_62/BiasAdd/ReadVariableOp2\
,sequential_15/dense_62/MatMul/ReadVariableOp,sequential_15/dense_62/MatMul/ReadVariableOp2^
-sequential_15/dense_63/BiasAdd/ReadVariableOp-sequential_15/dense_63/BiasAdd/ReadVariableOp2\
,sequential_15/dense_63/MatMul/ReadVariableOp,sequential_15/dense_63/MatMul/ReadVariableOp2^
-sequential_15/dense_64/BiasAdd/ReadVariableOp-sequential_15/dense_64/BiasAdd/ReadVariableOp2\
,sequential_15/dense_64/MatMul/ReadVariableOp,sequential_15/dense_64/MatMul/ReadVariableOp2^
-sequential_15/dense_65/BiasAdd/ReadVariableOp-sequential_15/dense_65/BiasAdd/ReadVariableOp2\
,sequential_15/dense_65/MatMul/ReadVariableOp,sequential_15/dense_65/MatMul/ReadVariableOp2^
-sequential_15/dense_66/BiasAdd/ReadVariableOp-sequential_15/dense_66/BiasAdd/ReadVariableOp2\
,sequential_15/dense_66/MatMul/ReadVariableOp,sequential_15/dense_66/MatMul/ReadVariableOp2^
-sequential_15/dense_67/BiasAdd/ReadVariableOp-sequential_15/dense_67/BiasAdd/ReadVariableOp2\
,sequential_15/dense_67/MatMul/ReadVariableOp,sequential_15/dense_67/MatMul/ReadVariableOp2\
,sequential_15/layer_1/BiasAdd/ReadVariableOp,sequential_15/layer_1/BiasAdd/ReadVariableOp2Z
+sequential_15/layer_1/MatMul/ReadVariableOp+sequential_15/layer_1/MatMul/ReadVariableOp2f
1sequential_15/layer_output/BiasAdd/ReadVariableOp1sequential_15/layer_output/BiasAdd/ReadVariableOp2d
0sequential_15/layer_output/MatMul/ReadVariableOp0sequential_15/layer_output/MatMul/ReadVariableOp:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:(	$
"
_user_specified_name
resource:(
$
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�

�
__inference_loss_fn_11_12129663C
5dense_66_bias_regularizer_abs_readvariableop_resource:X
identity��,dense_66/bias/Regularizer/Abs/ReadVariableOp�
,dense_66/bias/Regularizer/Abs/ReadVariableOpReadVariableOp5dense_66_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:X*
dtype0
dense_66/bias/Regularizer/AbsAbs4dense_66/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Xi
dense_66/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_66/bias/Regularizer/SumSum!dense_66/bias/Regularizer/Abs:y:0(dense_66/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_66/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_66/bias/Regularizer/mulMul(dense_66/bias/Regularizer/mul/x:output:0&dense_66/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentity!dense_66/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: Q
NoOpNoOp-^dense_66/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2\
,dense_66/bias/Regularizer/Abs/ReadVariableOp,dense_66/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
+__inference_dense_62_layer_call_fn_12129252

inputs
unknown:
c
	unknown_0:c
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������c*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_62_layer_call_and_return_conditional_losses_12128174o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������c<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
12129246:($
"
_user_specified_name
12129248
�
�
E__inference_layer_1_layer_call_and_return_conditional_losses_12128138

inputs0
matmul_readvariableop_resource:
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������
�
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
M
6__inference_layer_output_activity_regularizer_12128113
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
J__inference_dense_63_layer_call_and_return_all_conditional_losses_12129306

inputs
unknown:c
	unknown_0:
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_63_layer_call_and_return_conditional_losses_12128210�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_63_activity_regularizer_12128078o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������c: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������c
 
_user_specified_nameinputs:($
"
_user_specified_name
12129298:($
"
_user_specified_name
12129300
�
�
J__inference_dense_65_layer_call_and_return_all_conditional_losses_12129392

inputs
unknown:$
	unknown_0:
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_65_layer_call_and_return_conditional_losses_12128282�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_65_activity_regularizer_12128092o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������$: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������$
 
_user_specified_nameinputs:($
"
_user_specified_name
12129384:($
"
_user_specified_name
12129386
�
�
F__inference_dense_62_layer_call_and_return_conditional_losses_12129286

inputs0
matmul_readvariableop_resource:
c-
biasadd_readvariableop_resource:c
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_62/bias/Regularizer/Abs/ReadVariableOp�.dense_62/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
c*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������cr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:c*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������cP
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������c�
.dense_62/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
c*
dtype0�
dense_62/kernel/Regularizer/AbsAbs6dense_62/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
cr
!dense_62/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_62/kernel/Regularizer/SumSum#dense_62/kernel/Regularizer/Abs:y:0*dense_62/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_62/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_62/kernel/Regularizer/mulMul*dense_62/kernel/Regularizer/mul/x:output:0(dense_62/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_62/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:c*
dtype0
dense_62/bias/Regularizer/AbsAbs4dense_62/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:ci
dense_62/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_62/bias/Regularizer/SumSum!dense_62/bias/Regularizer/Abs:y:0(dense_62/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_62/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_62/bias/Regularizer/mulMul(dense_62/bias/Regularizer/mul/x:output:0&dense_62/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������c�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_62/bias/Regularizer/Abs/ReadVariableOp/^dense_62/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_62/bias/Regularizer/Abs/ReadVariableOp,dense_62/bias/Regularizer/Abs/ReadVariableOp2`
.dense_62/kernel/Regularizer/Abs/ReadVariableOp.dense_62/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�

�
__inference_loss_fn_9_12129643C
5dense_65_bias_regularizer_abs_readvariableop_resource:
identity��,dense_65/bias/Regularizer/Abs/ReadVariableOp�
,dense_65/bias/Regularizer/Abs/ReadVariableOpReadVariableOp5dense_65_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:*
dtype0
dense_65/bias/Regularizer/AbsAbs4dense_65/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_65/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_65/bias/Regularizer/SumSum!dense_65/bias/Regularizer/Abs:y:0(dense_65/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_65/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_65/bias/Regularizer/mulMul(dense_65/bias/Regularizer/mul/x:output:0&dense_65/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentity!dense_65/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: Q
NoOpNoOp-^dense_65/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2\
,dense_65/bias/Regularizer/Abs/ReadVariableOp,dense_65/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
__inference_loss_fn_2_12129573I
7dense_62_kernel_regularizer_abs_readvariableop_resource:
c
identity��.dense_62/kernel/Regularizer/Abs/ReadVariableOp�
.dense_62/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp7dense_62_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:
c*
dtype0�
dense_62/kernel/Regularizer/AbsAbs6dense_62/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
cr
!dense_62/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_62/kernel/Regularizer/SumSum#dense_62/kernel/Regularizer/Abs:y:0*dense_62/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_62/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_62/kernel/Regularizer/mulMul*dense_62/kernel/Regularizer/mul/x:output:0(dense_62/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentity#dense_62/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp/^dense_62/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2`
.dense_62/kernel/Regularizer/Abs/ReadVariableOp.dense_62/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
J__inference_dense_67_layer_call_and_return_all_conditional_losses_12129478

inputs
unknown:X
	unknown_0:
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *O
fJRH
F__inference_dense_67_layer_call_and_return_conditional_losses_12128354�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *;
f6R4
2__inference_dense_67_activity_regularizer_12128106o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������X: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������X
 
_user_specified_nameinputs:($
"
_user_specified_name
12129470:($
"
_user_specified_name
12129472
�

�
__inference_loss_fn_13_12129683C
5dense_67_bias_regularizer_abs_readvariableop_resource:
identity��,dense_67/bias/Regularizer/Abs/ReadVariableOp�
,dense_67/bias/Regularizer/Abs/ReadVariableOpReadVariableOp5dense_67_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:*
dtype0
dense_67/bias/Regularizer/AbsAbs4dense_67/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_67/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_67/bias/Regularizer/SumSum!dense_67/bias/Regularizer/Abs:y:0(dense_67/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_67/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_67/bias/Regularizer/mulMul(dense_67/bias/Regularizer/mul/x:output:0&dense_67/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentity!dense_67/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: Q
NoOpNoOp-^dense_67/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2\
,dense_67/bias/Regularizer/Abs/ReadVariableOp,dense_67/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
F__inference_dense_67_layer_call_and_return_conditional_losses_12129501

inputs0
matmul_readvariableop_resource:X-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�,dense_67/bias/Regularizer/Abs/ReadVariableOp�.dense_67/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:����������
.dense_67/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:X*
dtype0�
dense_67/kernel/Regularizer/AbsAbs6dense_67/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Xr
!dense_67/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
dense_67/kernel/Regularizer/SumSum#dense_67/kernel/Regularizer/Abs:y:0*dense_67/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: f
!dense_67/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
dense_67/kernel/Regularizer/mulMul*dense_67/kernel/Regularizer/mul/x:output:0(dense_67/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
,dense_67/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0
dense_67/bias/Regularizer/AbsAbs4dense_67/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:i
dense_67/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_67/bias/Regularizer/SumSum!dense_67/bias/Regularizer/Abs:y:0(dense_67/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: d
dense_67/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_67/bias/Regularizer/mulMul(dense_67/bias/Regularizer/mul/x:output:0&dense_67/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp-^dense_67/bias/Regularizer/Abs/ReadVariableOp/^dense_67/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������X: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2\
,dense_67/bias/Regularizer/Abs/ReadVariableOp,dense_67/bias/Regularizer/Abs/ReadVariableOp2`
.dense_67/kernel/Regularizer/Abs/ReadVariableOp.dense_67/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������X
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
I
2__inference_dense_62_activity_regularizer_12128071
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
G
layer_1_input6
serving_default_layer_1_input:0���������@
layer_output0
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
layer_with_weights-4
layer-4
layer_with_weights-5
layer-5
layer_with_weights-6
layer-6
layer_with_weights-7
layer-7
		variables

trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses
_default_save_signature
	optimizer
loss

signatures"
_tf_keras_sequential
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias"
_tf_keras_layer
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
* &call_and_return_all_conditional_losses

!kernel
"bias"
_tf_keras_layer
�
#	variables
$trainable_variables
%regularization_losses
&	keras_api
'__call__
*(&call_and_return_all_conditional_losses

)kernel
*bias"
_tf_keras_layer
�
+	variables
,trainable_variables
-regularization_losses
.	keras_api
/__call__
*0&call_and_return_all_conditional_losses

1kernel
2bias"
_tf_keras_layer
�
3	variables
4trainable_variables
5regularization_losses
6	keras_api
7__call__
*8&call_and_return_all_conditional_losses

9kernel
:bias"
_tf_keras_layer
�
;	variables
<trainable_variables
=regularization_losses
>	keras_api
?__call__
*@&call_and_return_all_conditional_losses

Akernel
Bbias"
_tf_keras_layer
�
C	variables
Dtrainable_variables
Eregularization_losses
F	keras_api
G__call__
*H&call_and_return_all_conditional_losses

Ikernel
Jbias"
_tf_keras_layer
�
K	variables
Ltrainable_variables
Mregularization_losses
N	keras_api
O__call__
*P&call_and_return_all_conditional_losses

Qkernel
Rbias"
_tf_keras_layer
�
0
1
!2
"3
)4
*5
16
27
98
:9
A10
B11
I12
J13
Q14
R15"
trackable_list_wrapper
�
0
1
!2
"3
)4
*5
16
27
98
:9
A10
B11
I12
J13
Q14
R15"
trackable_list_wrapper
�
S0
T1
U2
V3
W4
X5
Y6
Z7
[8
\9
]10
^11
_12
`13
a14
b15"
trackable_list_wrapper
�
cnon_trainable_variables

dlayers
emetrics
flayer_regularization_losses
glayer_metrics
		variables

trainable_variables
regularization_losses
__call__
_default_save_signature
*&call_and_return_all_conditional_losses
&"call_and_return_conditional_losses"
_generic_user_object
�
htrace_0
itrace_12�
0__inference_sequential_15_layer_call_fn_12128765
0__inference_sequential_15_layer_call_fn_12128810�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zhtrace_0zitrace_1
�
jtrace_0
ktrace_12�
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128508
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128720�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zjtrace_0zktrace_1
�B�
#__inference__wrapped_model_12128057layer_1_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
r
l
_variables
m_iterations
n_current_learning_rate
o_update_step_xla"
experimentalOptimizer
 "
trackable_list_wrapper
,
pserving_default"
signature_map
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
S0
T1"
trackable_list_wrapper
�
qnon_trainable_variables

rlayers
smetrics
tlayer_regularization_losses
ulayer_metrics
	variables
trainable_variables
regularization_losses
__call__
vactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&w"call_and_return_conditional_losses"
_generic_user_object
�
xtrace_02�
*__inference_layer_1_layer_call_fn_12129209�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zxtrace_0
�
ytrace_02�
I__inference_layer_1_layer_call_and_return_all_conditional_losses_12129220�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zytrace_0
 :
2layer_1/kernel
:
2layer_1/bias
.
!0
"1"
trackable_list_wrapper
.
!0
"1"
trackable_list_wrapper
.
U0
V1"
trackable_list_wrapper
�
znon_trainable_variables

{layers
|metrics
}layer_regularization_losses
~layer_metrics
	variables
trainable_variables
regularization_losses
__call__
activity_regularizer_fn
* &call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_dense_62_layer_call_fn_12129252�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
J__inference_dense_62_layer_call_and_return_all_conditional_losses_12129263�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
!:
c2dense_62/kernel
:c2dense_62/bias
.
)0
*1"
trackable_list_wrapper
.
)0
*1"
trackable_list_wrapper
.
W0
X1"
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
#	variables
$trainable_variables
%regularization_losses
'__call__
�activity_regularizer_fn
*(&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_dense_63_layer_call_fn_12129295�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
J__inference_dense_63_layer_call_and_return_all_conditional_losses_12129306�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
!:c2dense_63/kernel
:2dense_63/bias
.
10
21"
trackable_list_wrapper
.
10
21"
trackable_list_wrapper
.
Y0
Z1"
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
+	variables
,trainable_variables
-regularization_losses
/__call__
�activity_regularizer_fn
*0&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_dense_64_layer_call_fn_12129338�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
J__inference_dense_64_layer_call_and_return_all_conditional_losses_12129349�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
!:$2dense_64/kernel
:$2dense_64/bias
.
90
:1"
trackable_list_wrapper
.
90
:1"
trackable_list_wrapper
.
[0
\1"
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
3	variables
4trainable_variables
5regularization_losses
7__call__
�activity_regularizer_fn
*8&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_dense_65_layer_call_fn_12129381�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
J__inference_dense_65_layer_call_and_return_all_conditional_losses_12129392�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
!:$2dense_65/kernel
:2dense_65/bias
.
A0
B1"
trackable_list_wrapper
.
A0
B1"
trackable_list_wrapper
.
]0
^1"
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
;	variables
<trainable_variables
=regularization_losses
?__call__
�activity_regularizer_fn
*@&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_dense_66_layer_call_fn_12129424�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
J__inference_dense_66_layer_call_and_return_all_conditional_losses_12129435�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
!:X2dense_66/kernel
:X2dense_66/bias
.
I0
J1"
trackable_list_wrapper
.
I0
J1"
trackable_list_wrapper
.
_0
`1"
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
C	variables
Dtrainable_variables
Eregularization_losses
G__call__
�activity_regularizer_fn
*H&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
+__inference_dense_67_layer_call_fn_12129467�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
J__inference_dense_67_layer_call_and_return_all_conditional_losses_12129478�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
!:X2dense_67/kernel
:2dense_67/bias
.
Q0
R1"
trackable_list_wrapper
.
Q0
R1"
trackable_list_wrapper
.
a0
b1"
trackable_list_wrapper
�
�non_trainable_variables
�layers
�metrics
 �layer_regularization_losses
�layer_metrics
K	variables
Ltrainable_variables
Mregularization_losses
O__call__
�activity_regularizer_fn
*P&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
�
�trace_02�
/__inference_layer_output_layer_call_fn_12129510�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�
�trace_02�
N__inference_layer_output_layer_call_and_return_all_conditional_losses_12129521�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
%:#2layer_output/kernel
:2layer_output/bias
�
�trace_02�
__inference_loss_fn_0_12129553�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_1_12129563�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_2_12129573�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_3_12129583�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_4_12129593�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_5_12129603�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_6_12129613�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_7_12129623�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_8_12129633�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_9_12129643�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_10_12129653�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_11_12129663�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_12_12129673�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_13_12129683�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_14_12129693�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
�
�trace_02�
__inference_loss_fn_15_12129703�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� z�trace_0
 "
trackable_list_wrapper
X
0
1
2
3
4
5
6
7"
trackable_list_wrapper
(
�0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
0__inference_sequential_15_layer_call_fn_12128765layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
0__inference_sequential_15_layer_call_fn_12128810layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128508layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128720layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
'
m0"
trackable_list_wrapper
:	 2	iteration
: 2current_learning_rate
�2��
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
�B�
&__inference_signature_wrapper_12129104layer_1_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
S0
T1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
1__inference_layer_1_activity_regularizer_12128064�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
E__inference_layer_1_layer_call_and_return_conditional_losses_12129243�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
*__inference_layer_1_layer_call_fn_12129209inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
I__inference_layer_1_layer_call_and_return_all_conditional_losses_12129220inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
U0
V1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
2__inference_dense_62_activity_regularizer_12128071�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
F__inference_dense_62_layer_call_and_return_conditional_losses_12129286�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
+__inference_dense_62_layer_call_fn_12129252inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
J__inference_dense_62_layer_call_and_return_all_conditional_losses_12129263inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
W0
X1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
2__inference_dense_63_activity_regularizer_12128078�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
F__inference_dense_63_layer_call_and_return_conditional_losses_12129329�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
+__inference_dense_63_layer_call_fn_12129295inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
J__inference_dense_63_layer_call_and_return_all_conditional_losses_12129306inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
Y0
Z1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
2__inference_dense_64_activity_regularizer_12128085�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
F__inference_dense_64_layer_call_and_return_conditional_losses_12129372�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
+__inference_dense_64_layer_call_fn_12129338inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
J__inference_dense_64_layer_call_and_return_all_conditional_losses_12129349inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
[0
\1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
2__inference_dense_65_activity_regularizer_12128092�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
F__inference_dense_65_layer_call_and_return_conditional_losses_12129415�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
+__inference_dense_65_layer_call_fn_12129381inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
J__inference_dense_65_layer_call_and_return_all_conditional_losses_12129392inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
]0
^1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
2__inference_dense_66_activity_regularizer_12128099�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
F__inference_dense_66_layer_call_and_return_conditional_losses_12129458�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
+__inference_dense_66_layer_call_fn_12129424inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
J__inference_dense_66_layer_call_and_return_all_conditional_losses_12129435inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
_0
`1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
2__inference_dense_67_activity_regularizer_12128106�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
F__inference_dense_67_layer_call_and_return_conditional_losses_12129501�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
+__inference_dense_67_layer_call_fn_12129467inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
J__inference_dense_67_layer_call_and_return_all_conditional_losses_12129478inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
a0
b1"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
�trace_02�
6__inference_layer_output_activity_regularizer_12128113�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�z�trace_0
�
�trace_02�
J__inference_layer_output_layer_call_and_return_conditional_losses_12129543�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z�trace_0
�B�
/__inference_layer_output_layer_call_fn_12129510inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
N__inference_layer_output_layer_call_and_return_all_conditional_losses_12129521inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
__inference_loss_fn_0_12129553"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_1_12129563"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_2_12129573"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_3_12129583"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_4_12129593"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_5_12129603"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_6_12129613"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_7_12129623"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_8_12129633"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_9_12129643"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_10_12129653"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_11_12129663"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_12_12129673"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_13_12129683"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_14_12129693"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_15_12129703"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
R
�	variables
�	keras_api

�total

�count"
_tf_keras_metric
�B�
1__inference_layer_1_activity_regularizer_12128064x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
E__inference_layer_1_layer_call_and_return_conditional_losses_12129243inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
2__inference_dense_62_activity_regularizer_12128071x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
F__inference_dense_62_layer_call_and_return_conditional_losses_12129286inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
2__inference_dense_63_activity_regularizer_12128078x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
F__inference_dense_63_layer_call_and_return_conditional_losses_12129329inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
2__inference_dense_64_activity_regularizer_12128085x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
F__inference_dense_64_layer_call_and_return_conditional_losses_12129372inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
2__inference_dense_65_activity_regularizer_12128092x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
F__inference_dense_65_layer_call_and_return_conditional_losses_12129415inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
2__inference_dense_66_activity_regularizer_12128099x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
F__inference_dense_66_layer_call_and_return_conditional_losses_12129458inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
2__inference_dense_67_activity_regularizer_12128106x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
F__inference_dense_67_layer_call_and_return_conditional_losses_12129501inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
6__inference_layer_output_activity_regularizer_12128113x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
J__inference_layer_output_layer_call_and_return_conditional_losses_12129543inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
0
�0
�1"
trackable_list_wrapper
.
�	variables"
_generic_user_object
:  (2total
:  (2count�
#__inference__wrapped_model_12128057�!")*129:ABIJQR6�3
,�)
'�$
layer_1_input���������
� ";�8
6
layer_output&�#
layer_output���������e
2__inference_dense_62_activity_regularizer_12128071/�
�
�	
x
� "�
unknown �
J__inference_dense_62_layer_call_and_return_all_conditional_losses_12129263x!"/�,
%�"
 �
inputs���������

� "A�>
"�
tensor_0���������c
�
�

tensor_1_0 �
F__inference_dense_62_layer_call_and_return_conditional_losses_12129286c!"/�,
%�"
 �
inputs���������

� ",�)
"�
tensor_0���������c
� �
+__inference_dense_62_layer_call_fn_12129252X!"/�,
%�"
 �
inputs���������

� "!�
unknown���������ce
2__inference_dense_63_activity_regularizer_12128078/�
�
�	
x
� "�
unknown �
J__inference_dense_63_layer_call_and_return_all_conditional_losses_12129306x)*/�,
%�"
 �
inputs���������c
� "A�>
"�
tensor_0���������
�
�

tensor_1_0 �
F__inference_dense_63_layer_call_and_return_conditional_losses_12129329c)*/�,
%�"
 �
inputs���������c
� ",�)
"�
tensor_0���������
� �
+__inference_dense_63_layer_call_fn_12129295X)*/�,
%�"
 �
inputs���������c
� "!�
unknown���������e
2__inference_dense_64_activity_regularizer_12128085/�
�
�	
x
� "�
unknown �
J__inference_dense_64_layer_call_and_return_all_conditional_losses_12129349x12/�,
%�"
 �
inputs���������
� "A�>
"�
tensor_0���������$
�
�

tensor_1_0 �
F__inference_dense_64_layer_call_and_return_conditional_losses_12129372c12/�,
%�"
 �
inputs���������
� ",�)
"�
tensor_0���������$
� �
+__inference_dense_64_layer_call_fn_12129338X12/�,
%�"
 �
inputs���������
� "!�
unknown���������$e
2__inference_dense_65_activity_regularizer_12128092/�
�
�	
x
� "�
unknown �
J__inference_dense_65_layer_call_and_return_all_conditional_losses_12129392x9:/�,
%�"
 �
inputs���������$
� "A�>
"�
tensor_0���������
�
�

tensor_1_0 �
F__inference_dense_65_layer_call_and_return_conditional_losses_12129415c9:/�,
%�"
 �
inputs���������$
� ",�)
"�
tensor_0���������
� �
+__inference_dense_65_layer_call_fn_12129381X9:/�,
%�"
 �
inputs���������$
� "!�
unknown���������e
2__inference_dense_66_activity_regularizer_12128099/�
�
�	
x
� "�
unknown �
J__inference_dense_66_layer_call_and_return_all_conditional_losses_12129435xAB/�,
%�"
 �
inputs���������
� "A�>
"�
tensor_0���������X
�
�

tensor_1_0 �
F__inference_dense_66_layer_call_and_return_conditional_losses_12129458cAB/�,
%�"
 �
inputs���������
� ",�)
"�
tensor_0���������X
� �
+__inference_dense_66_layer_call_fn_12129424XAB/�,
%�"
 �
inputs���������
� "!�
unknown���������Xe
2__inference_dense_67_activity_regularizer_12128106/�
�
�	
x
� "�
unknown �
J__inference_dense_67_layer_call_and_return_all_conditional_losses_12129478xIJ/�,
%�"
 �
inputs���������X
� "A�>
"�
tensor_0���������
�
�

tensor_1_0 �
F__inference_dense_67_layer_call_and_return_conditional_losses_12129501cIJ/�,
%�"
 �
inputs���������X
� ",�)
"�
tensor_0���������
� �
+__inference_dense_67_layer_call_fn_12129467XIJ/�,
%�"
 �
inputs���������X
� "!�
unknown���������d
1__inference_layer_1_activity_regularizer_12128064/�
�
�	
x
� "�
unknown �
I__inference_layer_1_layer_call_and_return_all_conditional_losses_12129220x/�,
%�"
 �
inputs���������
� "A�>
"�
tensor_0���������

�
�

tensor_1_0 �
E__inference_layer_1_layer_call_and_return_conditional_losses_12129243c/�,
%�"
 �
inputs���������
� ",�)
"�
tensor_0���������

� �
*__inference_layer_1_layer_call_fn_12129209X/�,
%�"
 �
inputs���������
� "!�
unknown���������
i
6__inference_layer_output_activity_regularizer_12128113/�
�
�	
x
� "�
unknown �
N__inference_layer_output_layer_call_and_return_all_conditional_losses_12129521xQR/�,
%�"
 �
inputs���������
� "A�>
"�
tensor_0���������
�
�

tensor_1_0 �
J__inference_layer_output_layer_call_and_return_conditional_losses_12129543cQR/�,
%�"
 �
inputs���������
� ",�)
"�
tensor_0���������
� �
/__inference_layer_output_layer_call_fn_12129510XQR/�,
%�"
 �
inputs���������
� "!�
unknown���������F
__inference_loss_fn_0_12129553$�

� 
� "�
unknown G
__inference_loss_fn_10_12129653$A�

� 
� "�
unknown G
__inference_loss_fn_11_12129663$B�

� 
� "�
unknown G
__inference_loss_fn_12_12129673$I�

� 
� "�
unknown G
__inference_loss_fn_13_12129683$J�

� 
� "�
unknown G
__inference_loss_fn_14_12129693$Q�

� 
� "�
unknown G
__inference_loss_fn_15_12129703$R�

� 
� "�
unknown F
__inference_loss_fn_1_12129563$�

� 
� "�
unknown F
__inference_loss_fn_2_12129573$!�

� 
� "�
unknown F
__inference_loss_fn_3_12129583$"�

� 
� "�
unknown F
__inference_loss_fn_4_12129593$)�

� 
� "�
unknown F
__inference_loss_fn_5_12129603$*�

� 
� "�
unknown F
__inference_loss_fn_6_12129613$1�

� 
� "�
unknown F
__inference_loss_fn_7_12129623$2�

� 
� "�
unknown F
__inference_loss_fn_8_12129633$9�

� 
� "�
unknown F
__inference_loss_fn_9_12129643$:�

� 
� "�
unknown �
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128508�!")*129:ABIJQR>�;
4�1
'�$
layer_1_input���������
p

 
� "���
"�
tensor_0���������
���
�

tensor_1_0 
�

tensor_1_1 
�

tensor_1_2 
�

tensor_1_3 
�

tensor_1_4 
�

tensor_1_5 
�

tensor_1_6 
�

tensor_1_7 �
K__inference_sequential_15_layer_call_and_return_conditional_losses_12128720�!")*129:ABIJQR>�;
4�1
'�$
layer_1_input���������
p 

 
� "���
"�
tensor_0���������
���
�

tensor_1_0 
�

tensor_1_1 
�

tensor_1_2 
�

tensor_1_3 
�

tensor_1_4 
�

tensor_1_5 
�

tensor_1_6 
�

tensor_1_7 �
0__inference_sequential_15_layer_call_fn_12128765u!")*129:ABIJQR>�;
4�1
'�$
layer_1_input���������
p

 
� "!�
unknown����������
0__inference_sequential_15_layer_call_fn_12128810u!")*129:ABIJQR>�;
4�1
'�$
layer_1_input���������
p 

 
� "!�
unknown����������
&__inference_signature_wrapper_12129104�!")*129:ABIJQRG�D
� 
=�:
8
layer_1_input'�$
layer_1_input���������";�8
6
layer_output&�#
layer_output���������