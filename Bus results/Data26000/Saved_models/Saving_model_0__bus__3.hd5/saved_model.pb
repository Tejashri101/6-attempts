��	
��
^
AssignVariableOp
resource
value"dtype"
dtypetype"
validate_shapebool( �
�
BiasAdd

value"T	
bias"T
output"T""
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
8
Const
output"dtype"
valuetensor"
dtypetype
$
DisableCopyOnRead
resource�
.
Identity

input"T
output"T"	
Ttype
2
L2Loss
t"T
output"T"
Ttype:
2
u
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
2	
�
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool("
allow_missing_filesbool( �
?
Mul
x"T
y"T
z"T"
Ttype:
2	�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
@
ReadVariableOp
resource
value"dtype"
dtypetype�
@
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
d
Shape

input"T&
output"out_type��out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
L

StringJoin
inputs*N

output"

Nint("
	separatorstring 
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.13.02v2.13.0-rc2-7-g1cb1a030a628��
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
~
current_learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *&
shared_namecurrent_learning_rate
w
)current_learning_rate/Read/ReadVariableOpReadVariableOpcurrent_learning_rate*
_output_shapes
: *
dtype0
f
	iterationVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	iteration
_
iteration/Read/ReadVariableOpReadVariableOp	iteration*
_output_shapes
: *
dtype0	
z
layer_output/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_namelayer_output/bias
s
%layer_output/bias/Read/ReadVariableOpReadVariableOplayer_output/bias*
_output_shapes
:*
dtype0
�
layer_output/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:-*$
shared_namelayer_output/kernel
{
'layer_output/kernel/Read/ReadVariableOpReadVariableOplayer_output/kernel*
_output_shapes

:-*
dtype0
t
dense_123/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:-*
shared_namedense_123/bias
m
"dense_123/bias/Read/ReadVariableOpReadVariableOpdense_123/bias*
_output_shapes
:-*
dtype0
|
dense_123/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:Q-*!
shared_namedense_123/kernel
u
$dense_123/kernel/Read/ReadVariableOpReadVariableOpdense_123/kernel*
_output_shapes

:Q-*
dtype0
t
dense_122/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:Q*
shared_namedense_122/bias
m
"dense_122/bias/Read/ReadVariableOpReadVariableOpdense_122/bias*
_output_shapes
:Q*
dtype0
|
dense_122/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
Q*!
shared_namedense_122/kernel
u
$dense_122/kernel/Read/ReadVariableOpReadVariableOpdense_122/kernel*
_output_shapes

:
Q*
dtype0
p
layer_1/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:
*
shared_namelayer_1/bias
i
 layer_1/bias/Read/ReadVariableOpReadVariableOplayer_1/bias*
_output_shapes
:
*
dtype0
x
layer_1/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:
*
shared_namelayer_1/kernel
q
"layer_1/kernel/Read/ReadVariableOpReadVariableOplayer_1/kernel*
_output_shapes

:
*
dtype0
�
serving_default_layer_1_inputPlaceholder*'
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCallStatefulPartitionedCallserving_default_layer_1_inputlayer_1/kernellayer_1/biasdense_122/kerneldense_122/biasdense_123/kerneldense_123/biaslayer_output/kernellayer_output/bias*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� */
f*R(
&__inference_signature_wrapper_20874740

NoOpNoOp
�(
ConstConst"/device:CPU:0*
_output_shapes
: *
dtype0*�(
value�(B�( B�(
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
	variables
trainable_variables
regularization_losses
	keras_api
	__call__
*
&call_and_return_all_conditional_losses
_default_save_signature
	optimizer
loss

signatures*
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias*
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias*
�
	variables
 trainable_variables
!regularization_losses
"	keras_api
#__call__
*$&call_and_return_all_conditional_losses

%kernel
&bias*
�
'	variables
(trainable_variables
)regularization_losses
*	keras_api
+__call__
*,&call_and_return_all_conditional_losses

-kernel
.bias*
<
0
1
2
3
%4
&5
-6
.7*
<
0
1
2
3
%4
&5
-6
.7*
:
/0
01
12
23
34
45
56
67* 
�
7non_trainable_variables

8layers
9metrics
:layer_regularization_losses
;layer_metrics
	variables
trainable_variables
regularization_losses
	__call__
_default_save_signature
*
&call_and_return_all_conditional_losses
&
"call_and_return_conditional_losses*

<trace_0
=trace_1* 

>trace_0
?trace_1* 
* 
W
@
_variables
A_iterations
B_current_learning_rate
C_update_step_xla*
* 

Dserving_default* 

0
1*

0
1*

/0
01* 
�
Enon_trainable_variables

Flayers
Gmetrics
Hlayer_regularization_losses
Ilayer_metrics
	variables
trainable_variables
regularization_losses
__call__
Jactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&K"call_and_return_conditional_losses*

Ltrace_0* 

Mtrace_0* 
^X
VARIABLE_VALUElayer_1/kernel6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUE*
ZT
VARIABLE_VALUElayer_1/bias4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUE*

0
1*

0
1*

10
21* 
�
Nnon_trainable_variables

Olayers
Pmetrics
Qlayer_regularization_losses
Rlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
Sactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&T"call_and_return_conditional_losses*

Utrace_0* 

Vtrace_0* 
`Z
VARIABLE_VALUEdense_122/kernel6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUE*
\V
VARIABLE_VALUEdense_122/bias4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUE*

%0
&1*

%0
&1*

30
41* 
�
Wnon_trainable_variables

Xlayers
Ymetrics
Zlayer_regularization_losses
[layer_metrics
	variables
 trainable_variables
!regularization_losses
#__call__
\activity_regularizer_fn
*$&call_and_return_all_conditional_losses
&]"call_and_return_conditional_losses*

^trace_0* 

_trace_0* 
`Z
VARIABLE_VALUEdense_123/kernel6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUE*
\V
VARIABLE_VALUEdense_123/bias4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUE*

-0
.1*

-0
.1*

50
61* 
�
`non_trainable_variables

alayers
bmetrics
clayer_regularization_losses
dlayer_metrics
'	variables
(trainable_variables
)regularization_losses
+__call__
eactivity_regularizer_fn
*,&call_and_return_all_conditional_losses
&f"call_and_return_conditional_losses*

gtrace_0* 

htrace_0* 
c]
VARIABLE_VALUElayer_output/kernel6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUE*
_Y
VARIABLE_VALUElayer_output/bias4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUE*

itrace_0* 

jtrace_0* 

ktrace_0* 

ltrace_0* 

mtrace_0* 

ntrace_0* 

otrace_0* 

ptrace_0* 
* 
 
0
1
2
3*

q0*
* 
* 
* 
* 
* 
* 

A0*
SM
VARIABLE_VALUE	iteration0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUE*
jd
VARIABLE_VALUEcurrent_learning_rate;optimizer/_current_learning_rate/.ATTRIBUTES/VARIABLE_VALUE*
* 
* 
* 
* 
* 

/0
01* 
* 

rtrace_0* 

strace_0* 
* 
* 
* 
* 
* 

10
21* 
* 

ttrace_0* 

utrace_0* 
* 
* 
* 
* 
* 

30
41* 
* 

vtrace_0* 

wtrace_0* 
* 
* 
* 
* 
* 

50
61* 
* 

xtrace_0* 

ytrace_0* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
8
z	variables
{	keras_api
	|total
	}count*
* 
* 
* 
* 
* 
* 
* 
* 

|0
}1*

z	variables*
SM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE*
SM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE*
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�
StatefulPartitionedCall_1StatefulPartitionedCallsaver_filenamelayer_1/kernellayer_1/biasdense_122/kerneldense_122/biasdense_123/kerneldense_123/biaslayer_output/kernellayer_output/bias	iterationcurrent_learning_ratetotalcountConst*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__traced_save_20875133
�
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filenamelayer_1/kernellayer_1/biasdense_122/kerneldense_122/biasdense_123/kerneldense_123/biaslayer_output/kernellayer_output/bias	iterationcurrent_learning_ratetotalcount*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *-
f(R&
$__inference__traced_restore_20875178��
�

�
__inference_loss_fn_3_20874999D
6dense_122_bias_regularizer_abs_readvariableop_resource:Q
identity��-dense_122/bias/Regularizer/Abs/ReadVariableOp�
-dense_122/bias/Regularizer/Abs/ReadVariableOpReadVariableOp6dense_122_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:Q*
dtype0�
dense_122/bias/Regularizer/AbsAbs5dense_122/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Qj
 dense_122/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_122/bias/Regularizer/SumSum"dense_122/bias/Regularizer/Abs:y:0)dense_122/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_122/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_122/bias/Regularizer/mulMul)dense_122/bias/Regularizer/mul/x:output:0'dense_122/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: `
IdentityIdentity"dense_122/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: R
NoOpNoOp.^dense_122/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2^
-dense_122/bias/Regularizer/Abs/ReadVariableOp-dense_122/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
I__inference_layer_1_layer_call_and_return_all_conditional_losses_20874808

inputs
unknown:

	unknown_0:

identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_20874258�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *:
f5R3
1__inference_layer_1_activity_regularizer_20874212o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
20874800:($
"
_user_specified_name
20874802
�
�
0__inference_sequential_31_layer_call_fn_20874590
layer_1_input
unknown:

	unknown_0:

	unknown_1:
Q
	unknown_2:Q
	unknown_3:Q-
	unknown_4:-
	unknown_5:-
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout	
2*
_collective_manager_ids
 */
_output_shapes
:���������: : : : **
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874540o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:���������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
20874568:($
"
_user_specified_name
20874570:($
"
_user_specified_name
20874572:($
"
_user_specified_name
20874574:($
"
_user_specified_name
20874576:($
"
_user_specified_name
20874578:($
"
_user_specified_name
20874580:($
"
_user_specified_name
20874582
�
�
__inference_loss_fn_0_20874969H
6layer_1_kernel_regularizer_abs_readvariableop_resource:

identity��-layer_1/kernel/Regularizer/Abs/ReadVariableOp�
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp6layer_1_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: `
IdentityIdentity"layer_1/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: R
NoOpNoOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
__inference_loss_fn_4_20875009J
8dense_123_kernel_regularizer_abs_readvariableop_resource:Q-
identity��/dense_123/kernel/Regularizer/Abs/ReadVariableOp�
/dense_123/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp8dense_123_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:Q-*
dtype0�
 dense_123/kernel/Regularizer/AbsAbs7dense_123/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Q-s
"dense_123/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_123/kernel/Regularizer/SumSum$dense_123/kernel/Regularizer/Abs:y:0+dense_123/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_123/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_123/kernel/Regularizer/mulMul+dense_123/kernel/Regularizer/mul/x:output:0)dense_123/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: b
IdentityIdentity$dense_123/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: T
NoOpNoOp0^dense_123/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2b
/dense_123/kernel/Regularizer/Abs/ReadVariableOp/dense_123/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
__inference_loss_fn_7_20875039G
9layer_output_bias_regularizer_abs_readvariableop_resource:
identity��0layer_output/bias/Regularizer/Abs/ReadVariableOp�
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOp9layer_output_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: c
IdentityIdentity%layer_output/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: U
NoOpNoOp1^layer_output/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
G__inference_dense_123_layer_call_and_return_conditional_losses_20874917

inputs0
matmul_readvariableop_resource:Q--
biasadd_readvariableop_resource:-
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�-dense_123/bias/Regularizer/Abs/ReadVariableOp�/dense_123/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:Q-*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������-r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:-*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������-P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������-�
/dense_123/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:Q-*
dtype0�
 dense_123/kernel/Regularizer/AbsAbs7dense_123/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Q-s
"dense_123/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_123/kernel/Regularizer/SumSum$dense_123/kernel/Regularizer/Abs:y:0+dense_123/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_123/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_123/kernel/Regularizer/mulMul+dense_123/kernel/Regularizer/mul/x:output:0)dense_123/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
-dense_123/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:-*
dtype0�
dense_123/bias/Regularizer/AbsAbs5dense_123/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:-j
 dense_123/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_123/bias/Regularizer/SumSum"dense_123/bias/Regularizer/Abs:y:0)dense_123/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_123/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_123/bias/Regularizer/mulMul)dense_123/bias/Regularizer/mul/x:output:0'dense_123/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������-�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp.^dense_123/bias/Regularizer/Abs/ReadVariableOp0^dense_123/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������Q: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2^
-dense_123/bias/Regularizer/Abs/ReadVariableOp-dense_123/bias/Regularizer/Abs/ReadVariableOp2b
/dense_123/kernel/Regularizer/Abs/ReadVariableOp/dense_123/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������Q
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�j
�
#__inference__wrapped_model_20874205
layer_1_inputF
4sequential_31_layer_1_matmul_readvariableop_resource:
C
5sequential_31_layer_1_biasadd_readvariableop_resource:
H
6sequential_31_dense_122_matmul_readvariableop_resource:
QE
7sequential_31_dense_122_biasadd_readvariableop_resource:QH
6sequential_31_dense_123_matmul_readvariableop_resource:Q-E
7sequential_31_dense_123_biasadd_readvariableop_resource:-K
9sequential_31_layer_output_matmul_readvariableop_resource:-H
:sequential_31_layer_output_biasadd_readvariableop_resource:
identity��.sequential_31/dense_122/BiasAdd/ReadVariableOp�-sequential_31/dense_122/MatMul/ReadVariableOp�.sequential_31/dense_123/BiasAdd/ReadVariableOp�-sequential_31/dense_123/MatMul/ReadVariableOp�,sequential_31/layer_1/BiasAdd/ReadVariableOp�+sequential_31/layer_1/MatMul/ReadVariableOp�1sequential_31/layer_output/BiasAdd/ReadVariableOp�0sequential_31/layer_output/MatMul/ReadVariableOp�
+sequential_31/layer_1/MatMul/ReadVariableOpReadVariableOp4sequential_31_layer_1_matmul_readvariableop_resource*
_output_shapes

:
*
dtype0�
sequential_31/layer_1/MatMulMatMullayer_1_input3sequential_31/layer_1/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
�
,sequential_31/layer_1/BiasAdd/ReadVariableOpReadVariableOp5sequential_31_layer_1_biasadd_readvariableop_resource*
_output_shapes
:
*
dtype0�
sequential_31/layer_1/BiasAddBiasAdd&sequential_31/layer_1/MatMul:product:04sequential_31/layer_1/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
|
sequential_31/layer_1/ReluRelu&sequential_31/layer_1/BiasAdd:output:0*
T0*'
_output_shapes
:���������
�
0sequential_31/layer_1/ActivityRegularizer/L2LossL2Loss(sequential_31/layer_1/Relu:activations:0*
T0*
_output_shapes
: t
/sequential_31/layer_1/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
-sequential_31/layer_1/ActivityRegularizer/mulMul8sequential_31/layer_1/ActivityRegularizer/mul/x:output:09sequential_31/layer_1/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
/sequential_31/layer_1/ActivityRegularizer/ShapeShape(sequential_31/layer_1/Relu:activations:0*
T0*
_output_shapes
::���
=sequential_31/layer_1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
?sequential_31/layer_1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
?sequential_31/layer_1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
7sequential_31/layer_1/ActivityRegularizer/strided_sliceStridedSlice8sequential_31/layer_1/ActivityRegularizer/Shape:output:0Fsequential_31/layer_1/ActivityRegularizer/strided_slice/stack:output:0Hsequential_31/layer_1/ActivityRegularizer/strided_slice/stack_1:output:0Hsequential_31/layer_1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
.sequential_31/layer_1/ActivityRegularizer/CastCast@sequential_31/layer_1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
1sequential_31/layer_1/ActivityRegularizer/truedivRealDiv1sequential_31/layer_1/ActivityRegularizer/mul:z:02sequential_31/layer_1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
-sequential_31/dense_122/MatMul/ReadVariableOpReadVariableOp6sequential_31_dense_122_matmul_readvariableop_resource*
_output_shapes

:
Q*
dtype0�
sequential_31/dense_122/MatMulMatMul(sequential_31/layer_1/Relu:activations:05sequential_31/dense_122/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Q�
.sequential_31/dense_122/BiasAdd/ReadVariableOpReadVariableOp7sequential_31_dense_122_biasadd_readvariableop_resource*
_output_shapes
:Q*
dtype0�
sequential_31/dense_122/BiasAddBiasAdd(sequential_31/dense_122/MatMul:product:06sequential_31/dense_122/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Q�
sequential_31/dense_122/ReluRelu(sequential_31/dense_122/BiasAdd:output:0*
T0*'
_output_shapes
:���������Q�
2sequential_31/dense_122/ActivityRegularizer/L2LossL2Loss*sequential_31/dense_122/Relu:activations:0*
T0*
_output_shapes
: v
1sequential_31/dense_122/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
/sequential_31/dense_122/ActivityRegularizer/mulMul:sequential_31/dense_122/ActivityRegularizer/mul/x:output:0;sequential_31/dense_122/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
1sequential_31/dense_122/ActivityRegularizer/ShapeShape*sequential_31/dense_122/Relu:activations:0*
T0*
_output_shapes
::���
?sequential_31/dense_122/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Asequential_31/dense_122/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Asequential_31/dense_122/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
9sequential_31/dense_122/ActivityRegularizer/strided_sliceStridedSlice:sequential_31/dense_122/ActivityRegularizer/Shape:output:0Hsequential_31/dense_122/ActivityRegularizer/strided_slice/stack:output:0Jsequential_31/dense_122/ActivityRegularizer/strided_slice/stack_1:output:0Jsequential_31/dense_122/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
0sequential_31/dense_122/ActivityRegularizer/CastCastBsequential_31/dense_122/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
3sequential_31/dense_122/ActivityRegularizer/truedivRealDiv3sequential_31/dense_122/ActivityRegularizer/mul:z:04sequential_31/dense_122/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
-sequential_31/dense_123/MatMul/ReadVariableOpReadVariableOp6sequential_31_dense_123_matmul_readvariableop_resource*
_output_shapes

:Q-*
dtype0�
sequential_31/dense_123/MatMulMatMul*sequential_31/dense_122/Relu:activations:05sequential_31/dense_123/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������-�
.sequential_31/dense_123/BiasAdd/ReadVariableOpReadVariableOp7sequential_31_dense_123_biasadd_readvariableop_resource*
_output_shapes
:-*
dtype0�
sequential_31/dense_123/BiasAddBiasAdd(sequential_31/dense_123/MatMul:product:06sequential_31/dense_123/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������-�
sequential_31/dense_123/ReluRelu(sequential_31/dense_123/BiasAdd:output:0*
T0*'
_output_shapes
:���������-�
2sequential_31/dense_123/ActivityRegularizer/L2LossL2Loss*sequential_31/dense_123/Relu:activations:0*
T0*
_output_shapes
: v
1sequential_31/dense_123/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
/sequential_31/dense_123/ActivityRegularizer/mulMul:sequential_31/dense_123/ActivityRegularizer/mul/x:output:0;sequential_31/dense_123/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
1sequential_31/dense_123/ActivityRegularizer/ShapeShape*sequential_31/dense_123/Relu:activations:0*
T0*
_output_shapes
::���
?sequential_31/dense_123/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Asequential_31/dense_123/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Asequential_31/dense_123/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
9sequential_31/dense_123/ActivityRegularizer/strided_sliceStridedSlice:sequential_31/dense_123/ActivityRegularizer/Shape:output:0Hsequential_31/dense_123/ActivityRegularizer/strided_slice/stack:output:0Jsequential_31/dense_123/ActivityRegularizer/strided_slice/stack_1:output:0Jsequential_31/dense_123/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
0sequential_31/dense_123/ActivityRegularizer/CastCastBsequential_31/dense_123/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
3sequential_31/dense_123/ActivityRegularizer/truedivRealDiv3sequential_31/dense_123/ActivityRegularizer/mul:z:04sequential_31/dense_123/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
0sequential_31/layer_output/MatMul/ReadVariableOpReadVariableOp9sequential_31_layer_output_matmul_readvariableop_resource*
_output_shapes

:-*
dtype0�
!sequential_31/layer_output/MatMulMatMul*sequential_31/dense_123/Relu:activations:08sequential_31/layer_output/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
1sequential_31/layer_output/BiasAdd/ReadVariableOpReadVariableOp:sequential_31_layer_output_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
"sequential_31/layer_output/BiasAddBiasAdd+sequential_31/layer_output/MatMul:product:09sequential_31/layer_output/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
5sequential_31/layer_output/ActivityRegularizer/L2LossL2Loss+sequential_31/layer_output/BiasAdd:output:0*
T0*
_output_shapes
: y
4sequential_31/layer_output/ActivityRegularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=�
2sequential_31/layer_output/ActivityRegularizer/mulMul=sequential_31/layer_output/ActivityRegularizer/mul/x:output:0>sequential_31/layer_output/ActivityRegularizer/L2Loss:output:0*
T0*
_output_shapes
: �
4sequential_31/layer_output/ActivityRegularizer/ShapeShape+sequential_31/layer_output/BiasAdd:output:0*
T0*
_output_shapes
::���
Bsequential_31/layer_output/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Dsequential_31/layer_output/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Dsequential_31/layer_output/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
<sequential_31/layer_output/ActivityRegularizer/strided_sliceStridedSlice=sequential_31/layer_output/ActivityRegularizer/Shape:output:0Ksequential_31/layer_output/ActivityRegularizer/strided_slice/stack:output:0Msequential_31/layer_output/ActivityRegularizer/strided_slice/stack_1:output:0Msequential_31/layer_output/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
3sequential_31/layer_output/ActivityRegularizer/CastCastEsequential_31/layer_output/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
6sequential_31/layer_output/ActivityRegularizer/truedivRealDiv6sequential_31/layer_output/ActivityRegularizer/mul:z:07sequential_31/layer_output/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: z
IdentityIdentity+sequential_31/layer_output/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp/^sequential_31/dense_122/BiasAdd/ReadVariableOp.^sequential_31/dense_122/MatMul/ReadVariableOp/^sequential_31/dense_123/BiasAdd/ReadVariableOp.^sequential_31/dense_123/MatMul/ReadVariableOp-^sequential_31/layer_1/BiasAdd/ReadVariableOp,^sequential_31/layer_1/MatMul/ReadVariableOp2^sequential_31/layer_output/BiasAdd/ReadVariableOp1^sequential_31/layer_output/MatMul/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:���������: : : : : : : : 2`
.sequential_31/dense_122/BiasAdd/ReadVariableOp.sequential_31/dense_122/BiasAdd/ReadVariableOp2^
-sequential_31/dense_122/MatMul/ReadVariableOp-sequential_31/dense_122/MatMul/ReadVariableOp2`
.sequential_31/dense_123/BiasAdd/ReadVariableOp.sequential_31/dense_123/BiasAdd/ReadVariableOp2^
-sequential_31/dense_123/MatMul/ReadVariableOp-sequential_31/dense_123/MatMul/ReadVariableOp2\
,sequential_31/layer_1/BiasAdd/ReadVariableOp,sequential_31/layer_1/BiasAdd/ReadVariableOp2Z
+sequential_31/layer_1/MatMul/ReadVariableOp+sequential_31/layer_1/MatMul/ReadVariableOp2f
1sequential_31/layer_output/BiasAdd/ReadVariableOp1sequential_31/layer_output/BiasAdd/ReadVariableOp2d
0sequential_31/layer_output/MatMul/ReadVariableOp0sequential_31/layer_output/MatMul/ReadVariableOp:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
0__inference_sequential_31_layer_call_fn_20874565
layer_1_input
unknown:

	unknown_0:

	unknown_1:
Q
	unknown_2:Q
	unknown_3:Q-
	unknown_4:-
	unknown_5:-
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout	
2*
_collective_manager_ids
 */
_output_shapes
:���������: : : : **
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *T
fORM
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874432o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:���������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
20874543:($
"
_user_specified_name
20874545:($
"
_user_specified_name
20874547:($
"
_user_specified_name
20874549:($
"
_user_specified_name
20874551:($
"
_user_specified_name
20874553:($
"
_user_specified_name
20874555:($
"
_user_specified_name
20874557
�
�
K__inference_dense_122_layer_call_and_return_all_conditional_losses_20874851

inputs
unknown:
Q
	unknown_0:Q
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������Q*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_122_layer_call_and_return_conditional_losses_20874294�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *<
f7R5
3__inference_dense_122_activity_regularizer_20874219o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������QX

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
20874843:($
"
_user_specified_name
20874845
�
�
*__inference_layer_1_layer_call_fn_20874797

inputs
unknown:

	unknown_0:

identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_20874258o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������
<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
20874791:($
"
_user_specified_name
20874793
�
�
N__inference_layer_output_layer_call_and_return_all_conditional_losses_20874937

inputs
unknown:-
	unknown_0:
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_20874365�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *?
f:R8
6__inference_layer_output_activity_regularizer_20874233o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������-: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������-
 
_user_specified_nameinputs:($
"
_user_specified_name
20874929:($
"
_user_specified_name
20874931
�d
�

!__inference__traced_save_20875133
file_prefix7
%read_disablecopyonread_layer_1_kernel:
3
%read_1_disablecopyonread_layer_1_bias:
;
)read_2_disablecopyonread_dense_122_kernel:
Q5
'read_3_disablecopyonread_dense_122_bias:Q;
)read_4_disablecopyonread_dense_123_kernel:Q-5
'read_5_disablecopyonread_dense_123_bias:->
,read_6_disablecopyonread_layer_output_kernel:-8
*read_7_disablecopyonread_layer_output_bias:,
"read_8_disablecopyonread_iteration:	 8
.read_9_disablecopyonread_current_learning_rate: )
read_10_disablecopyonread_total: )
read_11_disablecopyonread_count: 
savev2_const
identity_25��MergeV2Checkpoints�Read/DisableCopyOnRead�Read/ReadVariableOp�Read_1/DisableCopyOnRead�Read_1/ReadVariableOp�Read_10/DisableCopyOnRead�Read_10/ReadVariableOp�Read_11/DisableCopyOnRead�Read_11/ReadVariableOp�Read_2/DisableCopyOnRead�Read_2/ReadVariableOp�Read_3/DisableCopyOnRead�Read_3/ReadVariableOp�Read_4/DisableCopyOnRead�Read_4/ReadVariableOp�Read_5/DisableCopyOnRead�Read_5/ReadVariableOp�Read_6/DisableCopyOnRead�Read_6/ReadVariableOp�Read_7/DisableCopyOnRead�Read_7/ReadVariableOp�Read_8/DisableCopyOnRead�Read_8/ReadVariableOp�Read_9/DisableCopyOnRead�Read_9/ReadVariableOpw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: w
Read/DisableCopyOnReadDisableCopyOnRead%read_disablecopyonread_layer_1_kernel"/device:CPU:0*
_output_shapes
 �
Read/ReadVariableOpReadVariableOp%read_disablecopyonread_layer_1_kernel^Read/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:
*
dtype0i
IdentityIdentityRead/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:
a

Identity_1IdentityIdentity:output:0"/device:CPU:0*
T0*
_output_shapes

:
y
Read_1/DisableCopyOnReadDisableCopyOnRead%read_1_disablecopyonread_layer_1_bias"/device:CPU:0*
_output_shapes
 �
Read_1/ReadVariableOpReadVariableOp%read_1_disablecopyonread_layer_1_bias^Read_1/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:
*
dtype0i

Identity_2IdentityRead_1/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:
_

Identity_3IdentityIdentity_2:output:0"/device:CPU:0*
T0*
_output_shapes
:
}
Read_2/DisableCopyOnReadDisableCopyOnRead)read_2_disablecopyonread_dense_122_kernel"/device:CPU:0*
_output_shapes
 �
Read_2/ReadVariableOpReadVariableOp)read_2_disablecopyonread_dense_122_kernel^Read_2/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:
Q*
dtype0m

Identity_4IdentityRead_2/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:
Qc

Identity_5IdentityIdentity_4:output:0"/device:CPU:0*
T0*
_output_shapes

:
Q{
Read_3/DisableCopyOnReadDisableCopyOnRead'read_3_disablecopyonread_dense_122_bias"/device:CPU:0*
_output_shapes
 �
Read_3/ReadVariableOpReadVariableOp'read_3_disablecopyonread_dense_122_bias^Read_3/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:Q*
dtype0i

Identity_6IdentityRead_3/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:Q_

Identity_7IdentityIdentity_6:output:0"/device:CPU:0*
T0*
_output_shapes
:Q}
Read_4/DisableCopyOnReadDisableCopyOnRead)read_4_disablecopyonread_dense_123_kernel"/device:CPU:0*
_output_shapes
 �
Read_4/ReadVariableOpReadVariableOp)read_4_disablecopyonread_dense_123_kernel^Read_4/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:Q-*
dtype0m

Identity_8IdentityRead_4/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:Q-c

Identity_9IdentityIdentity_8:output:0"/device:CPU:0*
T0*
_output_shapes

:Q-{
Read_5/DisableCopyOnReadDisableCopyOnRead'read_5_disablecopyonread_dense_123_bias"/device:CPU:0*
_output_shapes
 �
Read_5/ReadVariableOpReadVariableOp'read_5_disablecopyonread_dense_123_bias^Read_5/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:-*
dtype0j
Identity_10IdentityRead_5/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:-a
Identity_11IdentityIdentity_10:output:0"/device:CPU:0*
T0*
_output_shapes
:-�
Read_6/DisableCopyOnReadDisableCopyOnRead,read_6_disablecopyonread_layer_output_kernel"/device:CPU:0*
_output_shapes
 �
Read_6/ReadVariableOpReadVariableOp,read_6_disablecopyonread_layer_output_kernel^Read_6/DisableCopyOnRead"/device:CPU:0*
_output_shapes

:-*
dtype0n
Identity_12IdentityRead_6/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes

:-e
Identity_13IdentityIdentity_12:output:0"/device:CPU:0*
T0*
_output_shapes

:-~
Read_7/DisableCopyOnReadDisableCopyOnRead*read_7_disablecopyonread_layer_output_bias"/device:CPU:0*
_output_shapes
 �
Read_7/ReadVariableOpReadVariableOp*read_7_disablecopyonread_layer_output_bias^Read_7/DisableCopyOnRead"/device:CPU:0*
_output_shapes
:*
dtype0j
Identity_14IdentityRead_7/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
:a
Identity_15IdentityIdentity_14:output:0"/device:CPU:0*
T0*
_output_shapes
:v
Read_8/DisableCopyOnReadDisableCopyOnRead"read_8_disablecopyonread_iteration"/device:CPU:0*
_output_shapes
 �
Read_8/ReadVariableOpReadVariableOp"read_8_disablecopyonread_iteration^Read_8/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0	f
Identity_16IdentityRead_8/ReadVariableOp:value:0"/device:CPU:0*
T0	*
_output_shapes
: ]
Identity_17IdentityIdentity_16:output:0"/device:CPU:0*
T0	*
_output_shapes
: �
Read_9/DisableCopyOnReadDisableCopyOnRead.read_9_disablecopyonread_current_learning_rate"/device:CPU:0*
_output_shapes
 �
Read_9/ReadVariableOpReadVariableOp.read_9_disablecopyonread_current_learning_rate^Read_9/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0f
Identity_18IdentityRead_9/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_19IdentityIdentity_18:output:0"/device:CPU:0*
T0*
_output_shapes
: t
Read_10/DisableCopyOnReadDisableCopyOnReadread_10_disablecopyonread_total"/device:CPU:0*
_output_shapes
 �
Read_10/ReadVariableOpReadVariableOpread_10_disablecopyonread_total^Read_10/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_20IdentityRead_10/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_21IdentityIdentity_20:output:0"/device:CPU:0*
T0*
_output_shapes
: t
Read_11/DisableCopyOnReadDisableCopyOnReadread_11_disablecopyonread_count"/device:CPU:0*
_output_shapes
 �
Read_11/ReadVariableOpReadVariableOpread_11_disablecopyonread_count^Read_11/DisableCopyOnRead"/device:CPU:0*
_output_shapes
: *
dtype0g
Identity_22IdentityRead_11/ReadVariableOp:value:0"/device:CPU:0*
T0*
_output_shapes
: ]
Identity_23IdentityIdentity_22:output:0"/device:CPU:0*
T0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB;optimizer/_current_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*-
value$B"B B B B B B B B B B B B B �
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0Identity_1:output:0Identity_3:output:0Identity_5:output:0Identity_7:output:0Identity_9:output:0Identity_11:output:0Identity_13:output:0Identity_15:output:0Identity_17:output:0Identity_19:output:0Identity_21:output:0Identity_23:output:0savev2_const"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtypes
2	�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 i
Identity_24Identityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: U
Identity_25IdentityIdentity_24:output:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp^MergeV2Checkpoints^Read/DisableCopyOnRead^Read/ReadVariableOp^Read_1/DisableCopyOnRead^Read_1/ReadVariableOp^Read_10/DisableCopyOnRead^Read_10/ReadVariableOp^Read_11/DisableCopyOnRead^Read_11/ReadVariableOp^Read_2/DisableCopyOnRead^Read_2/ReadVariableOp^Read_3/DisableCopyOnRead^Read_3/ReadVariableOp^Read_4/DisableCopyOnRead^Read_4/ReadVariableOp^Read_5/DisableCopyOnRead^Read_5/ReadVariableOp^Read_6/DisableCopyOnRead^Read_6/ReadVariableOp^Read_7/DisableCopyOnRead^Read_7/ReadVariableOp^Read_8/DisableCopyOnRead^Read_8/ReadVariableOp^Read_9/DisableCopyOnRead^Read_9/ReadVariableOp*
_output_shapes
 "#
identity_25Identity_25:output:0*(
_construction_contextkEagerRuntime*/
_input_shapes
: : : : : : : : : : : : : : 2(
MergeV2CheckpointsMergeV2Checkpoints20
Read/DisableCopyOnReadRead/DisableCopyOnRead2*
Read/ReadVariableOpRead/ReadVariableOp24
Read_1/DisableCopyOnReadRead_1/DisableCopyOnRead2.
Read_1/ReadVariableOpRead_1/ReadVariableOp26
Read_10/DisableCopyOnReadRead_10/DisableCopyOnRead20
Read_10/ReadVariableOpRead_10/ReadVariableOp26
Read_11/DisableCopyOnReadRead_11/DisableCopyOnRead20
Read_11/ReadVariableOpRead_11/ReadVariableOp24
Read_2/DisableCopyOnReadRead_2/DisableCopyOnRead2.
Read_2/ReadVariableOpRead_2/ReadVariableOp24
Read_3/DisableCopyOnReadRead_3/DisableCopyOnRead2.
Read_3/ReadVariableOpRead_3/ReadVariableOp24
Read_4/DisableCopyOnReadRead_4/DisableCopyOnRead2.
Read_4/ReadVariableOpRead_4/ReadVariableOp24
Read_5/DisableCopyOnReadRead_5/DisableCopyOnRead2.
Read_5/ReadVariableOpRead_5/ReadVariableOp24
Read_6/DisableCopyOnReadRead_6/DisableCopyOnRead2.
Read_6/ReadVariableOpRead_6/ReadVariableOp24
Read_7/DisableCopyOnReadRead_7/DisableCopyOnRead2.
Read_7/ReadVariableOpRead_7/ReadVariableOp24
Read_8/DisableCopyOnReadRead_8/DisableCopyOnRead2.
Read_8/ReadVariableOpRead_8/ReadVariableOp24
Read_9/DisableCopyOnReadRead_9/DisableCopyOnRead2.
Read_9/ReadVariableOpRead_9/ReadVariableOp:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:.*
(
_user_specified_namelayer_1/kernel:,(
&
_user_specified_namelayer_1/bias:0,
*
_user_specified_namedense_122/kernel:.*
(
_user_specified_namedense_122/bias:0,
*
_user_specified_namedense_123/kernel:.*
(
_user_specified_namedense_123/bias:3/
-
_user_specified_namelayer_output/kernel:1-
+
_user_specified_namelayer_output/bias:)	%
#
_user_specified_name	iteration:5
1
/
_user_specified_namecurrent_learning_rate:%!

_user_specified_nametotal:%!

_user_specified_namecount:=9

_output_shapes
: 

_user_specified_nameConst
�
�
,__inference_dense_122_layer_call_fn_20874840

inputs
unknown:
Q
	unknown_0:Q
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������Q*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_122_layer_call_and_return_conditional_losses_20874294o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������Q<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
20874834:($
"
_user_specified_name
20874836
�
�
/__inference_layer_output_layer_call_fn_20874926

inputs
unknown:-
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_20874365o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������-: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������-
 
_user_specified_nameinputs:($
"
_user_specified_name
20874920:($
"
_user_specified_name
20874922
�
�
G__inference_dense_122_layer_call_and_return_conditional_losses_20874294

inputs0
matmul_readvariableop_resource:
Q-
biasadd_readvariableop_resource:Q
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�-dense_122/bias/Regularizer/Abs/ReadVariableOp�/dense_122/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
Q*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Qr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:Q*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������QP
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������Q�
/dense_122/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
Q*
dtype0�
 dense_122/kernel/Regularizer/AbsAbs7dense_122/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
Qs
"dense_122/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_122/kernel/Regularizer/SumSum$dense_122/kernel/Regularizer/Abs:y:0+dense_122/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_122/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_122/kernel/Regularizer/mulMul+dense_122/kernel/Regularizer/mul/x:output:0)dense_122/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
-dense_122/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:Q*
dtype0�
dense_122/bias/Regularizer/AbsAbs5dense_122/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Qj
 dense_122/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_122/bias/Regularizer/SumSum"dense_122/bias/Regularizer/Abs:y:0)dense_122/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_122/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_122/bias/Regularizer/mulMul)dense_122/bias/Regularizer/mul/x:output:0'dense_122/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������Q�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp.^dense_122/bias/Regularizer/Abs/ReadVariableOp0^dense_122/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2^
-dense_122/bias/Regularizer/Abs/ReadVariableOp-dense_122/bias/Regularizer/Abs/ReadVariableOp2b
/dense_122/kernel/Regularizer/Abs/ReadVariableOp/dense_122/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874432
layer_1_input"
layer_1_20874259:

layer_1_20874261:
$
dense_122_20874295:
Q 
dense_122_20874297:Q$
dense_123_20874331:Q- 
dense_123_20874333:-'
layer_output_20874366:-#
layer_output_20874368:
identity

identity_1

identity_2

identity_3

identity_4��!dense_122/StatefulPartitionedCall�-dense_122/bias/Regularizer/Abs/ReadVariableOp�/dense_122/kernel/Regularizer/Abs/ReadVariableOp�!dense_123/StatefulPartitionedCall�-dense_123/bias/Regularizer/Abs/ReadVariableOp�/dense_123/kernel/Regularizer/Abs/ReadVariableOp�layer_1/StatefulPartitionedCall�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOp�$layer_output/StatefulPartitionedCall�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOp�
layer_1/StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputlayer_1_20874259layer_1_20874261*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_20874258�
+layer_1/ActivityRegularizer/PartitionedCallPartitionedCall(layer_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *:
f5R3
1__inference_layer_1_activity_regularizer_20874212�
!layer_1/ActivityRegularizer/ShapeShape(layer_1/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��y
/layer_1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1layer_1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1layer_1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)layer_1/ActivityRegularizer/strided_sliceStridedSlice*layer_1/ActivityRegularizer/Shape:output:08layer_1/ActivityRegularizer/strided_slice/stack:output:0:layer_1/ActivityRegularizer/strided_slice/stack_1:output:0:layer_1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
 layer_1/ActivityRegularizer/CastCast2layer_1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
#layer_1/ActivityRegularizer/truedivRealDiv4layer_1/ActivityRegularizer/PartitionedCall:output:0$layer_1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
!dense_122/StatefulPartitionedCallStatefulPartitionedCall(layer_1/StatefulPartitionedCall:output:0dense_122_20874295dense_122_20874297*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������Q*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_122_layer_call_and_return_conditional_losses_20874294�
-dense_122/ActivityRegularizer/PartitionedCallPartitionedCall*dense_122/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *<
f7R5
3__inference_dense_122_activity_regularizer_20874219�
#dense_122/ActivityRegularizer/ShapeShape*dense_122/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��{
1dense_122/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3dense_122/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3dense_122/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+dense_122/ActivityRegularizer/strided_sliceStridedSlice,dense_122/ActivityRegularizer/Shape:output:0:dense_122/ActivityRegularizer/strided_slice/stack:output:0<dense_122/ActivityRegularizer/strided_slice/stack_1:output:0<dense_122/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
"dense_122/ActivityRegularizer/CastCast4dense_122/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
%dense_122/ActivityRegularizer/truedivRealDiv6dense_122/ActivityRegularizer/PartitionedCall:output:0&dense_122/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
!dense_123/StatefulPartitionedCallStatefulPartitionedCall*dense_122/StatefulPartitionedCall:output:0dense_123_20874331dense_123_20874333*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������-*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_123_layer_call_and_return_conditional_losses_20874330�
-dense_123/ActivityRegularizer/PartitionedCallPartitionedCall*dense_123/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *<
f7R5
3__inference_dense_123_activity_regularizer_20874226�
#dense_123/ActivityRegularizer/ShapeShape*dense_123/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��{
1dense_123/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3dense_123/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3dense_123/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+dense_123/ActivityRegularizer/strided_sliceStridedSlice,dense_123/ActivityRegularizer/Shape:output:0:dense_123/ActivityRegularizer/strided_slice/stack:output:0<dense_123/ActivityRegularizer/strided_slice/stack_1:output:0<dense_123/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
"dense_123/ActivityRegularizer/CastCast4dense_123/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
%dense_123/ActivityRegularizer/truedivRealDiv6dense_123/ActivityRegularizer/PartitionedCall:output:0&dense_123/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
$layer_output/StatefulPartitionedCallStatefulPartitionedCall*dense_123/StatefulPartitionedCall:output:0layer_output_20874366layer_output_20874368*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_20874365�
0layer_output/ActivityRegularizer/PartitionedCallPartitionedCall-layer_output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *?
f:R8
6__inference_layer_output_activity_regularizer_20874233�
&layer_output/ActivityRegularizer/ShapeShape-layer_output/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��~
4layer_output/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6layer_output/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6layer_output/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.layer_output/ActivityRegularizer/strided_sliceStridedSlice/layer_output/ActivityRegularizer/Shape:output:0=layer_output/ActivityRegularizer/strided_slice/stack:output:0?layer_output/ActivityRegularizer/strided_slice/stack_1:output:0?layer_output/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
%layer_output/ActivityRegularizer/CastCast7layer_output/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
(layer_output/ActivityRegularizer/truedivRealDiv9layer_output/ActivityRegularizer/PartitionedCall:output:0)layer_output/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: ~
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_20874259*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: x
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_20874261*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
/dense_122/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_122_20874295*
_output_shapes

:
Q*
dtype0�
 dense_122/kernel/Regularizer/AbsAbs7dense_122/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
Qs
"dense_122/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_122/kernel/Regularizer/SumSum$dense_122/kernel/Regularizer/Abs:y:0+dense_122/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_122/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_122/kernel/Regularizer/mulMul+dense_122/kernel/Regularizer/mul/x:output:0)dense_122/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
-dense_122/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_122_20874297*
_output_shapes
:Q*
dtype0�
dense_122/bias/Regularizer/AbsAbs5dense_122/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Qj
 dense_122/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_122/bias/Regularizer/SumSum"dense_122/bias/Regularizer/Abs:y:0)dense_122/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_122/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_122/bias/Regularizer/mulMul)dense_122/bias/Regularizer/mul/x:output:0'dense_122/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
/dense_123/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_123_20874331*
_output_shapes

:Q-*
dtype0�
 dense_123/kernel/Regularizer/AbsAbs7dense_123/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Q-s
"dense_123/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_123/kernel/Regularizer/SumSum$dense_123/kernel/Regularizer/Abs:y:0+dense_123/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_123/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_123/kernel/Regularizer/mulMul+dense_123/kernel/Regularizer/mul/x:output:0)dense_123/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
-dense_123/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_123_20874333*
_output_shapes
:-*
dtype0�
dense_123/bias/Regularizer/AbsAbs5dense_123/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:-j
 dense_123/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_123/bias/Regularizer/SumSum"dense_123/bias/Regularizer/Abs:y:0)dense_123/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_123/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_123/bias/Regularizer/mulMul)dense_123/bias/Regularizer/mul/x:output:0'dense_123/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_20874366*
_output_shapes

:-*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:-v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_20874368*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
IdentityIdentity-layer_output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������g

Identity_1Identity'layer_1/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: i

Identity_2Identity)dense_122/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: i

Identity_3Identity)dense_123/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: l

Identity_4Identity,layer_output/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp"^dense_122/StatefulPartitionedCall.^dense_122/bias/Regularizer/Abs/ReadVariableOp0^dense_122/kernel/Regularizer/Abs/ReadVariableOp"^dense_123/StatefulPartitionedCall.^dense_123/bias/Regularizer/Abs/ReadVariableOp0^dense_123/kernel/Regularizer/Abs/ReadVariableOp ^layer_1/StatefulPartitionedCall,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp%^layer_output/StatefulPartitionedCall1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:���������: : : : : : : : 2F
!dense_122/StatefulPartitionedCall!dense_122/StatefulPartitionedCall2^
-dense_122/bias/Regularizer/Abs/ReadVariableOp-dense_122/bias/Regularizer/Abs/ReadVariableOp2b
/dense_122/kernel/Regularizer/Abs/ReadVariableOp/dense_122/kernel/Regularizer/Abs/ReadVariableOp2F
!dense_123/StatefulPartitionedCall!dense_123/StatefulPartitionedCall2^
-dense_123/bias/Regularizer/Abs/ReadVariableOp-dense_123/bias/Regularizer/Abs/ReadVariableOp2b
/dense_123/kernel/Regularizer/Abs/ReadVariableOp/dense_123/kernel/Regularizer/Abs/ReadVariableOp2B
layer_1/StatefulPartitionedCalllayer_1/StatefulPartitionedCall2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp2L
$layer_output/StatefulPartitionedCall$layer_output/StatefulPartitionedCall2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
20874259:($
"
_user_specified_name
20874261:($
"
_user_specified_name
20874295:($
"
_user_specified_name
20874297:($
"
_user_specified_name
20874331:($
"
_user_specified_name
20874333:($
"
_user_specified_name
20874366:($
"
_user_specified_name
20874368
�

�
__inference_loss_fn_5_20875019D
6dense_123_bias_regularizer_abs_readvariableop_resource:-
identity��-dense_123/bias/Regularizer/Abs/ReadVariableOp�
-dense_123/bias/Regularizer/Abs/ReadVariableOpReadVariableOp6dense_123_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:-*
dtype0�
dense_123/bias/Regularizer/AbsAbs5dense_123/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:-j
 dense_123/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_123/bias/Regularizer/SumSum"dense_123/bias/Regularizer/Abs:y:0)dense_123/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_123/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_123/bias/Regularizer/mulMul)dense_123/bias/Regularizer/mul/x:output:0'dense_123/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: `
IdentityIdentity"dense_123/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: R
NoOpNoOp.^dense_123/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2^
-dense_123/bias/Regularizer/Abs/ReadVariableOp-dense_123/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
E__inference_layer_1_layer_call_and_return_conditional_losses_20874258

inputs0
matmul_readvariableop_resource:
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������
�
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
J__inference_layer_output_layer_call_and_return_conditional_losses_20874365

inputs0
matmul_readvariableop_resource:--
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:-*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:-*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:-v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������-: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������-
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
,__inference_dense_123_layer_call_fn_20874883

inputs
unknown:Q-
	unknown_0:-
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������-*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_123_layer_call_and_return_conditional_losses_20874330o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������-<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������Q: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������Q
 
_user_specified_nameinputs:($
"
_user_specified_name
20874877:($
"
_user_specified_name
20874879
�
J
3__inference_dense_123_activity_regularizer_20874226
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
J
3__inference_dense_122_activity_regularizer_20874219
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
J__inference_layer_output_layer_call_and_return_conditional_losses_20874959

inputs0
matmul_readvariableop_resource:--
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:-*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:-*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:-v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: _
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������-: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������-
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
M
6__inference_layer_output_activity_regularizer_20874233
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
K__inference_dense_123_layer_call_and_return_all_conditional_losses_20874894

inputs
unknown:Q-
	unknown_0:-
identity

identity_1��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������-*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_123_layer_call_and_return_conditional_losses_20874330�
PartitionedCallPartitionedCall StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *<
f7R5
3__inference_dense_123_activity_regularizer_20874226o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������-X

Identity_1IdentityPartitionedCall:output:0^NoOp*
T0*
_output_shapes
: <
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������Q: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������Q
 
_user_specified_nameinputs:($
"
_user_specified_name
20874886:($
"
_user_specified_name
20874888
�
�
__inference_loss_fn_2_20874989J
8dense_122_kernel_regularizer_abs_readvariableop_resource:
Q
identity��/dense_122/kernel/Regularizer/Abs/ReadVariableOp�
/dense_122/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp8dense_122_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:
Q*
dtype0�
 dense_122/kernel/Regularizer/AbsAbs7dense_122/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
Qs
"dense_122/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_122/kernel/Regularizer/SumSum$dense_122/kernel/Regularizer/Abs:y:0+dense_122/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_122/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_122/kernel/Regularizer/mulMul+dense_122/kernel/Regularizer/mul/x:output:0)dense_122/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: b
IdentityIdentity$dense_122/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: T
NoOpNoOp0^dense_122/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2b
/dense_122/kernel/Regularizer/Abs/ReadVariableOp/dense_122/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
G__inference_dense_123_layer_call_and_return_conditional_losses_20874330

inputs0
matmul_readvariableop_resource:Q--
biasadd_readvariableop_resource:-
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�-dense_123/bias/Regularizer/Abs/ReadVariableOp�/dense_123/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:Q-*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������-r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:-*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������-P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������-�
/dense_123/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:Q-*
dtype0�
 dense_123/kernel/Regularizer/AbsAbs7dense_123/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Q-s
"dense_123/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_123/kernel/Regularizer/SumSum$dense_123/kernel/Regularizer/Abs:y:0+dense_123/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_123/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_123/kernel/Regularizer/mulMul+dense_123/kernel/Regularizer/mul/x:output:0)dense_123/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
-dense_123/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:-*
dtype0�
dense_123/bias/Regularizer/AbsAbs5dense_123/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:-j
 dense_123/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_123/bias/Regularizer/SumSum"dense_123/bias/Regularizer/Abs:y:0)dense_123/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_123/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_123/bias/Regularizer/mulMul)dense_123/bias/Regularizer/mul/x:output:0'dense_123/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������-�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp.^dense_123/bias/Regularizer/Abs/ReadVariableOp0^dense_123/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������Q: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2^
-dense_123/bias/Regularizer/Abs/ReadVariableOp-dense_123/bias/Regularizer/Abs/ReadVariableOp2b
/dense_123/kernel/Regularizer/Abs/ReadVariableOp/dense_123/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������Q
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
&__inference_signature_wrapper_20874740
layer_1_input
unknown:

	unknown_0:

	unknown_1:
Q
	unknown_2:Q
	unknown_3:Q-
	unknown_4:-
	unknown_5:-
	unknown_6:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5	unknown_6*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������**
_read_only_resource_inputs

*-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference__wrapped_model_20874205o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������<
NoOpNoOp^StatefulPartitionedCall*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:���������: : : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
20874722:($
"
_user_specified_name
20874724:($
"
_user_specified_name
20874726:($
"
_user_specified_name
20874728:($
"
_user_specified_name
20874730:($
"
_user_specified_name
20874732:($
"
_user_specified_name
20874734:($
"
_user_specified_name
20874736
�
�
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874540
layer_1_input"
layer_1_20874435:

layer_1_20874437:
$
dense_122_20874448:
Q 
dense_122_20874450:Q$
dense_123_20874461:Q- 
dense_123_20874463:-'
layer_output_20874474:-#
layer_output_20874476:
identity

identity_1

identity_2

identity_3

identity_4��!dense_122/StatefulPartitionedCall�-dense_122/bias/Regularizer/Abs/ReadVariableOp�/dense_122/kernel/Regularizer/Abs/ReadVariableOp�!dense_123/StatefulPartitionedCall�-dense_123/bias/Regularizer/Abs/ReadVariableOp�/dense_123/kernel/Regularizer/Abs/ReadVariableOp�layer_1/StatefulPartitionedCall�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOp�$layer_output/StatefulPartitionedCall�0layer_output/bias/Regularizer/Abs/ReadVariableOp�2layer_output/kernel/Regularizer/Abs/ReadVariableOp�
layer_1/StatefulPartitionedCallStatefulPartitionedCalllayer_1_inputlayer_1_20874435layer_1_20874437*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������
*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_layer_1_layer_call_and_return_conditional_losses_20874258�
+layer_1/ActivityRegularizer/PartitionedCallPartitionedCall(layer_1/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *:
f5R3
1__inference_layer_1_activity_regularizer_20874212�
!layer_1/ActivityRegularizer/ShapeShape(layer_1/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��y
/layer_1/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: {
1layer_1/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:{
1layer_1/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
)layer_1/ActivityRegularizer/strided_sliceStridedSlice*layer_1/ActivityRegularizer/Shape:output:08layer_1/ActivityRegularizer/strided_slice/stack:output:0:layer_1/ActivityRegularizer/strided_slice/stack_1:output:0:layer_1/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
 layer_1/ActivityRegularizer/CastCast2layer_1/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
#layer_1/ActivityRegularizer/truedivRealDiv4layer_1/ActivityRegularizer/PartitionedCall:output:0$layer_1/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
!dense_122/StatefulPartitionedCallStatefulPartitionedCall(layer_1/StatefulPartitionedCall:output:0dense_122_20874448dense_122_20874450*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������Q*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_122_layer_call_and_return_conditional_losses_20874294�
-dense_122/ActivityRegularizer/PartitionedCallPartitionedCall*dense_122/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *<
f7R5
3__inference_dense_122_activity_regularizer_20874219�
#dense_122/ActivityRegularizer/ShapeShape*dense_122/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��{
1dense_122/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3dense_122/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3dense_122/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+dense_122/ActivityRegularizer/strided_sliceStridedSlice,dense_122/ActivityRegularizer/Shape:output:0:dense_122/ActivityRegularizer/strided_slice/stack:output:0<dense_122/ActivityRegularizer/strided_slice/stack_1:output:0<dense_122/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
"dense_122/ActivityRegularizer/CastCast4dense_122/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
%dense_122/ActivityRegularizer/truedivRealDiv6dense_122/ActivityRegularizer/PartitionedCall:output:0&dense_122/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
!dense_123/StatefulPartitionedCallStatefulPartitionedCall*dense_122/StatefulPartitionedCall:output:0dense_123_20874461dense_123_20874463*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������-*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_dense_123_layer_call_and_return_conditional_losses_20874330�
-dense_123/ActivityRegularizer/PartitionedCallPartitionedCall*dense_123/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *<
f7R5
3__inference_dense_123_activity_regularizer_20874226�
#dense_123/ActivityRegularizer/ShapeShape*dense_123/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��{
1dense_123/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: }
3dense_123/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:}
3dense_123/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
+dense_123/ActivityRegularizer/strided_sliceStridedSlice,dense_123/ActivityRegularizer/Shape:output:0:dense_123/ActivityRegularizer/strided_slice/stack:output:0<dense_123/ActivityRegularizer/strided_slice/stack_1:output:0<dense_123/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
"dense_123/ActivityRegularizer/CastCast4dense_123/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
%dense_123/ActivityRegularizer/truedivRealDiv6dense_123/ActivityRegularizer/PartitionedCall:output:0&dense_123/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: �
$layer_output/StatefulPartitionedCallStatefulPartitionedCall*dense_123/StatefulPartitionedCall:output:0layer_output_20874474layer_output_20874476*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *S
fNRL
J__inference_layer_output_layer_call_and_return_conditional_losses_20874365�
0layer_output/ActivityRegularizer/PartitionedCallPartitionedCall-layer_output/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *?
f:R8
6__inference_layer_output_activity_regularizer_20874233�
&layer_output/ActivityRegularizer/ShapeShape-layer_output/StatefulPartitionedCall:output:0*
T0*
_output_shapes
::��~
4layer_output/ActivityRegularizer/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6layer_output/ActivityRegularizer/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6layer_output/ActivityRegularizer/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.layer_output/ActivityRegularizer/strided_sliceStridedSlice/layer_output/ActivityRegularizer/Shape:output:0=layer_output/ActivityRegularizer/strided_slice/stack:output:0?layer_output/ActivityRegularizer/strided_slice/stack_1:output:0?layer_output/ActivityRegularizer/strided_slice/stack_2:output:0*
Index0*
T0*
_output_shapes
: *
shrink_axis_mask�
%layer_output/ActivityRegularizer/CastCast7layer_output/ActivityRegularizer/strided_slice:output:0*

DstT0*

SrcT0*
_output_shapes
: �
(layer_output/ActivityRegularizer/truedivRealDiv9layer_output/ActivityRegularizer/PartitionedCall:output:0)layer_output/ActivityRegularizer/Cast:y:0*
T0*
_output_shapes
: ~
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_20874435*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: x
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_1_20874437*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
/dense_122/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_122_20874448*
_output_shapes

:
Q*
dtype0�
 dense_122/kernel/Regularizer/AbsAbs7dense_122/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
Qs
"dense_122/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_122/kernel/Regularizer/SumSum$dense_122/kernel/Regularizer/Abs:y:0+dense_122/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_122/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_122/kernel/Regularizer/mulMul+dense_122/kernel/Regularizer/mul/x:output:0)dense_122/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
-dense_122/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_122_20874450*
_output_shapes
:Q*
dtype0�
dense_122/bias/Regularizer/AbsAbs5dense_122/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Qj
 dense_122/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_122/bias/Regularizer/SumSum"dense_122/bias/Regularizer/Abs:y:0)dense_122/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_122/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_122/bias/Regularizer/mulMul)dense_122/bias/Regularizer/mul/x:output:0'dense_122/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
/dense_123/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpdense_123_20874461*
_output_shapes

:Q-*
dtype0�
 dense_123/kernel/Regularizer/AbsAbs7dense_123/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:Q-s
"dense_123/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_123/kernel/Regularizer/SumSum$dense_123/kernel/Regularizer/Abs:y:0+dense_123/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_123/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_123/kernel/Regularizer/mulMul+dense_123/kernel/Regularizer/mul/x:output:0)dense_123/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
-dense_123/bias/Regularizer/Abs/ReadVariableOpReadVariableOpdense_123_20874463*
_output_shapes
:-*
dtype0�
dense_123/bias/Regularizer/AbsAbs5dense_123/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:-j
 dense_123/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_123/bias/Regularizer/SumSum"dense_123/bias/Regularizer/Abs:y:0)dense_123/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_123/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_123/bias/Regularizer/mulMul)dense_123/bias/Regularizer/mul/x:output:0'dense_123/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_20874474*
_output_shapes

:-*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:-v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
0layer_output/bias/Regularizer/Abs/ReadVariableOpReadVariableOplayer_output_20874476*
_output_shapes
:*
dtype0�
!layer_output/bias/Regularizer/AbsAbs8layer_output/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:m
#layer_output/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
!layer_output/bias/Regularizer/SumSum%layer_output/bias/Regularizer/Abs:y:0,layer_output/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: h
#layer_output/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
!layer_output/bias/Regularizer/mulMul,layer_output/bias/Regularizer/mul/x:output:0*layer_output/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: |
IdentityIdentity-layer_output/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������g

Identity_1Identity'layer_1/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: i

Identity_2Identity)dense_122/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: i

Identity_3Identity)dense_123/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: l

Identity_4Identity,layer_output/ActivityRegularizer/truediv:z:0^NoOp*
T0*
_output_shapes
: �
NoOpNoOp"^dense_122/StatefulPartitionedCall.^dense_122/bias/Regularizer/Abs/ReadVariableOp0^dense_122/kernel/Regularizer/Abs/ReadVariableOp"^dense_123/StatefulPartitionedCall.^dense_123/bias/Regularizer/Abs/ReadVariableOp0^dense_123/kernel/Regularizer/Abs/ReadVariableOp ^layer_1/StatefulPartitionedCall,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp%^layer_output/StatefulPartitionedCall1^layer_output/bias/Regularizer/Abs/ReadVariableOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0*(
_construction_contextkEagerRuntime*6
_input_shapes%
#:���������: : : : : : : : 2F
!dense_122/StatefulPartitionedCall!dense_122/StatefulPartitionedCall2^
-dense_122/bias/Regularizer/Abs/ReadVariableOp-dense_122/bias/Regularizer/Abs/ReadVariableOp2b
/dense_122/kernel/Regularizer/Abs/ReadVariableOp/dense_122/kernel/Regularizer/Abs/ReadVariableOp2F
!dense_123/StatefulPartitionedCall!dense_123/StatefulPartitionedCall2^
-dense_123/bias/Regularizer/Abs/ReadVariableOp-dense_123/bias/Regularizer/Abs/ReadVariableOp2b
/dense_123/kernel/Regularizer/Abs/ReadVariableOp/dense_123/kernel/Regularizer/Abs/ReadVariableOp2B
layer_1/StatefulPartitionedCalllayer_1/StatefulPartitionedCall2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp2L
$layer_output/StatefulPartitionedCall$layer_output/StatefulPartitionedCall2d
0layer_output/bias/Regularizer/Abs/ReadVariableOp0layer_output/bias/Regularizer/Abs/ReadVariableOp2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:V R
'
_output_shapes
:���������
'
_user_specified_namelayer_1_input:($
"
_user_specified_name
20874435:($
"
_user_specified_name
20874437:($
"
_user_specified_name
20874448:($
"
_user_specified_name
20874450:($
"
_user_specified_name
20874461:($
"
_user_specified_name
20874463:($
"
_user_specified_name
20874474:($
"
_user_specified_name
20874476
�:
�
$__inference__traced_restore_20875178
file_prefix1
assignvariableop_layer_1_kernel:
-
assignvariableop_1_layer_1_bias:
5
#assignvariableop_2_dense_122_kernel:
Q/
!assignvariableop_3_dense_122_bias:Q5
#assignvariableop_4_dense_123_kernel:Q-/
!assignvariableop_5_dense_123_bias:-8
&assignvariableop_6_layer_output_kernel:-2
$assignvariableop_7_layer_output_bias:&
assignvariableop_8_iteration:	 2
(assignvariableop_9_current_learning_rate: #
assignvariableop_10_total: #
assignvariableop_11_count: 
identity_13��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_2�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B6layer_with_weights-0/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-0/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-1/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-1/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-2/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-2/bias/.ATTRIBUTES/VARIABLE_VALUEB6layer_with_weights-3/kernel/.ATTRIBUTES/VARIABLE_VALUEB4layer_with_weights-3/bias/.ATTRIBUTES/VARIABLE_VALUEB0optimizer/_iterations/.ATTRIBUTES/VARIABLE_VALUEB;optimizer/_current_learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*-
value$B"B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*H
_output_shapes6
4:::::::::::::*
dtypes
2	[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOpassignvariableop_layer_1_kernelIdentity:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOpassignvariableop_1_layer_1_biasIdentity_1:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOp#assignvariableop_2_dense_122_kernelIdentity_2:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_3IdentityRestoreV2:tensors:3"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_3AssignVariableOp!assignvariableop_3_dense_122_biasIdentity_3:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_4IdentityRestoreV2:tensors:4"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOp#assignvariableop_4_dense_123_kernelIdentity_4:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:5"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOp!assignvariableop_5_dense_123_biasIdentity_5:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOp&assignvariableop_6_layer_output_kernelIdentity_6:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOp$assignvariableop_7_layer_output_biasIdentity_7:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0]

Identity_8IdentityRestoreV2:tensors:8"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_8AssignVariableOpassignvariableop_8_iterationIdentity_8:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0	]

Identity_9IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOp(assignvariableop_9_current_learning_rateIdentity_9:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOpassignvariableop_10_totalIdentity_10:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOpassignvariableop_11_countIdentity_11:output:0"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 *
dtype0Y
NoOpNoOp"/device:CPU:0*&
 _has_manual_control_dependencies(*
_output_shapes
 �
Identity_12Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_13IdentityIdentity_12:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_2^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_9*
_output_shapes
 "#
identity_13Identity_13:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
: : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112(
AssignVariableOp_2AssignVariableOp_22(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_9:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:.*
(
_user_specified_namelayer_1/kernel:,(
&
_user_specified_namelayer_1/bias:0,
*
_user_specified_namedense_122/kernel:.*
(
_user_specified_namedense_122/bias:0,
*
_user_specified_namedense_123/kernel:.*
(
_user_specified_namedense_123/bias:3/
-
_user_specified_namelayer_output/kernel:1-
+
_user_specified_namelayer_output/bias:)	%
#
_user_specified_name	iteration:5
1
/
_user_specified_namecurrent_learning_rate:%!

_user_specified_nametotal:%!

_user_specified_namecount
�

�
__inference_loss_fn_1_20874979B
4layer_1_bias_regularizer_abs_readvariableop_resource:

identity��+layer_1/bias/Regularizer/Abs/ReadVariableOp�
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOp4layer_1_bias_regularizer_abs_readvariableop_resource*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: ^
IdentityIdentity layer_1/bias/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: P
NoOpNoOp,^layer_1/bias/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
H
1__inference_layer_1_activity_regularizer_20874212
x
identity4
L2LossL2Lossx*
T0*
_output_shapes
: J
mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș�=L
mulMulmul/x:output:0L2Loss:output:0*
T0*
_output_shapes
: >
IdentityIdentitymul:z:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
::; 7

_output_shapes
:

_user_specified_namex
�
�
__inference_loss_fn_6_20875029M
;layer_output_kernel_regularizer_abs_readvariableop_resource:-
identity��2layer_output/kernel/Regularizer/Abs/ReadVariableOp�
2layer_output/kernel/Regularizer/Abs/ReadVariableOpReadVariableOp;layer_output_kernel_regularizer_abs_readvariableop_resource*
_output_shapes

:-*
dtype0�
#layer_output/kernel/Regularizer/AbsAbs:layer_output/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:-v
%layer_output/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
#layer_output/kernel/Regularizer/SumSum'layer_output/kernel/Regularizer/Abs:y:0.layer_output/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: j
%layer_output/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
#layer_output/kernel/Regularizer/mulMul.layer_output/kernel/Regularizer/mul/x:output:0,layer_output/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: e
IdentityIdentity'layer_output/kernel/Regularizer/mul:z:0^NoOp*
T0*
_output_shapes
: W
NoOpNoOp3^layer_output/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: 2h
2layer_output/kernel/Regularizer/Abs/ReadVariableOp2layer_output/kernel/Regularizer/Abs/ReadVariableOp:( $
"
_user_specified_name
resource
�
�
E__inference_layer_1_layer_call_and_return_conditional_losses_20874831

inputs0
matmul_readvariableop_resource:
-
biasadd_readvariableop_resource:

identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�+layer_1/bias/Regularizer/Abs/ReadVariableOp�-layer_1/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������
P
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������
�
-layer_1/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
*
dtype0�
layer_1/kernel/Regularizer/AbsAbs5layer_1/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
q
 layer_1/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
layer_1/kernel/Regularizer/SumSum"layer_1/kernel/Regularizer/Abs:y:0)layer_1/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 layer_1/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
layer_1/kernel/Regularizer/mulMul)layer_1/kernel/Regularizer/mul/x:output:0'layer_1/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
+layer_1/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:
*
dtype0}
layer_1/bias/Regularizer/AbsAbs3layer_1/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:
h
layer_1/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
layer_1/bias/Regularizer/SumSum layer_1/bias/Regularizer/Abs:y:0'layer_1/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: c
layer_1/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
layer_1/bias/Regularizer/mulMul'layer_1/bias/Regularizer/mul/x:output:0%layer_1/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������
�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp,^layer_1/bias/Regularizer/Abs/ReadVariableOp.^layer_1/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2Z
+layer_1/bias/Regularizer/Abs/ReadVariableOp+layer_1/bias/Regularizer/Abs/ReadVariableOp2^
-layer_1/kernel/Regularizer/Abs/ReadVariableOp-layer_1/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource
�
�
G__inference_dense_122_layer_call_and_return_conditional_losses_20874874

inputs0
matmul_readvariableop_resource:
Q-
biasadd_readvariableop_resource:Q
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOp�-dense_122/bias/Regularizer/Abs/ReadVariableOp�/dense_122/kernel/Regularizer/Abs/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
Q*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������Qr
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:Q*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������QP
ReluReluBiasAdd:output:0*
T0*'
_output_shapes
:���������Q�
/dense_122/kernel/Regularizer/Abs/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:
Q*
dtype0�
 dense_122/kernel/Regularizer/AbsAbs7dense_122/kernel/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes

:
Qs
"dense_122/kernel/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB"       �
 dense_122/kernel/Regularizer/SumSum$dense_122/kernel/Regularizer/Abs:y:0+dense_122/kernel/Regularizer/Const:output:0*
T0*
_output_shapes
: g
"dense_122/kernel/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *Ș;=�
 dense_122/kernel/Regularizer/mulMul+dense_122/kernel/Regularizer/mul/x:output:0)dense_122/kernel/Regularizer/Sum:output:0*
T0*
_output_shapes
: �
-dense_122/bias/Regularizer/Abs/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:Q*
dtype0�
dense_122/bias/Regularizer/AbsAbs5dense_122/bias/Regularizer/Abs/ReadVariableOp:value:0*
T0*
_output_shapes
:Qj
 dense_122/bias/Regularizer/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
dense_122/bias/Regularizer/SumSum"dense_122/bias/Regularizer/Abs:y:0)dense_122/bias/Regularizer/Const:output:0*
T0*
_output_shapes
: e
 dense_122/bias/Regularizer/mul/xConst*
_output_shapes
: *
dtype0*
valueB
 *�Ga=�
dense_122/bias/Regularizer/mulMul)dense_122/bias/Regularizer/mul/x:output:0'dense_122/bias/Regularizer/Sum:output:0*
T0*
_output_shapes
: a
IdentityIdentityRelu:activations:0^NoOp*
T0*'
_output_shapes
:���������Q�
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp.^dense_122/bias/Regularizer/Abs/ReadVariableOp0^dense_122/kernel/Regularizer/Abs/ReadVariableOp*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������
: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp2^
-dense_122/bias/Regularizer/Abs/ReadVariableOp-dense_122/bias/Regularizer/Abs/ReadVariableOp2b
/dense_122/kernel/Regularizer/Abs/ReadVariableOp/dense_122/kernel/Regularizer/Abs/ReadVariableOp:O K
'
_output_shapes
:���������

 
_user_specified_nameinputs:($
"
_user_specified_name
resource:($
"
_user_specified_name
resource"�L
saver_filename:0StatefulPartitionedCall_1:0StatefulPartitionedCall_28"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
G
layer_1_input6
serving_default_layer_1_input:0���������@
layer_output0
StatefulPartitionedCall:0���������tensorflow/serving/predict:��
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
layer_with_weights-2
layer-2
layer_with_weights-3
layer-3
	variables
trainable_variables
regularization_losses
	keras_api
	__call__
*
&call_and_return_all_conditional_losses
_default_save_signature
	optimizer
loss

signatures"
_tf_keras_sequential
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias"
_tf_keras_layer
�
	variables
trainable_variables
regularization_losses
	keras_api
__call__
*&call_and_return_all_conditional_losses

kernel
bias"
_tf_keras_layer
�
	variables
 trainable_variables
!regularization_losses
"	keras_api
#__call__
*$&call_and_return_all_conditional_losses

%kernel
&bias"
_tf_keras_layer
�
'	variables
(trainable_variables
)regularization_losses
*	keras_api
+__call__
*,&call_and_return_all_conditional_losses

-kernel
.bias"
_tf_keras_layer
X
0
1
2
3
%4
&5
-6
.7"
trackable_list_wrapper
X
0
1
2
3
%4
&5
-6
.7"
trackable_list_wrapper
X
/0
01
12
23
34
45
56
67"
trackable_list_wrapper
�
7non_trainable_variables

8layers
9metrics
:layer_regularization_losses
;layer_metrics
	variables
trainable_variables
regularization_losses
	__call__
_default_save_signature
*
&call_and_return_all_conditional_losses
&
"call_and_return_conditional_losses"
_generic_user_object
�
<trace_0
=trace_12�
0__inference_sequential_31_layer_call_fn_20874565
0__inference_sequential_31_layer_call_fn_20874590�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z<trace_0z=trace_1
�
>trace_0
?trace_12�
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874432
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874540�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z>trace_0z?trace_1
�B�
#__inference__wrapped_model_20874205layer_1_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
r
@
_variables
A_iterations
B_current_learning_rate
C_update_step_xla"
experimentalOptimizer
 "
trackable_list_wrapper
,
Dserving_default"
signature_map
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
�
Enon_trainable_variables

Flayers
Gmetrics
Hlayer_regularization_losses
Ilayer_metrics
	variables
trainable_variables
regularization_losses
__call__
Jactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&K"call_and_return_conditional_losses"
_generic_user_object
�
Ltrace_02�
*__inference_layer_1_layer_call_fn_20874797�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zLtrace_0
�
Mtrace_02�
I__inference_layer_1_layer_call_and_return_all_conditional_losses_20874808�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zMtrace_0
 :
2layer_1/kernel
:
2layer_1/bias
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
10
21"
trackable_list_wrapper
�
Nnon_trainable_variables

Olayers
Pmetrics
Qlayer_regularization_losses
Rlayer_metrics
	variables
trainable_variables
regularization_losses
__call__
Sactivity_regularizer_fn
*&call_and_return_all_conditional_losses
&T"call_and_return_conditional_losses"
_generic_user_object
�
Utrace_02�
,__inference_dense_122_layer_call_fn_20874840�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zUtrace_0
�
Vtrace_02�
K__inference_dense_122_layer_call_and_return_all_conditional_losses_20874851�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zVtrace_0
": 
Q2dense_122/kernel
:Q2dense_122/bias
.
%0
&1"
trackable_list_wrapper
.
%0
&1"
trackable_list_wrapper
.
30
41"
trackable_list_wrapper
�
Wnon_trainable_variables

Xlayers
Ymetrics
Zlayer_regularization_losses
[layer_metrics
	variables
 trainable_variables
!regularization_losses
#__call__
\activity_regularizer_fn
*$&call_and_return_all_conditional_losses
&]"call_and_return_conditional_losses"
_generic_user_object
�
^trace_02�
,__inference_dense_123_layer_call_fn_20874883�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z^trace_0
�
_trace_02�
K__inference_dense_123_layer_call_and_return_all_conditional_losses_20874894�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 z_trace_0
": Q-2dense_123/kernel
:-2dense_123/bias
.
-0
.1"
trackable_list_wrapper
.
-0
.1"
trackable_list_wrapper
.
50
61"
trackable_list_wrapper
�
`non_trainable_variables

alayers
bmetrics
clayer_regularization_losses
dlayer_metrics
'	variables
(trainable_variables
)regularization_losses
+__call__
eactivity_regularizer_fn
*,&call_and_return_all_conditional_losses
&f"call_and_return_conditional_losses"
_generic_user_object
�
gtrace_02�
/__inference_layer_output_layer_call_fn_20874926�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zgtrace_0
�
htrace_02�
N__inference_layer_output_layer_call_and_return_all_conditional_losses_20874937�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zhtrace_0
%:#-2layer_output/kernel
:2layer_output/bias
�
itrace_02�
__inference_loss_fn_0_20874969�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zitrace_0
�
jtrace_02�
__inference_loss_fn_1_20874979�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zjtrace_0
�
ktrace_02�
__inference_loss_fn_2_20874989�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zktrace_0
�
ltrace_02�
__inference_loss_fn_3_20874999�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zltrace_0
�
mtrace_02�
__inference_loss_fn_4_20875009�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zmtrace_0
�
ntrace_02�
__inference_loss_fn_5_20875019�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zntrace_0
�
otrace_02�
__inference_loss_fn_6_20875029�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zotrace_0
�
ptrace_02�
__inference_loss_fn_7_20875039�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� zptrace_0
 "
trackable_list_wrapper
<
0
1
2
3"
trackable_list_wrapper
'
q0"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
�B�
0__inference_sequential_31_layer_call_fn_20874565layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
0__inference_sequential_31_layer_call_fn_20874590layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874432layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874540layer_1_input"�
���
FullArgSpec)
args!�
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
'
A0"
trackable_list_wrapper
:	 2	iteration
: 2current_learning_rate
�2��
���
FullArgSpec*
args"�

jgradient

jvariable
jkey
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 0
�B�
&__inference_signature_wrapper_20874740layer_1_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
/0
01"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
rtrace_02�
1__inference_layer_1_activity_regularizer_20874212�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�zrtrace_0
�
strace_02�
E__inference_layer_1_layer_call_and_return_conditional_losses_20874831�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zstrace_0
�B�
*__inference_layer_1_layer_call_fn_20874797inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
I__inference_layer_1_layer_call_and_return_all_conditional_losses_20874808inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
10
21"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
ttrace_02�
3__inference_dense_122_activity_regularizer_20874219�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�zttrace_0
�
utrace_02�
G__inference_dense_122_layer_call_and_return_conditional_losses_20874874�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zutrace_0
�B�
,__inference_dense_122_layer_call_fn_20874840inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
K__inference_dense_122_layer_call_and_return_all_conditional_losses_20874851inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
30
41"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
vtrace_02�
3__inference_dense_123_activity_regularizer_20874226�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�zvtrace_0
�
wtrace_02�
G__inference_dense_123_layer_call_and_return_conditional_losses_20874917�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zwtrace_0
�B�
,__inference_dense_123_layer_call_fn_20874883inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
K__inference_dense_123_layer_call_and_return_all_conditional_losses_20874894inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
.
50
61"
trackable_list_wrapper
 "
trackable_dict_wrapper
�
xtrace_02�
6__inference_layer_output_activity_regularizer_20874233�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�zxtrace_0
�
ytrace_02�
J__inference_layer_output_layer_call_and_return_conditional_losses_20874959�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 zytrace_0
�B�
/__inference_layer_output_layer_call_fn_20874926inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
N__inference_layer_output_layer_call_and_return_all_conditional_losses_20874937inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
__inference_loss_fn_0_20874969"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_1_20874979"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_2_20874989"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_3_20874999"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_4_20875009"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_5_20875019"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_6_20875029"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_loss_fn_7_20875039"�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
N
z	variables
{	keras_api
	|total
	}count"
_tf_keras_metric
�B�
1__inference_layer_1_activity_regularizer_20874212x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
E__inference_layer_1_layer_call_and_return_conditional_losses_20874831inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
3__inference_dense_122_activity_regularizer_20874219x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
G__inference_dense_122_layer_call_and_return_conditional_losses_20874874inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
3__inference_dense_123_activity_regularizer_20874226x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
G__inference_dense_123_layer_call_and_return_conditional_losses_20874917inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�B�
6__inference_layer_output_activity_regularizer_20874233x"�
���
FullArgSpec
args�
jx
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
�B�
J__inference_layer_output_layer_call_and_return_conditional_losses_20874959inputs"�
���
FullArgSpec
args�

jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
.
|0
}1"
trackable_list_wrapper
-
z	variables"
_generic_user_object
:  (2total
:  (2count�
#__inference__wrapped_model_20874205%&-.6�3
,�)
'�$
layer_1_input���������
� ";�8
6
layer_output&�#
layer_output���������f
3__inference_dense_122_activity_regularizer_20874219/�
�
�	
x
� "�
unknown �
K__inference_dense_122_layer_call_and_return_all_conditional_losses_20874851x/�,
%�"
 �
inputs���������

� "A�>
"�
tensor_0���������Q
�
�

tensor_1_0 �
G__inference_dense_122_layer_call_and_return_conditional_losses_20874874c/�,
%�"
 �
inputs���������

� ",�)
"�
tensor_0���������Q
� �
,__inference_dense_122_layer_call_fn_20874840X/�,
%�"
 �
inputs���������

� "!�
unknown���������Qf
3__inference_dense_123_activity_regularizer_20874226/�
�
�	
x
� "�
unknown �
K__inference_dense_123_layer_call_and_return_all_conditional_losses_20874894x%&/�,
%�"
 �
inputs���������Q
� "A�>
"�
tensor_0���������-
�
�

tensor_1_0 �
G__inference_dense_123_layer_call_and_return_conditional_losses_20874917c%&/�,
%�"
 �
inputs���������Q
� ",�)
"�
tensor_0���������-
� �
,__inference_dense_123_layer_call_fn_20874883X%&/�,
%�"
 �
inputs���������Q
� "!�
unknown���������-d
1__inference_layer_1_activity_regularizer_20874212/�
�
�	
x
� "�
unknown �
I__inference_layer_1_layer_call_and_return_all_conditional_losses_20874808x/�,
%�"
 �
inputs���������
� "A�>
"�
tensor_0���������

�
�

tensor_1_0 �
E__inference_layer_1_layer_call_and_return_conditional_losses_20874831c/�,
%�"
 �
inputs���������
� ",�)
"�
tensor_0���������

� �
*__inference_layer_1_layer_call_fn_20874797X/�,
%�"
 �
inputs���������
� "!�
unknown���������
i
6__inference_layer_output_activity_regularizer_20874233/�
�
�	
x
� "�
unknown �
N__inference_layer_output_layer_call_and_return_all_conditional_losses_20874937x-./�,
%�"
 �
inputs���������-
� "A�>
"�
tensor_0���������
�
�

tensor_1_0 �
J__inference_layer_output_layer_call_and_return_conditional_losses_20874959c-./�,
%�"
 �
inputs���������-
� ",�)
"�
tensor_0���������
� �
/__inference_layer_output_layer_call_fn_20874926X-./�,
%�"
 �
inputs���������-
� "!�
unknown���������F
__inference_loss_fn_0_20874969$�

� 
� "�
unknown F
__inference_loss_fn_1_20874979$�

� 
� "�
unknown F
__inference_loss_fn_2_20874989$�

� 
� "�
unknown F
__inference_loss_fn_3_20874999$�

� 
� "�
unknown F
__inference_loss_fn_4_20875009$%�

� 
� "�
unknown F
__inference_loss_fn_5_20875019$&�

� 
� "�
unknown F
__inference_loss_fn_6_20875029$-�

� 
� "�
unknown F
__inference_loss_fn_7_20875039$.�

� 
� "�
unknown �
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874432�%&-.>�;
4�1
'�$
layer_1_input���������
p

 
� "��}
"�
tensor_0���������
W�T
�

tensor_1_0 
�

tensor_1_1 
�

tensor_1_2 
�

tensor_1_3 �
K__inference_sequential_31_layer_call_and_return_conditional_losses_20874540�%&-.>�;
4�1
'�$
layer_1_input���������
p 

 
� "��}
"�
tensor_0���������
W�T
�

tensor_1_0 
�

tensor_1_1 
�

tensor_1_2 
�

tensor_1_3 �
0__inference_sequential_31_layer_call_fn_20874565m%&-.>�;
4�1
'�$
layer_1_input���������
p

 
� "!�
unknown����������
0__inference_sequential_31_layer_call_fn_20874590m%&-.>�;
4�1
'�$
layer_1_input���������
p 

 
� "!�
unknown����������
&__inference_signature_wrapper_20874740�%&-.G�D
� 
=�:
8
layer_1_input'�$
layer_1_input���������";�8
6
layer_output&�#
layer_output���������