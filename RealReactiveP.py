import numpy as np
import matplotlib.pyplot as plt
import os

def RealReact(graph_name, phi=45):

    # Define the parameters
    t = np.linspace(0, 2*np.pi, 100)
    f = .2  # Frequency
    w = 2 * np.pi * f
    i_amplitude = .8*5  # Amplitude of i(t)
    u_amplitude = 5 # Amplitude of u(t)

    i_t = i_amplitude * np.cos((w * t))
    u_t = u_amplitude * np.cos((w * t) + np.deg2rad(phi))

    I_t = i_t / (2**0.5)
    U_t = u_t / (2**0.5)

    real_p = U_t * I_t * np.cos(np.deg2rad(phi))
    app_p = -(U_t * I_t * np.sin(np.deg2rad(phi)))
    s_t = real_p + app_p

    plt.figure(figsize=(10, 6))
    plt.plot(t, i_t, label='$i(t) = i \cdot \cos(wt)$', color='red')
    plt.plot(t, u_t, label='$u(t) = u \cdot \cos(wt + \phi)$', color='blue')
    plt.plot(t, s_t, label='$s(t) = U \cdot I \cdot \cos(\phi) (REAL) - U \cdot I \cdot \sin(\phi) (APPARENT)$', color='turquoise')
    plt.fill_between(t, 0, s_t, color='turquoise', alpha=0.5)
    plt.plot(t, real_p, label='REAL', color='teal')
    plt.fill_between(t, 0, real_p, color='lime', alpha=0.5)
    plt.plot(t, app_p, label='APPARENT', color='salmon')
    plt.fill_between(t, 0, app_p, color='gold', alpha=0.2)


    plt.xlabel('$wt$')
    plt.ylabel('$i(t), u(t)$')
    plt.legend()
    plt.xticks([0, 0.5*np.pi, np.pi, 1.5*np.pi, 2*np.pi], ['$0$', '$0.5\pi$', '$\pi$', '$1.5\pi$', '$2\pi$'])
    plt.title('Parametric Plot of $i(t)$ and $u(t)$ vs. $wt$')
    #plt.yticks([])
    plt.grid(True)
    plt.savefig(f'{graph_name}.png')

if __name__ == "__main__":
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    RealReact(os.path.join(output_path,'Power_Resis.png'), 0)
    RealReact(os.path.join(output_path, 'Power_30.png'), 30)
    RealReact(os.path.join(output_path, 'Power_45.png'), 45)
    RealReact(os.path.join(output_path, 'Power_React.png'), 90)


