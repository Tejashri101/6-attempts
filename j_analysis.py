import b_Control_function
import c_Linear_regression_single_bus_Lasso
import d_Average_of_RGS_single_bus_lasso
import f_New_parameter_model_single_bus_lasso

#plotting
import g_Plot_mygraphs_single_bus
import h_Compare_mygraphs_single_bus
import i_Scale_mygraphs_single_bus

import os
import pandas as pd
import time
import numpy as np
import matplotlib.pyplot as plt

def mean_median_std(controller, col, select_bus):

    mean = controller[select_bus][col].mean()
    median = controller[select_bus][col].median()
    std = controller[select_bus][col].std()
    return mean, median, std

if __name__ == '__main__':
    data, num_cols, bus_name = b_Control_function.prepare_data()  # fetching data from control_function
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    print(bus_name)
    select_bus = bus_name[0]
    new_loss, new_controller, old_loss, old_controller = f_New_parameter_model_single_bus_lasso.New_parameter_model(data, num_cols, select_bus, choice_of_hyper='best')
    mean_n, median_n, std_n = mean_median_std(new_controller, 'Model Prediction', select_bus)
    mean_o, median_o, std_o = mean_median_std(new_controller, 'Original', select_bus)

    print(mean_n, median_n, std_n)
    print(mean_o, median_o, std_o)