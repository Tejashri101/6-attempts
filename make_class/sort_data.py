import os
import matplotlib.pyplot as plt
import pandas as pd

output_path = os.path.abspath(os.path.join('/home/tbhatt/Documents/1. MasterThesis/6. Attempts/Data/', "real_sorted_dataframe"))

def sort(real_data, cols = ['time', 'vt', 'qt', 'qt1']):

    new_df = pd.DataFrame(columns=cols)
    new_df['time'] = real_data.index
    n_rows = new_df['time'].count()

    #find number of buses
    collect_all = []
    for col in real_data.columns:
        collect_all.append(col.split('.')[0])
    bus_names = sorted(list(set(collect_all)))
    num_buses = (len(bus_names))
    col_v = []
    col_q = []
    col_qt1 = []
    for i in range(num_buses):
        col_v.append(bus_names[i] + '___vmpu')
    for i in range(num_buses):
        col_q.append(bus_names[i] + '___q_mvar')
    for i in range(num_buses):
        col_qt1.append(bus_names[i] + '___qt1_mvar')

    df_v = pd.DataFrame(columns=col_v)
    df_q = pd.DataFrame(columns=col_q)
    df_qt1 = pd.DataFrame(columns=col_qt1)

    i=0
    for col in df_v.columns:
        df_v[col] = real_data.filter(like= ((str(df_v.columns[i]).split('___'))[0] + '.1'), axis =1) #'.1' is for voltage
        i=i+1

    i=0
    for col in df_qt1.columns:
        df_qt1[col] = real_data.filter(like= ((str(df_qt1.columns[i]).split('___'))[0] + '.2'), axis =1) #'.2' is for qt1
        i=i+1

    i=0
    for col in df_q.columns:
        pattern = (str(df_q.columns[i]).split('___'))[0]
        matching_columns = real_data.columns[real_data.columns == pattern]
        df_q[col] = real_data[matching_columns[0]]
        i=i+1


    df_v = (df_v.drop(0)).astype(float)
    df_q = (df_q.drop(0)).astype(float)
    df_qt1 = (df_qt1.drop(0)).astype(float)

    """plt.figure(figsize=(8, 6))
    plt.plot(df_v.index, (df_v['0__bus__1___vmpu'].values), label='V', color='red')
    plt.plot(df_q.index, (df_q['0__bus__1___q_mvar'].values), label='q', color='blue')
    plt.plot(df_qt1.index, (df_qt1['0__bus__1___qt1_mvar'].values), label='qt1', color='green'                                                                                      '')
    plt.legend()
    plt.show()"""


    return df_v, df_q, df_qt1


if __name__ == '__main__':
    real_data = pd.read_csv(f'{output_path}.csv')
    cols = ['time', 'vt', 'qt', 'qt1']
    vtdata, qtdata, qt1data = sort(real_data, cols)
    #print(vtdata, qtdata, qt1data)


