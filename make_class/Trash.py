import matplotlib.pyplot as plt
import pandas as pd
import g_Plot_mygraphs
import os

import sys
sys.path.append('/home/tbhatt/classic-arl/src/behavior_cloning/util')

import sort_real_data


output_path = os.path.abspath(os.path.join('/home/tbhatt/classic-arl/src/behavior_cloning/util/Store_realistic_data'))

def prepare_data(n_samples=0):

    real_data = pd.read_csv(f'{output_path}.csv')
    vdata, qdata, qt1data = sort_real_data.real_data_sort(real_data)
    num_cols = len(vdata.columns)
    data = {}
    bus_name =[]

    for idx in range(num_cols):
        bus_name_temp = vdata.columns[idx].split("___")[0]
        vt_pass = vdata[vdata.columns[idx]].values
        qgent_pass = qdata[qdata.columns[idx]].values
        result = qt1data[qt1data.columns[idx]].values


        bus_name.append(bus_name_temp) #collecting the names

        data[bus_name_temp] = {
            "q generated": qgent_pass,
            "V": vt_pass,
            "q generated t+1": result,
        }

    df = {}
    for col1 in data:                   #unpacking the data fetched
        for col2 in data[col1]:
            df[(col1, col2)] = data[col1][col2]
    df = pd.DataFrame(df)

    if n_samples > 0:
        df = df[:n_samples]

    return df, num_cols, bus_name


def main():
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    data, num_cols, bus_name = prepare_data()
    g_Plot_mygraphs.plot_mygraph(os.path.join(output_path,'Qt_Qt1_V'), data, 'Time', 'q generated', 'q generated t+1', 'V')

    time_series = range(0, len(data))
    fig, axs = plt.subplots(4, 3, figsize=(15, 15))

    for bus_num in range(1, 15):
        if bus_num == 2:
            hi = 1
        else:
            bus_name = f'0__bus__{bus_num}'

            axs[0, 0].plot(time_series, data[bus_name]['V'], label=f'{bus_name} voltage')
            axs[0, 1].plot(time_series, data[bus_name]['q generated'], label=f'{bus_name} q generated')
            axs[0, 2].plot(time_series, data[bus_name]['q generated t+1'], label=f'{bus_name} q generated t+1')

    # Add legends
    #axs[0, 0].legend()
    #axs[0, 1].legend()
    #axs[0, 2].legend()

    # Set titles for subplots
    axs[0,0].set_title('V')
    axs[0,1].set_title('q generated')
    axs[0,2].set_title('q generated t+1')

    plt.subplots_adjust(hspace=2)  # You can adjust the value as needed

    # Display mean and std deviation in additional plots
    mean_data_v = []
    std_data_v = []
    mean_data_q = []
    std_data_q = []
    mean_data_qt1 = []
    std_data_qt1 = []
    for bus_num in range(1, 15):
        if bus_num == 2:
            hi = 1
        else:
            bus_name = f'0__bus__{bus_num}'

            mean_v = data[bus_name]['V'].mean()
            std_v = data[bus_name]['V'].std()

            mean_q = data[bus_name]['q generated'].mean()
            std_q = data[bus_name]['q generated'].std()

            mean_qt1 = data[bus_name]['q generated t+1'].mean()
            std_qt1 = data[bus_name]['q generated t+1'].std()

            mean_data_v.append(mean_v)
            std_data_v.append(std_v)

            mean_data_q.append(mean_q)
            std_data_q.append(std_q)

            mean_data_qt1.append(mean_qt1)
            std_data_qt1.append(std_qt1)

    # Plot mean values
    axs[1, 0].plot(range(1, 14), mean_data_v, color='blue')
    axs[1, 0].set_title('Mean V')
    axs[1, 0].set_xlabel('Bus Number')
    axs[1, 0].set_ylabel('Mean Value')

    # Plot std deviation values
    axs[1, 1].plot(range(1, 14), std_data_v, color='orange')
    axs[1, 1].set_title('Std Dev V')
    axs[1, 1].set_xlabel('Bus Number')
    axs[1, 1].set_ylabel('Std Dev Value')

    # Plot mean values
    axs[2, 0].plot(range(1, 14), mean_data_q, color='blue')
    axs[2, 0].set_title('Mean Q')
    axs[2, 0].set_xlabel('Bus Number')
    axs[2, 0].set_ylabel('Mean Value')

    # Plot std deviation values
    axs[2, 1].plot(range(1, 14), std_data_q, color='orange')
    axs[2, 1].set_title('Std Dev Q')
    axs[2, 1].set_xlabel('Bus Number')
    axs[2, 1].set_ylabel('Std Dev Value')

    # Plot mean values
    axs[3, 0].plot(range(1, 14), mean_data_qt1, color='blue')
    axs[3, 0].set_title('Mean Q@t+1')
    axs[3, 0].set_xlabel('Bus Number')
    axs[3, 0].set_ylabel('Mean Value')

    # Plot std deviation values
    axs[3, 1].plot(range(1, 14), std_data_qt1, color='orange')
    axs[3, 1].set_title('Std Dev Q@t+1')
    axs[3, 1].set_xlabel('Bus Number')
    axs[3, 1].set_ylabel('Std Dev Value')

    # Remove unnecessary subplots
    #for i in range(3):
    axs[1, 2].axis('off')
    axs[2, 2].axis('off')
    axs[3, 2].axis('off')

    # Save the combined plot
    plt.tight_layout()
    plt.savefig(os.path.join(output_path, 'v_q_qt1_real.svg'), format='svg', dpi=300)
    plt.show()



    #print(data['0__bus__11']['V'])
    #print('V', data['0__bus__11']['V'].mean())
    #print('q generated', data['0__bus__11']['q generated'].mean())
    #print('q generated t+1', data['0__bus__11']['q generated t+1'].mean())

# Main program execution
if __name__ == "__main__":

    main()
