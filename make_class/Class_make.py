import sys
sys.path.append('/home/tbhatt/classic-arl/src/behavior_cloning/util')
import sort_real_data
import sort_data
from sklearn.metrics import mean_squared_error, r2_score
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.losses import MeanSquaredError
from sklearn.model_selection import train_test_split  # For creating train/test splits
from sklearn.base import BaseEstimator, RegressorMixin
from itertools import chain
import os
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RandomizedSearchCV
import time
from scipy.stats import randint as sp_randint, uniform
from scipy.integrate import trapz
from keras.callbacks import History
history = History()

# Set the random seed for NumPy and TensorFlow
random_seed = 42
np.random.seed(random_seed)
tf.random.set_seed(random_seed)
epochs_num = 100
choice = 'real' #choose real or simulated
remove_defective = 'yes' #yes to remove rows with V<0.5, no otherwise
data_points = 0 # 0 means entire dataset
selected_num_cols = 1  # None means all of the buses
start_bus = 1
end_bus = selected_num_cols
fresh_data = 'no' #'yes' to get new data in and 'no' to use the old csv file for the real_data

class Itachi:

    # Constructor method (called when an object is created)
    def __init__(self, epochs_num, file_name):
        # Instance attributes (specific to each instance)
        self.epochs_num = epochs_num
        self.data = None
        self.num_cols = None
        self.bus_name = None
        self.file_name = file_name

    # Instance method
    def prepare_new_data(self, n_samples=0, fresh_data='no'):

        if fresh_data == 'yes': #Use this when new data is created for the real data (stored in 'Data' folder)
            current_directory = os.path.dirname(os.path.abspath(__file__))
            output_path = os.path.join(current_directory, 'Data', f'{self.file_name}')
            output_path = os.path.abspath(output_path)
            real_data = pd.read_csv(f'{output_path}.csv')
            vdata, qdata, qt1data = sort_real_data.real_data_sort(real_data)
            num_cols = len(vdata.columns)
            data = {}
            bus_name = []

            for idx in range(num_cols):
                bus_name_temp = vdata.columns[idx].split("___")[0]
                vt_pass = vdata[vdata.columns[idx]].values
                qgent_pass = qdata[qdata.columns[idx]].values
                result = qt1data[qt1data.columns[idx]].values

                bus_name.append(bus_name_temp)  # collecting the names

                data[bus_name_temp] = {
                    "q generated": qgent_pass,
                    "V": vt_pass,
                    "q generated t+1": result,
                }

            df = {}
            for col1 in data:  # unpacking the data fetched
                for col2 in data[col1]:
                    df[(col1, col2)] = data[col1][col2]
            df = pd.DataFrame(df)

            #df.to_csv(os.path.join('/home/tbhatt/Documents/1. MasterThesis/6. Attempts/Data/', 'real_sorted_dataframe.csv'), index=False)
            df.to_csv(os.path.join(current_directory, 'Data','real_sorted_dataframe.csv'))
            #df.to_csv(os.path.join(__file__, '..', '/Data/', 'real_sorted_dataframe.csv'), index=False)
            df = df.drop(0)

            #defective data points
            """defective_V = {}
            count = 0
            for k in bus_name:
                defective_indices = []
                for i in range(df.shape[0]):
                    if df[k]['V'][i] < 0.5:
                        defective_indices.append(i)
                        count += 1
                defective_V[k] = defective_indices  

            print(defective_V)
            print(count)       
            defective_indices = [index for indices in defective_V.values() for index in indices] # Flatten the list of indices"""
            defective_indices = [21727, 21822, 21918, 28846]
            # Drop the rows with defective values
            df_cleaned = df.drop(defective_indices)

            if n_samples > 0:
                df_cleaned = df_cleaned[:n_samples]

            self.data = df_cleaned
            self.num_cols = num_cols
            self.bus_name = bus_name

        elif fresh_data == 'no':
            current_directory = os.path.dirname(os.path.abspath(__file__))
            file_path = os.path.join(current_directory, 'Data', 'real_sorted_dataframe.csv')
            file_path = os.path.abspath(file_path)
            df = pd.read_csv(file_path)

            vdata, qdata, qt1data = sort_data.sort(df)
            num_cols = len(vdata.columns)
            data = {}
            bus_name = []

            """plt.figure(figsize=(8, 6))
            plt.plot(vdata.index, (vdata['0__bus__1___vmpu'].values), label='V', color='red')
            plt.plot(vdata.index, (qdata['0__bus__1___q_mvar'].values), label='q', color='blue')
            plt.plot(vdata.index, (qt1data['0__bus__1___qt1_mvar'].values), label='qt1', color='green'                                                                                    '')
            plt.legend()
            plt.show()"""

            for idx in range(num_cols):
                bus_name_temp = vdata.columns[idx].split("___")[0]
                vt_pass = vdata[vdata.columns[idx]].values
                qgent_pass = qdata[qdata.columns[idx]].values
                result = qt1data[qt1data.columns[idx]].values

                bus_name.append(bus_name_temp)  # collecting the names

                data[bus_name_temp] = {
                    "q generated": qgent_pass,
                    "V": vt_pass,
                    "q generated t+1": result,
                }

            df = {}
            for col1 in data:  # unpacking the data fetched
                for col2 in data[col1]:
                    df[(col1, col2)] = data[col1][col2]
            df = pd.DataFrame(df)
            df = df.drop(0)

            """defective_V = {}
            count = 0
            for k in bus_name:
                defective_indices = []
                for i in range(df.shape[0]):
                    if df[k]['V'][i] < 0.5:
                        defective_indices.append(i)
                        count += 1
                defective_V[k] = defective_indices  
    
            print(defective_V)
            print(count)       
            defective_indices = list(set([index for indices in defective_V.values() for index in indices])) # Flatten the list of indices
            print(defective_indices)"""

            defective_indices = [21727, 21822, 21918, 28846]
            # Drop the rows with defective values
            df_cleaned = df.drop(defective_indices)

            if n_samples > 0:
                df_cleaned = df_cleaned[:n_samples]

            self.data = df_cleaned
            self.num_cols = num_cols
            self.bus_name = bus_name

            """plt.figure(figsize=(8, 6))
            plt.plot(df_cleaned.index, df_cleaned['0__bus__1']['V'], label='V', color = 'red')
            plt.plot(df_cleaned.index, df_cleaned['0__bus__1']['q generated'], label='q generated', color = 'blue')
            plt.plot(df_cleaned.index, df_cleaned['0__bus__1']['q generated t+1'], label='q generated t+1', color = 'green')
            plt.legend()
            plt.show()"""

            return df_cleaned, num_cols, bus_name


    def prepare_old_data(self, n_samples=0):
        data = pd.read_csv(f'/home/tbhatt/Documents/1. MasterThesis/6. Attempts/Data/{self.file_name}.csv')
        data.columns = pd.MultiIndex.from_tuples(
            [('0__bus__1', 'q generated'), ('0__bus__1', 'V'), ('0__bus__1', 'q generated t+1'),
             ('0__bus__10', 'q generated'), ('0__bus__10', 'V'), ('0__bus__10', 'q generated t+1'),
             ('0__bus__11', 'q generated'), ('0__bus__11', 'V'), ('0__bus__11', 'q generated t+1'),
             ('0__bus__12', 'q generated'), ('0__bus__12', 'V'), ('0__bus__12', 'q generated t+1'),
             ('0__bus__13', 'q generated'), ('0__bus__13', 'V'), ('0__bus__13', 'q generated t+1'),
             ('0__bus__14', 'q generated'), ('0__bus__14', 'V'), ('0__bus__14', 'q generated t+1'),
             ('0__bus__2', 'q generated'), ('0__bus__2', 'V'), ('0__bus__2', 'q generated t+1'),
             ('0__bus__3', 'q generated'), ('0__bus__3', 'V'), ('0__bus__3', 'q generated t+1'),
             ('0__bus__4', 'q generated'), ('0__bus__4', 'V'), ('0__bus__4', 'q generated t+1'),
             ('0__bus__5', 'q generated'), ('0__bus__5', 'V'), ('0__bus__5', 'q generated t+1'),
             ('0__bus__6', 'q generated'), ('0__bus__6', 'V'), ('0__bus__6', 'q generated t+1'),
             ('0__bus__7', 'q generated'), ('0__bus__7', 'V'), ('0__bus__7', 'q generated t+1'),
             ('0__bus__8', 'q generated'), ('0__bus__8', 'V'), ('0__bus__8', 'q generated t+1'),
             ('0__bus__9', 'q generated'), ('0__bus__9', 'V'), ('0__bus__9', 'q generated t+1')], names=['', ''])
        data = data.drop(0)
        data = data.apply(pd.to_numeric, errors='coerce')

        num_cols = int(data.shape[1] / 3)
        bus_name = ['0__bus__1', '0__bus__10', '0__bus__11', '0__bus__12', '0__bus__13', '0__bus__14', '0__bus__2',
                    '0__bus__3', '0__bus__4', '0__bus__5', '0__bus__6', '0__bus__7', '0__bus__8', '0__bus__9']

        if n_samples > 0:
            data = data[:n_samples]

        return data, num_cols, bus_name

    @staticmethod
    def reshape_for_regression(x1_train, x1_test, x2_train, x2_test):

        reshaped_xtrain = np.stack([x1_train, x2_train], 1)  # Reshape to (None, 1)
        reshaped_xtest = np.stack([x1_test, x2_test], 1)  # Reshape to (None, 1)

        return reshaped_xtrain, reshaped_xtest

    @staticmethod
    def split_data(x1, x2, y, test_ratio=0.3):

        x1_train, x1_test = train_test_split(x1, test_size=test_ratio, random_state=42)
        x2_train, x2_test = train_test_split(x2, test_size=test_ratio, random_state=42)
        y_train, y_test = train_test_split(y, test_size=test_ratio, random_state=42)
        # data is split between test and train only as the cv spli will be donr automatically within the randomizedgridsearch function.

        # Z score normalization x = (x-x_mean)/x_std
        scaler_x1train = StandardScaler()
        normalized_x1train = scaler_x1train.fit_transform(x1_train.reshape(-1, 1))

        scaler_x2train = StandardScaler()
        normalized_x2train = scaler_x2train.fit_transform(x2_train.reshape(-1, 1))

        scaler_x1test = StandardScaler()
        normalized_x1test = scaler_x1test.fit_transform(x1_test.reshape(-1, 1))

        scaler_x2test = StandardScaler()
        normalized_x2test = scaler_x2test.fit_transform(x2_test.reshape(-1, 1))

        scaler_ytrain = StandardScaler()
        normalized_ytrain = scaler_ytrain.fit_transform(y_train.reshape(-1, 1))

        scaler_ytest = StandardScaler()
        normalized_ytest = scaler_ytest.fit_transform(y_test.reshape(-1, 1))

        # Reshaping
        reshaped_xtrain, reshaped_xtest = Itachi.reshape_for_regression(
            normalized_x1train, normalized_x1test,
            normalized_x2train, normalized_x2test
        )
        xtrain_reshape = reshaped_xtrain.reshape(reshaped_xtrain.shape[0], -1)
        xtest_reshape = reshaped_xtest.reshape(reshaped_xtest.shape[0], -1)
        ytrain_reshape = normalized_ytrain.flatten()
        ytest_reshape = normalized_ytest.flatten()

        return xtrain_reshape, xtest_reshape, ytrain_reshape, ytest_reshape

    @staticmethod
    def create_model(select_bus, activation, learning_rate, num_layers, num_neurons, equally_split_neurons, batch_size):
        output_path = os.path.abspath(os.path.join(__file__, "..", "Saved_models"))
        L1 = 0.0458 #0.0458
        L2 =  0.0458  #0.0458
        bias_L1 = 0.055
        model = Sequential()
        model.add(Dense(units=num_neurons,
                        input_dim=2,
                        activation=activation,
                        name='layer_1',
                        kernel_regularizer=tf.keras.regularizers.L1(L1),
                        activity_regularizer=tf.keras.regularizers.L2(L2),
                        bias_regularizer=tf.keras.regularizers.L1(bias_L1), #0.055
                        use_bias = True,
                        ),
                  )

        if equally_split_neurons:
            neurons_per_layer = [num_neurons] * num_layers #number of layers and neurons are only used if equally split is True
        else:
            # Generate a random number of layers
            min_num_layers = 1
            max_num_layers = 10
            random_num_layers = np.random.randint(min_num_layers, max_num_layers + 1)

            # Generate random neurons for each layer
            min_random_neurons = 1
            max_random_neurons = 10
            neurons_per_layer = np.random.randint(min_random_neurons, max_random_neurons + 1, size=random_num_layers)

        for neurons in neurons_per_layer:
            model.add(Dense(units=neurons,
                            activation=activation,
                            kernel_regularizer=tf.keras.regularizers.L1(L1),
                            activity_regularizer=tf.keras.regularizers.L2(L2),
                            bias_regularizer=tf.keras.regularizers.L1(bias_L1), #0.055
                            use_bias = True,
                            ),
                      )

        model.add(Dense(units=1,
                        activation='linear', #since output layer
                        name='layer_output',
                        kernel_regularizer=tf.keras.regularizers.L1(L1),
                        activity_regularizer=tf.keras.regularizers.L2(L2),
                        bias_regularizer=tf.keras.regularizers.L1(bias_L1), #0.055
                        use_bias = True
                        ),
                  )

        STEPS_PER_EPOCH = int(1e3) // batch_size  #batch_size not used to build the model though

        learning_rate_mod=tf.keras.optimizers.schedules.InverseTimeDecay(learning_rate, decay_steps=STEPS_PER_EPOCH*1000, decay_rate=1, staircase=False)

        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate_mod),
            loss=[MeanSquaredError()]
        )
        if choice == 'real':
            model_name = 'itachi_model_' + select_bus
        elif choice == 'simulated':
            model_name = 'simulated_model_' + select_bus
        model.save(f'{output_path}/{model_name}')

        model.summary()

        return model

    @staticmethod
    def Linear_regression_single_bus(data, select_bus, activation='relu', learning_rate=1e-3, num_layers=3, num_neurons=10, batch_size=10, equally_split_neurons=True):
    # def Linear_regression_single_bus(data, select_bus, activation='relu', learning_rate=1e-3, num_layers=3, num_neurons=10, batch_size=10, equally_split_neurons=True): #0__bus_1
    # def Linear_regression_single_bus(data, select_bus, activation='linear', learning_rate=0.019455, num_layers=3.5, num_neurons=36, batch_size=19.5, equally_split_neurons=True): #0__bus_3
        #selecting 0.01 instead of 0.001 because the learning curve of 0.01 for 20 datapoints on the first bus was better
        interim_data_loss= {}
        interim_data_controller= {}

        # Fetching data for the first bus
        x1 = np.array(pd.DataFrame(data)[select_bus]["V"])
        x2 = np.array(pd.DataFrame(data)[select_bus]["q generated"])
        y = np.array(pd.DataFrame(data)[select_bus]["q generated t+1"])

        # Splitting
        reshaped_xtrain, reshaped_xtest, y_train, y_test = Itachi.split_data(x1, x2, y)
    #---------------------------------------------------------------------------------------
        # Training set
        model_train = KerasRegressorWithHistory(build_fn=lambda: Itachi.create_model(select_bus, activation, learning_rate, int(num_layers), int(num_neurons), equally_split_neurons, batch_size))
        model_train.fit(reshaped_xtrain, y_train, int(batch_size))

        xtrain_loss_history = model_train.loss_history #training loss
        xval_loss_history = model_train.val_loss_history #validation loss

        req_len=len(xtrain_loss_history)
    # ---------------------------------------------------------------------------------------
        interim_data_loss[(select_bus, 'Training set')]=xtrain_loss_history
        interim_data_loss[(select_bus, 'Extra1')] = [0]*req_len
        interim_data_loss[(select_bus, 'Extra2')] = [0]*req_len

        send_data_loss = pd.DataFrame(interim_data_loss)

        # ------------------------------------------------------------------
        original_controller = y_test
        model_predicted_outputs = list(chain.from_iterable((model_train.predict(reshaped_xtest)).tolist())) # Use the trained model to predict
        model_predicted_train_outputs = list(chain.from_iterable((model_train.predict(reshaped_xtrain)).tolist()))

        # Adjust the sizes to make a dataframe at the end

        len_ori = len(original_controller)
        len_mod = len(model_predicted_outputs)

        max_len = max(len_ori, len_mod)
        min_len = min(len_ori, len_mod)

        req_len = max_len - min_len
        add_to_predict = [None]*req_len

        model_predicted_outputs.extend(add_to_predict)

        interim_data_controller[(select_bus, 'Original')] = original_controller
        interim_data_controller[(select_bus, 'Model Prediction')] = model_predicted_outputs
        interim_data_controller[(select_bus, 'Extra2')] = [0]*max_len

        send_data_controller = pd.DataFrame(interim_data_controller)

        # ------------------------------------------------------------------
        mse_test = mean_squared_error(y_test, model_predicted_outputs)
        mse_train = mean_squared_error(y_train, model_predicted_train_outputs)
        std_dev_ytest = np.std(y_test)
        std_dev_ytrain = np.std(y_train)
        std_dev_pred = np.std(model_predicted_outputs)
        r2_test = r2_score(y_test, model_predicted_outputs)
        r2_train = r2_score(y_train, model_predicted_train_outputs)
        mean_ytest = y_test.mean()
        mean_model_pred = ((np.array(model_predicted_outputs)).mean())
        mean_ytrain = y_train.mean()
        # ------------------------------------------------------------------

        #print(mse, mse_train)

        return send_data_loss, send_data_controller, mse_test, mse_train, xval_loss_history, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred

    @staticmethod
    def Average_of_RGS(data, select_bus, param_dist,
                       cols=['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons'],
                       vals={'Best MSE': 0, 'activation': 0, 'batch_size': 0, 'epochs': 0, 'learning_rate': 0, 'num_layers': 0, 'num_neurons': 0}):

        collection = []
        num_of_itr = 2  # number of iterations reduced because the performance went down with increase in iterations

        for i in range(num_of_itr):
            collection.append(Itachi.Randomized_grid_search_RGS(data, select_bus, param_dist))

        df = pd.DataFrame(columns=cols)

        rows = 1  # number of buses
        colm = len(cols)  # number of parameters

        for i in range(rows):
            df = df._append(vals, ignore_index=True)
            for j in range(colm):
                if j == 1:
                    check1 = 0
                    check2 = 0
                    for k in range(num_of_itr):
                        if collection[k].iloc[i][j] == 'relu':
                            check1 = check1 + 1
                        elif collection[k].iloc[i][j] == 'linear':
                            check2 = check2 + 1

                    if check1 > check2:
                        df.iloc[i][j] = 'relu'
                    else:
                        df.iloc[i][j] = 'linear'
                else:
                    sum = 0
                    for k in range(num_of_itr):
                        sum = sum + collection[k].iloc[i][j]
                    df.iloc[i][j] = sum / num_of_itr

        print('Average of RGS')
        print(df)
        # ---------------------------------------------------------------------------------
        return df

    @staticmethod
    def Randomized_grid_search_RGS(data, select_bus, param_dist):

        # Fetching data for the first bus
        x1 = np.array(pd.DataFrame(data)[select_bus]["V"])
        x2 = np.array(pd.DataFrame(data)[select_bus]["q generated"])
        y = np.array(pd.DataFrame(data)[select_bus]["q generated t+1"])

        # Splitting data into training, test and cross validation
        reshaped_xtrain, reshaped_xtest, y_train, y_test = Itachi.split_data(x1, x2, y)

        # Perform randomized grid search
        n_iter_search = 2  # change?---------------------------------------------------------------------------------
        random_search = RandomizedSearchCV(ran_KerasRegressorWithHistory(), param_distributions=param_dist,
                                           n_iter=n_iter_search, n_jobs=-1, verbose=0, cv=3, pre_dispatch='2*n_jobs')

        # Fit the randomized search to the data
        random_search.fit(reshaped_xtrain, y_train)
        new_model = random_search.best_estimator_  # best model
        v = list(random_search.best_params_.values())  # best parameters
        y_est = list(chain.from_iterable(new_model.predict(reshaped_xtrain)))  # predict values
        print(v)

        result_data = {'Best MSE': random_search.best_score_,  # best parameters found out
                       'activation': v[0],
                       'batch_size': v[1],
                       'epochs': v[2],
                       'learning_rate': v[3],
                       'num_layers': v[4],
                       'num_neurons': v[5],
                       'model': y_est}
        result_cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers',
                       'num_neurons', 'model']

        df = pd.DataFrame(columns=result_cols)

        df = df._append(result_data, ignore_index=True)

        return df

    @staticmethod
    def ran_create_model(learning_rate=1e-3, num_layers=2, activation='relu', num_neurons=15):
        L1 = 0.0458  # 0.0458
        L2 = 0.0458 # 0.0458
        bias_L1 = 0.055
        model = Sequential()
        if activation == 'linear':
            model.add(Dense(units=1,
                            input_dim=2,
                            activation=activation,
                            name='layer_1',
                            kernel_regularizer=tf.keras.regularizers.L1(L1),
                            activity_regularizer=tf.keras.regularizers.L2(L2),
                            bias_regularizer=tf.keras.regularizers.L1(bias_L1),  # 0.055
                            use_bias=True
                            )
                      )
        else:
            model.add(tf.keras.layers.Dense(units=int(num_neurons),
                                            input_dim=2,
                                            activation=activation,
                                            name='layer_1',
                                            kernel_regularizer=tf.keras.regularizers.L1(L1),
                                            activity_regularizer=tf.keras.regularizers.L2(L2),
                                            bias_regularizer=tf.keras.regularizers.L1(bias_L1),  # 0.055
                                            use_bias=True
                                            )
                      )
        for _ in range(int(num_layers)):
            model.add(tf.keras.layers.Dense(units=int(num_neurons),
                                            activation=activation,
                                            kernel_regularizer=tf.keras.regularizers.L1(L1),
                                            activity_regularizer=tf.keras.regularizers.L2(L2),
                                            bias_regularizer=tf.keras.regularizers.L1(bias_L1),  # 0.055
                                            use_bias=True
                                            )
                      )
        model.add(Dense(units=1, activation='linear', name='layer_output'))
        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate), loss=MeanSquaredError())
        return model

    @staticmethod
    def new_model_creation(data, num_cols, select_bus, param_dist, cols, vals):


        df = Itachi.Average_of_RGS(data, select_bus, param_dist, cols, vals)

        activation = df.iloc[0][1]
        batch_size = int(df.iloc[0][2])  # 3 was epochs
        learning_rate = df.iloc[0][4]
        num_layers = int(df.iloc[0][5])
        num_neurons = int(df.iloc[0][6])

        new_loss, new_controller, mse_test, mse_train, val_mse_values, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred = Itachi.Linear_regression_single_bus(
            data,
            select_bus,
            activation=activation,
            learning_rate=learning_rate,
            num_layers=num_layers,
            num_neurons=num_neurons,
            batch_size=batch_size,
            equally_split_neurons=True
        )

        return new_loss, new_controller, mse_test, mse_train, val_mse_values, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred

    @staticmethod
    def old_model_extract(data, select_bus):

        old_loss, old_controller, mse_test, mse_train, val_mse_values, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred = Itachi.Linear_regression_single_bus(
            data, select_bus)

        return old_loss, old_controller, r2_test, r2_train

    @staticmethod
    def normalize_mydata(data_as_dataframe):

        scaler = StandardScaler()
        normalized_data = scaler.fit_transform(data_as_dataframe)
        normalized_df = pd.DataFrame(normalized_data, columns=data_as_dataframe.columns)
        return normalized_df

    @staticmethod
    def New_parameter_model(data, num_cols, select_bus):

        param_dist = {
            'epochs': sp_randint(10, 500),
            'batch_size': sp_randint(10, 30),
            'learning_rate': uniform(1e-5, 1e-1),
            'num_layers': sp_randint(1, 5),
            'num_neurons': sp_randint(1, 10),
            'activation': ['relu', 'linear']
        }
        cols = ['Best MSE', 'activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons']
        vals = {'Best MSE': 0,
                'activation': 0,
                'batch_size': 0,
                'epochs': 0,
                'learning_rate': 0,
                'num_layers': 0,
                'num_neurons': 0}

        new_loss, new_controller, mse_test, mse_train, val_mse_values, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred = Itachi.new_model_creation(
            data, num_cols, select_bus, param_dist, cols, vals)
        old_loss, old_controller, r2_oltest, r2_oltrain = Itachi.old_model_extract(data, select_bus)

        # graph_name = 'new_loss_' + select_bus
        # g_Plot_mygraphs_single_bus.plot_mygraph(os.path.join(output_path,graph_name), new_loss, 'Epoch', 'Training set', 'Extra1', 'Extra2')
        # graph_name = 'new_controller_' + select_bus
        # g_Plot_mygraphs_single_bus.plot_mygraph(os.path.join(output_path,graph_name), new_controller, 'Time', 'Original', 'Model Prediction', 'Extra2')
        #graph_name = 'SampleEfficiency_' + select_bus
        #Itachi.plot_mygraph(os.path.join(output_path, graph_name), new_controller, labelx1='Time', labely1='Model Prediction', mse=None)
        # graph_name = 'compare_' + select_bus
        # h_Compare_mygraphs_single_bus.compare_mygraph(os.path.join(output_path,graph_name), new_controller, old_controller, 'Time', 'Model Prediction','New model', 'Model Prediction', 'Old model', 'Original', 'Original')
        # graph_name = 'normalize_' + select_bus
        # i_Scale_mygraphs_single_bus.feature_scaling_mymodel(os.path.join(output_path,graph_name), new_controller, old_controller)

        return new_loss, new_controller, old_loss, old_controller,r2_oltest, r2_oltrain, mse_test, mse_train, val_mse_values, r2_test, r2_train, mean_ytest, mean_model_pred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_pred

    @staticmethod
    def plot_r2_mse(graph_name, x, y_r2_test, y_r2_train, y_mse_test, y_mse_train, y_ytest_mean, y_ypred_mean, y_ytrain_mean, std_dev_ytest, std_dev_ytrain, std_dev_model):
        plt.figure(figsize=(8, 6))
        plt.plot(x, y_r2_test, label='R-squared', color='blue', marker='o')
        plt.plot(x, y_r2_train, label='R-squared', color='magenta', marker='o')
        plt.plot(x, y_mse_test, label='MSE test', color='green', marker='o')
        plt.plot(x, y_mse_train, label='MSE train', color='cyan', marker='o')
        plt.plot(x, y_ytest_mean, label='Mean y_test', color='orange', marker='o')
        plt.plot(x, y_ypred_mean, label='Mean y_pred', color='red', marker='o')
        plt.plot(x, y_ytrain_mean, label='Mean y_train', color='lightsalmon', marker='o')
        plt.plot(x, std_dev_ytest, label='Std y_test', color='grey', marker='o')
        plt.plot(x, std_dev_ytrain, label='Std y_test', color='yellow', marker='o')
        plt.plot(x, std_dev_model, label='STd y_pred', color='black', marker='o')

        plt.xlabel('Data Points')
        plt.ylabel('Values')
        plt.title('R-squared and MSE Values')
        plt.legend()
        plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)

    @staticmethod
    def plot_graph(graph_name, data, labelx1, labely1, labely2=None, labely3=None, mse=None):

        num_cols = int(len(data.columns) / 3)
        fig, axes = plt.subplots()

        for idx in range(num_cols):

            bus_name = data.columns[3 * idx][0].split("___")[0]  # 3*idx done to select the correct value of bus_name due to data's structure

            # Extract values for plotting
            time_series = data.index
            values1 = data[bus_name][labely1]
            values2 = data[bus_name][labely2]
            values3 = data[bus_name][labely3]

            # Create a line plot
            axes.plot(time_series, values1, label=labely1)
            if mse!= None:
                axes.plot(time_series, mse, label='Validation set')
            if (labely2 != 'Extra1'):
                axes.plot(time_series, values2, label=labely2, color='green')

            if (labely3 != 'Extra2'):
                axes.plot(time_series, values3, label=labely3)

            # Add labels and title
            if graph_name == 'Loss function':
                axes.set_ylim(-.001, .001)

            axes.set_xlabel(labelx1)
            axes.set_ylabel('Y_axis')
            axes.set_title(bus_name)
            axes.legend(loc='upper right')

            # Save the plot as an image file
            plt.tight_layout()

        plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
        plt.close()

    @staticmethod
    def compare_mygraph(graph_name, data1, data2, labelx1, labely1, labely11, labely2, labely22, labely3='Extra2', labely33='Extra2'):

        num_cols = int(len(data1.columns) / 3)
        fig, axes = plt.subplots()
        area_comparison_df = pd.DataFrame(columns=['Bus Name', 'Org-Old(Yellow)', 'Org-New(Red)'])

        for idx in range(num_cols):
            calc = 0
            bus_name = data1.columns[3 * idx][0].split("___")[0]  # 3*idx done to select the correct value of bus_name due to data#s structure

            # Extract values for plotting
            time_series = data1.index
            values1 = data1[bus_name][labely1]
            values2 = data2[bus_name][labely2]
            if labely3 != 'Extra2':
                values3 = data2[bus_name][labely3]

            # Create a line plot
            axes.plot(time_series, values1, label=labely11, color='Red')
            axes.plot(time_series, values2, label=labely22, color='grey')
            if labely3 != 'Extra2':
                axes.plot(time_series, values3, label=labely33, color='black')
                axes.fill_between(time_series, values1, values3, where=(values1 >= values3), interpolate=True, color='red', alpha=0.3, label='New')
                axes.fill_between(time_series, values1, values3, where=(values1 < values3), interpolate=True,  color='red', alpha=0.3)
                axes.fill_between(time_series, values2, values3, where=(values2 >= values3), interpolate=True,  color='yellow', alpha=0.3, label='Old')
                axes.fill_between(time_series, values2, values3, where=(values2 < values3), interpolate=True,  color='yellow', alpha=0.3)
                area_red = trapz(np.abs(values3 - values1), x=time_series)
                area_yellow = trapz(np.abs(values3 - values2), x=time_series)
                data_add = pd.DataFrame({'Bus Name': [bus_name], 'Org-Old(Yellow)': [area_yellow], 'Org-New(Red)': [area_red]})
                area_comparison_df = area_comparison_df._append(data_add, ignore_index=True)

            # Add labels and title
            axes.set_xlabel(labelx1)
            axes.set_ylabel('Y_axis')
            axes.set_title(bus_name)
            axes.legend(loc='upper right')

            # Save the plot as an image file
            plt.tight_layout()

        if labely3 != 'Extra2':
            count = 0
            area_comparison_df['Difference'] = area_comparison_df['Org-Old(Yellow)'] - area_comparison_df[
                'Org-New(Red)']
            area_comparison_df['Difference %'] = (100 * area_comparison_df['Difference']) / area_comparison_df[
                'Org-Old(Yellow)']
            area_comparison_df['Check'] = np.nan
            for idx in range(num_cols):
                if area_comparison_df['Org-Old(Yellow)'][idx] > area_comparison_df['Org-New(Red)'][idx]:
                    area_comparison_df['Check'][idx] = 1
                else:
                    area_comparison_df['Check'][idx] = 0
                    count += 1
            print("Comparison of Areas:")
            print(area_comparison_df)
            print(f"{num_cols - count} / {num_cols} buses have performed better than before.")
            # count buses which perform better
            if (num_cols - count) == 1:
                calc = 1

        # plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
        plt.close()
        return calc, area_comparison_df

    @staticmethod
    def feature_scaling(graph_name, norm_new_crtl, norm_old_crtl, count, time_taken=0):
        # FEATURE SCALING!!
        num_cols = int(len(norm_new_crtl.columns) / 3)
        fig, axes = plt.subplots(figsize=(20, 10))

        for idx in range(num_cols):
            bus_name = norm_new_crtl.columns[3 * idx][0].split("___")[0]  # 3*idx done to select the correct value of bus_name due to data#s structure
            # Extract values for plotting

            time_series = norm_new_crtl.index

            values1 = (norm_new_crtl[bus_name]['Model Prediction']) / (norm_old_crtl[bus_name]['Original'])
            values2 = (norm_old_crtl[bus_name]['Model Prediction']) / (norm_old_crtl[bus_name]['Original'])
            values3 = (norm_old_crtl[bus_name]['Original']) / (norm_old_crtl[bus_name]['Original'])

            # Create a line plot

            axes.plot(time_series, values2, label='Old Model', color='grey')
            axes.plot(time_series, values3, label='Original', color='black')
            if count == 1:
                axes.plot(time_series, values1, label='New Model', color='green')
                axes.fill_between(time_series, values1, values3, where=(values1 >= values3), interpolate=True,
                                  color='green', alpha=0.3, label='New')
                axes.fill_between(time_series, values1, values3, where=(values1 < values3), interpolate=True,
                                  color='green', alpha=0.3)
            else:
                axes.plot(time_series, values1, label='New Model', color='red')
                axes.fill_between(time_series, values1, values3, where=(values1 >= values3), interpolate=True,
                                  color='red', alpha=0.3, label='New')
                axes.fill_between(time_series, values1, values3, where=(values1 < values3), interpolate=True,
                                  color='red', alpha=0.3)
            axes.fill_between(time_series, values2, values3, where=(values2 >= values3), interpolate=True,
                              color='yellow', alpha=0.3, label='Old')
            axes.fill_between(time_series, values2, values3, where=(values2 < values3), interpolate=True,
                              color='yellow', alpha=0.3)

            # Add labels and title
            axes.set_xlabel('Time')
            axes.set_ylabel('Y_axis')
            axes.set_title(bus_name)

            # Append 'Time Taken' to the existing legends
            handles, labels = axes.get_legend_handles_labels()
            time_taken_min = time_taken / 60
            handles.append(plt.Line2D([0], [0], color='white', label=f'Time Taken: {time_taken_min:.3f} minutes'))
            labels.append(f'Time Taken: {time_taken_min:.3f} minutes')
            axes.legend(handles, labels, loc='upper right')

            # Save the plot as an image file
            plt.savefig(f'{graph_name}.svg', format='svg', dpi=300)
            plt.tight_layout()


class ran_KerasRegressorWithHistory(BaseEstimator, RegressorMixin):
    def __init__(self, build_fn=Itachi.ran_create_model, epochs=100, batch_size=3, learning_rate=1e-3, num_layers=2, activation='relu', num_neurons=15):
        self.build_fn = build_fn
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.num_layers = int(num_layers)
        self.activation = activation
        self.num_neurons = int(num_neurons)
        self.model = None
        self.loss_history = []

    def fit(self, X, y):
        self.model = self.build_fn(self.learning_rate, self.num_layers, self.activation, self.num_neurons)
        history = self.model.fit(X, y, epochs=self.epochs, batch_size=self.batch_size, verbose=0, callbacks=[
            tf.keras.callbacks.LambdaCallback(
                on_epoch_end=lambda epoch, logs: self.loss_history.append(logs['loss']))])
        return self

    def predict(self, X):
        return self.model.predict(X)


class KerasRegressorWithHistory(BaseEstimator, RegressorMixin):
    def __init__(self, build_fn, epochs = epochs_num):
        self.build_fn = build_fn
        self.epochs = epochs
        self.model = None
        #self.alpha = 0.01
        self.loss_history = []
        self.val_loss_history = []

    def fit(self, X, y, batch_size):#, x_test, y_test):

        self.model = self.build_fn()
        history = self.model.fit(X, y,
                                 epochs=self.epochs,
                                 batch_size=batch_size,
                                 verbose=2,
                                 callbacks=[tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda epoch, logs: self.loss_history.append(logs['loss'])),
                                            tf.keras.callbacks.LambdaCallback(on_epoch_end=lambda epoch, logs: self.val_loss_history.append(logs['val_loss']))
                                            ],
                                 #validation_data=(x_test, y_test),
                                 validation_split = 0.7,
                                 workers = 2,
                                 use_multiprocessing = True
                                 )
        return self

    def predict(self, X):
        return self.model.predict(X)


if __name__ == "__main__":

    start_total = time.time()
    calc=0
    total_time=0

    start1 = time.time()

    read_file_real = 'realistic_data'
    read_file_simulated = 'simulated_data'

    if choice == 'real':
        itachi_instance = Itachi(epochs_num, read_file_real)
        data, num_cols, bus_name = itachi_instance.prepare_new_data(data_points, fresh_data)
    elif choice == 'simulated':
        itachi_instance = Itachi(epochs_num, read_file_simulated)
        data, num_cols, bus_name = itachi_instance.prepare_old_data(data_points)

    mse_test = []
    mse_train = []
    r2_test = []
    r2_oltest = []
    r2_oltrain = []
    r2_train = []
    mean_ytest = []
    mean_ypred = []
    mean_ytrain = []
    std_dev_ytest = []
    std_dev_ytrain = []
    std_dev_model = []


    if selected_num_cols!= None:
        num_cols=selected_num_cols
    result_area = pd.DataFrame()

    buses = []
    buses.append(bus_name[8])
    print(bus_name)
    print(f'selected: {buses}')
    num_cols = len(buses)

    for bus in buses:
        start = time.time() # recording the time
        select_bus = bus
        try: #to check if there is some syntax error or not
            (new_loss, new_controller, old_loss, old_controller, r2_oltestn, r2_oltrainn,
             mse_testn, mse_trainn, val_mse_values, r2_testn, r2_trainn,
             mean_ytest_n, mean_model_pred_n, mean_ytrain_n, std_dev_ytest_n,
             std_dev_ytrain_n, std_dev_pred_n)= itachi_instance.New_parameter_model(data, num_cols, select_bus)
        except Exception:
            print("Error in main")
            raise

        stop = time.time()
        time_taken = (stop - start)
        output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
        graph_name = os.path.join(output_path, ('new_learning_curve_' + select_bus))
        itachi_instance.plot_graph(graph_name, new_loss, 'Epoch', 'Training set', 'Extra1', 'Extra2', val_mse_values)
        graph_name = 'compare_' + select_bus
        count, area_comparison_df=itachi_instance.compare_mygraph(os.path.join(output_path,graph_name), new_controller, old_controller, 'Time', 'Model Prediction','New model', 'Model Prediction', 'Old model', 'Original', 'Original')
        graph_name = 'rescaling_' + select_bus
        itachi_instance.feature_scaling(os.path.join(output_path, graph_name), new_controller, old_controller, count, time_taken)

        mse_test.append(mse_testn)
        mse_train.append(mse_trainn)
        r2_test.append(r2_testn)
        r2_train.append(r2_trainn)
        r2_oltest.append(r2_oltestn)
        r2_oltrain.append(r2_oltrainn)
        mean_ytest.append(mean_ytest_n)
        mean_ypred.append(mean_model_pred_n)
        mean_ytrain.append(mean_ytrain_n)
        std_dev_ytest.append(std_dev_ytest_n)
        std_dev_ytrain.append(std_dev_ytrain_n)
        std_dev_model.append(std_dev_pred_n)

        result_area = result_area._append(area_comparison_df, ignore_index=True)
        if count==1:
            calc =calc+1

    print(result_area)

    # r2, mse, mean plot-----------------------------------------------------------
    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    x = range(1, len(mse_test) + 1)
    graph_name = os.path.join(output_path,'r2_mse_mean.svg')
    itachi_instance.plot_r2_mse(graph_name,x, r2_test, r2_train, mse_test, mse_train, mean_ytest, mean_ypred, mean_ytrain, std_dev_ytest, std_dev_ytrain, std_dev_model)

    collection = []
    for i in range(num_cols):
        data = {
            'r2_new': [float(r2_test[i]), float(r2_train[i]), '-'],
            'r2_old': [float(r2_oltest[i]), float(r2_oltrain[i]), '-'],
            'mse': [float(mse_test[i]), float(mse_train[i]), '-'],
            'mean': [float(mean_ytest[i]), float(mean_ytrain[i]), float(mean_ypred[i])],
            'std': [float(std_dev_ytest[i]), (std_dev_ytrain[i]), float(std_dev_model[i])]
        }
        r2_mean_mse_df = pd.DataFrame(data, index=['Test', 'Train', 'Pred'])
        r2_mean_mse_df.to_csv(os.path.join(output_path, f'r2_mse_mean.csv_{i}'), index=False)
        collection.append(r2_mean_mse_df)

    # Creating a DataFrame
    print(collection)
    result_df = pd.concat(collection, axis=1)
    result_df.replace('-', np.nan, inplace=True)
    result_df = result_df.apply(pd.to_numeric, errors='coerce')
    average_values = result_df.groupby(level=0, axis=1).mean()

    print("\nAverage Values:")
    print(average_values)
    #average_values.to_csv(os.path.join(output_path, f'avg_r2meanstd.csv_{i}'), index=False)
    # -----------------------------------------------------------------------------
    stop2 = time.time()
    total_time = (stop2-start1)/60
    print(f"{calc} buses have performed better than before in {total_time} minutes.")
    stop_total = time.time()
    time_taken = (stop - start)
    print(f'Total time:{time_taken/60} minutes.')

    bus_optimal={}
    bus_optimal['Names'] = ['activation', 'batch_size', 'epochs', 'learning_rate', 'num_layers', 'num_neurons']
    bus_optimal['0__bus__1'] = ['relu', 10, 100, 1e-3, 3,10]
    bus_optimal['0__bus__3'] = ['linear',19.5,354.5,0.019455,3.5,36.0]
    print(f'\n{bus_optimal}')


    """def input_insight(self):
        #data, num_cols, bus_name = self.prepare_data()
        # g_Plot_mygraphs.plot_mygraph(os.path.join(output_path,'Qt_Qt1_V_real'), data, 'Time', 'q generated', 'q generated t+1', 'V')

        num_subplots_rows = 4
        num_subplots_cols = 3
        time_series = range(0, len(self.data))
        fig, axs = plt.subplots(num_subplots_rows, num_subplots_cols, figsize=(15, 15))

        for bus_num in range(1, self.num_cols+1):
            if bus_num == 2:
                continue
            else:
                bus_name = f'0__bus__{bus_num}'

                for i, subplot_title in enumerate(['V', 'q generated', 'q generated t+1']):
                    axs[0, i].plot(time_series, self.data[bus_name][subplot_title], label=f'{bus_name}_{subplot_title}')

    def calculate_mean_std(self, data):
        mean_data = {}
        std_data = {}

        for bus_num in range(1, self.num_cols + 1):
            if bus_num == 2:
                continue
            bus_name = f'0__bus__{bus_num}'

            for subplot_title in ['V', 'q generated', 'q generated t+1']:
                mean_val = self.data[bus_name][subplot_title].mean()
                std_val = self.data[bus_name][subplot_title].std()

                mean_data.setdefault(subplot_title, []).append(mean_val)
                std_data.setdefault(subplot_title, []).append(std_val)

        return mean_data, std_data"""

    """def plot_mean_std(self, axs, mean_data, std_data):
        num_plot_rows = 3
        num_plot_cols = 2
        for i, subplot_title in enumerate(['V', 'q generated', 'q generated t+1']):
            axs[num_plot_rows, i].plot(range(1, num_cols + 1), mean_data[subplot_title], color='blue')
            axs[num_plot_rows + 1, i].plot(range(1, num_cols + 1), std_data[subplot_title], color='orange')
            axs[num_plot_rows, i].set_title(f'Mean {subplot_title}')
            axs[num_plot_rows + 1, i].set_title(f'Std Dev {subplot_title}')
            axs[num_plot_rows, i].set_xlabel('Bus Number')
            axs[num_plot_rows, i].set_ylabel('Mean Value')
            axs[num_plot_rows + 1, i].set_xlabel('Bus Number')
            axs[num_plot_rows + 1, i].set_ylabel('Std Dev Value')

        # Remove unnecessary subplots
        for i in range(1, num_plot_cols + 1):
            axs[num_plot_rows, i].axis('off')
            axs[num_plot_rows + 1, i].axis('off')"""