import os
import ast
import pandas as pd
import ast
from datetime import datetime
output_path = os.path.abspath(os.path.join(__file__, "..", "Store_realistic_data_perm"))

def real_data_sort(real_data, cols = ['time', 'vt', 'qt', 'qt1']):

    new_df = pd.DataFrame(columns=cols)
    new_df['time'] = real_data['muscle_action_walltime'].copy()
    n_rows = new_df['time'].count()
    num_buses = 14
    #print(real_data['muscle_sensor_readings'][0])


    for i in range(n_rows):
        collect_qt1 = []
        int_qt1 = real_data['muscle_actuator_setpoints'][i]
        int_qt1_1 = ast.literal_eval(int_qt1)
        num_elements = len(int_qt1_1)
        for k in range(num_elements):
            collect_qt1.append(int_qt1_1[f'midas_powergrid.Pysimmods-0.Photovoltaic-{k}.q_set_mvar'])
        new_df.at[i, 'qt1'] = collect_qt1

    for i in range(n_rows):
        collect_v = []
        collect_q = []
        int_vq = real_data['muscle_sensor_readings'][i]
        int_vq_1 = ast.literal_eval(int_vq)
        num_elements = 13
        for k in range(num_elements):
            collect_q.append(int_vq_1[f'midas_powergrid.Pysimmods-0.Photovoltaic-{k}.q_mvar'])
        for j in range(1,num_buses+1):
            if j == 2:
                continue
            else:
                collect_v.append(int_vq_1[f'midas_powergrid.Powergrid-0.0-bus-{j}.vm_pu'])
        new_df.at[i, 'vt'] = collect_v
        new_df.at[i, 'qt'] = collect_q

    split_values = real_data['muscle_sensor_readings'].apply(lambda x: ast.literal_eval(str(x)))
    half_len = len(split_values.iloc[0]) // 2


    #new_df['qt1'] = new_df['qt1'].apply(lambda x: ast.literal_eval(str(x)) if pd.notnull(x) else [])
    #new_df['vt'] = split_values.apply(lambda row: [float(val) for val in row[:half_len]])
    #new_df['qt'] = split_values.apply(lambda row: [float(val) for val in row[half_len:]])
    num_values = len(new_df['qt1'].iloc[0])

    for j in range(num_values):
        new_df[f'0__bus__{j + 1}___qt1_mvar'] = new_df['qt1'].apply(lambda row: row[j])
    for i in range(half_len):
        new_df[f'0__bus__{i + 1}___vm_pu'] = new_df['vt'].apply(lambda row: row[i])
        new_df[f'0__bus__{i + 1}___q_mvar'] = new_df['qt'].apply(lambda row: row[i])

    renamed_columns = {'0__bus__13___vm_pu': '0__bus__14___vm_pu', '0__bus__13___q_mvar': '0__bus__14___q_mvar', '0__bus__13___qt1_mvar': '0__bus__14___qt1_mvar',
                       '0__bus__12___vm_pu': '0__bus__13___vm_pu', '0__bus__12___q_mvar': '0__bus__13___q_mvar', '0__bus__12___qt1_mvar': '0__bus__13___qt1_mvar',
                       '0__bus__11___vm_pu': '0__bus__12___vm_pu', '0__bus__11___q_mvar': '0__bus__12___q_mvar', '0__bus__11___qt1_mvar': '0__bus__12___qt1_mvar',
                       '0__bus__10___vm_pu': '0__bus__11___vm_pu', '0__bus__10___q_mvar': '0__bus__11___q_mvar', '0__bus__10___qt1_mvar': '0__bus__11___qt1_mvar',
                       '0__bus__9___vm_pu': '0__bus__10___vm_pu', '0__bus__9___q_mvar': '0__bus__10___q_mvar', '0__bus__9___qt1_mvar': '0__bus__10___qt1_mvar',
                       '0__bus__8___vm_pu': '0__bus__9___vm_pu', '0__bus__8___q_mvar': '0__bus__9___q_mvar', '0__bus__8___qt1_mvar': '0__bus__9___qt1_mvar',
                       '0__bus__7___vm_pu': '0__bus__8___vm_pu', '0__bus__7___q_mvar': '0__bus__8___q_mvar', '0__bus__7___qt1_mvar': '0__bus__8___qt1_mvar',
                       '0__bus__6___vm_pu': '0__bus__7___vm_pu', '0__bus__6___q_mvar': '0__bus__7___q_mvar', '0__bus__6___qt1_mvar': '0__bus__7___qt1_mvar',
                       '0__bus__5___vm_pu': '0__bus__6___vm_pu', '0__bus__5___q_mvar': '0__bus__6___q_mvar', '0__bus__5___qt1_mvar': '0__bus__6___qt1_mvar',
                       '0__bus__4___vm_pu': '0__bus__5___vm_pu', '0__bus__4___q_mvar': '0__bus__5___q_mvar', '0__bus__4___qt1_mvar': '0__bus__5___qt1_mvar',
                       '0__bus__3___vm_pu': '0__bus__4___vm_pu', '0__bus__3___q_mvar': '0__bus__4___q_mvar', '0__bus__3___qt1_mvar': '0__bus__4___qt1_mvar',
                       '0__bus__2___vm_pu': '0__bus__3___vm_pu', '0__bus__2___q_mvar': '0__bus__3___q_mvar', '0__bus__2___qt1_mvar': '0__bus__3___qt1_mvar',
                       }
    new_df = new_df.drop('qt1', axis=1)
    new_df = new_df.drop('qt', axis=1)
    new_df = new_df.drop('vt', axis=1)
    new_df1 = new_df.rename(columns=renamed_columns)

    vtdata = new_df1.filter(like='vm_pu')
    qtdata = new_df1.filter(like='q_mvar')
    qt1data = new_df1.filter(like='qt1_mvar')


    timedata = new_df1['time']
    #print(timedata)
    timestamp1 = datetime.strptime(timedata[0], '%Y-%m-%d %H:%M:%S.%f')
    timestamp2 = datetime.strptime(timedata[8], '%Y-%m-%d %H:%M:%S.%f')
    time_difference = timestamp2 - timestamp1
    #print(f"The difference is: {time_difference}")



    return vtdata, qtdata, qt1data


if __name__ == '__main__':
    real_data = pd.read_csv(f'{output_path}.csv')
    cols = ['time', 'vt', 'qt', 'qt1']
    vtdata, qtdata, qt1data = real_data_sort(real_data, cols)
    #print(vtdata, qtdata, qt1data)


