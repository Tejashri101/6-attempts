import matplotlib.pyplot as plt
import numpy as np
import os

def power_triangle():
    # Define the vectors P and Q
    P = np.array([3, 0])
    Q = np.array([0, 4])

    # Calculate the vector S as the sum of P and Q
    S = P + Q

    # Create the plot
    plt.figure(figsize=(6, 6))

    # Plot vector P as a green arrow
    plt.arrow(0, 0, P[0], P[1], head_width=0.2, head_length=0.2, fc='green', ec='green', label='P [kW]')

    # Plot vector Q as a red arrow
    plt.arrow(P[0], P[1], Q[0], Q[1], head_width=0.2, head_length=0.2, fc='red', ec='red', label='Q [kvar]')

    # Plot vector S as a blue arrow
    plt.arrow(0, 0, S[0], S[1], head_width=0.2, head_length=0.2, fc='blue', ec='blue', label='S [kVA]')


    # Set axis limits and labels
    plt.xlim(-1, 4)
    plt.ylim(-1, 5)
    plt.xlabel('P [kW]', fontsize=14, color='green')
    plt.ylabel('Q [kvar]', fontsize=14, color='red')

    # Add a legend
    plt.legend()

    # Show the plot
    plt.grid(True)
    plt.title('Power Triangle')

    output_path = os.path.abspath(os.path.join(__file__, "..", "Storage"))
    graph_name = os.path.join(output_path,'Power_triangle')
    plt.savefig(f'{graph_name}.png')

if __name__ == "__main__":
    power_triangle()
